package com.hfqixin.core.util;

import org.springframework.aop.framework.Advised;
import org.springframework.aop.target.SingletonTargetSource;
import org.springframework.beans.BeansException;
import org.springframework.context.ApplicationContext;

/**
 * 
 * @author 闪电侠
 *SpringContextUtils的applicationContext属性会自动初始化（不必 new ClassPathXmlApplicationContext("applicationContext.xml")），于是可以自行编写一些静态方法从 applicationContext 中获取bean了
 */


public class SpringContextUtils
{
	
	@SuppressWarnings ("unchecked")
    public static <T> T getBean(ApplicationContext applicationContext,String name) throws BeansException {
		return (T)applicationContext.getBean(name);
	}
	
	@SuppressWarnings ("unchecked")
    public static <T> T getSourceBean(ApplicationContext applicationContext,String name) {
		Advised advised = (Advised) applicationContext.getBean(name);
		SingletonTargetSource singTarget = (SingletonTargetSource) advised.getTargetSource();
		return (T)singTarget.getTarget();
	}
	
}
