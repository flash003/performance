package com.hfqixin.problem.dao;

import com.hfqixin.problem.domain.PUser;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface PUserMapper {
    int deleteByPrimaryKey(String id);

    int insert(PUser record);

    int insertSelective(PUser record);

    PUser selectByPrimaryKey(String id);

    int updateByPrimaryKeySelective(PUser record);

    int updateByPrimaryKey(PUser record);

    List<PUser> checkAccount(PUser pUser)throws Exception;
}