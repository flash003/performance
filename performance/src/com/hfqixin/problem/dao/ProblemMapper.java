package com.hfqixin.problem.dao;

import com.hfqixin.problem.domain.Problem;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface ProblemMapper {
    int deleteByPrimaryKey(String id)throws Exception;

    int insertSelective(Problem record)throws Exception;

    Problem selectByPrimaryKey(String id)throws Exception;

    int updateByPrimaryKeySelective(Problem record)throws Exception;

    List<Problem> completeProblem(Problem problem)throws Exception;

    List<Problem> unCompleteProblem(Problem problem)throws Exception;

    int approve(Problem problem)throws Exception;

    List<Problem> getUnApproveList()throws Exception;


}