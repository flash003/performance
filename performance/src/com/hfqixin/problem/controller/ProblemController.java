package com.hfqixin.problem.controller;

import com.hfqixin.manager.bean.DataAuthority;
import com.hfqixin.manager.controller.BaseController;
import com.hfqixin.manager.util.SessionUtils;
import com.hfqixin.problem.domain.Problem;
import com.hfqixin.problem.service.ProblemService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.List;

@Controller
@RequestMapping(value = "problem")
public class ProblemController extends BaseController {

    @Autowired
    private ProblemService service;

    @RequestMapping(value = "/addProblem")
    public void addProblem(HttpServletResponse response, HttpServletRequest request, @RequestBody Problem problem) {
        DataAuthority dataAuthority = SessionUtils.getCurrDataAuthority(request);
        try {
            int temp = service.addProblem(problem);
            if (temp == 1) {
                respSuccessMsg(response, null, "操作成功");
            } else {
                respErrorMsg(response, "非正常添加");
            }
        } catch (Exception e) {
            e.printStackTrace();
            respErrorMsg(response, "系统异常");
        }
    }

    @RequestMapping(value = "complete")
    public void complete(HttpServletResponse response,HttpServletRequest request, @RequestBody Problem problem) {
        DataAuthority dataAuthority = SessionUtils.getCurrDataAuthority(request);
        try {
            List<Problem> problemList = service.complete(problem);
            respSuccessMsg(response, problemList, "操作成功");
        } catch (Exception e) {
            e.printStackTrace();
            respErrorMsg(response, "系统异常");
        }
    }

    @RequestMapping(value = "unComplete")
    public void unComplete(HttpServletResponse response,HttpServletRequest request, @RequestBody Problem problem) {
        DataAuthority dataAuthority = SessionUtils.getCurrDataAuthority(request);
        try {
            List<Problem> problemList = service.unComplete(problem);
            respSuccessMsg(response, problemList, "操作成功");
        } catch (Exception e) {
            e.printStackTrace();
            respErrorMsg(response, "系统异常");
        }
    }

    @RequestMapping(value = "/modifyProblem")
    public void modifyProblem(HttpServletResponse response,HttpServletRequest request, @RequestBody Problem problem) {
        DataAuthority dataAuthority = SessionUtils.getCurrDataAuthority(request);
        try {
            int temp = service.modifyProblem(problem);
            if (temp == 1) {
                respSuccessMsg(response, null, "操作成功");
            } else {
                respErrorMsg(response, "非正常修改");
            }
        } catch (Exception e) {
            e.printStackTrace();
            respErrorMsg(response, "系统异常");
        }
    }

    @RequestMapping(value = "getUnApproveProblem")
    public void getUnApproveProblem(HttpServletResponse response,HttpServletRequest request, @RequestBody Problem problem) {
        DataAuthority dataAuthority = SessionUtils.getCurrDataAuthority(request);
        try {
            List<Problem> problemList = service.getUnApproveList();
            respSuccessMsg(response, problemList, "操作成功");
        } catch (Exception e) {
            e.printStackTrace();
            respErrorMsg(response, "系统异常");
        }
    }

    @RequestMapping(value = "/approve")
    public void approve(HttpServletResponse response,HttpServletRequest request, @RequestBody Problem problem){
        DataAuthority dataAuthority = SessionUtils.getCurrDataAuthority(request);
        try {
             service.approve(problem);
            respSuccessMsg(response, null, "操作成功");
        } catch (Exception e) {
            e.printStackTrace();
            respErrorMsg(response, "系统异常");
        }
    }

    @RequestMapping(value = "/remove")
    public void remove(HttpServletResponse response,HttpServletRequest request, @RequestBody Problem problem){
        DataAuthority dataAuthority = SessionUtils.getCurrDataAuthority(request);
        try {
            if(null != problem.getId()){
                service.remove(problem);
                respSuccessMsg(response, null, "操作成功");
            }else{
                respErrorMsg(response, "参数有误");
            }
        } catch (Exception e) {
            e.printStackTrace();
            respErrorMsg(response, "系统异常");
        }
    }

}
