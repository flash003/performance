package com.hfqixin.problem.controller;

import com.hfqixin.manager.bean.DataAuthority;
import com.hfqixin.manager.controller.BaseController;
import com.hfqixin.manager.util.SessionUtils;
import com.hfqixin.problem.domain.PUser;
import com.hfqixin.problem.service.PUserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.List;

@Controller
@RequestMapping("pUser")
public class PUserController extends BaseController{

    @Autowired
    private PUserService service;

    @RequestMapping(value = "/login")
    public void login(HttpServletResponse response, HttpServletRequest request, @RequestBody PUser pUser){
        try {
            DataAuthority dataAuthority = SessionUtils.getCurrDataAuthority(request);
            if(null != pUser.getUserName() && null != pUser.getPassWord()){
                List<PUser> pUserList = service.checkAccount(pUser);
                if(null != pUserList && pUserList.size() == 1){
                    respSuccessMsg(response, pUserList.get(0), "登录成功");
                }else{
                    respErrorMsg(response, "登录失败");
                }
            }else{
                respErrorMsg(response, "参数有误");
            }
        } catch (Exception e) {
            e.printStackTrace();
            respErrorMsg(response, "系统异常");
        }
    }
}
