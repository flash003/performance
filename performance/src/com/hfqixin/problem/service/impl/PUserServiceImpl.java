package com.hfqixin.problem.service.impl;

import com.hfqixin.problem.dao.PUserMapper;
import com.hfqixin.problem.domain.PUser;
import com.hfqixin.problem.service.PUserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class PUserServiceImpl implements PUserService {

    @Autowired
    private PUserMapper dao;

    @Override
    public List<PUser> checkAccount(PUser pUser) throws Exception {
        return dao.checkAccount(pUser);
    }
}
