package com.hfqixin.problem.service.impl;

import com.hfqixin.problem.dao.ProblemMapper;
import com.hfqixin.problem.domain.Problem;
import com.hfqixin.problem.service.ProblemService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.List;
import java.util.UUID;

@Service
public class ProblemServiceImpl implements ProblemService {

    @Autowired
    private ProblemMapper dao;

    @Override
    public int addProblem(Problem problem) throws Exception {
        problem.setInsertTime(new Date());
        problem.setUpdateTime(new Date());
        problem.setId(UUID.randomUUID().toString().replaceAll("-",""));
        return dao.insertSelective(problem);
    }

    @Override
    public List<Problem> complete(Problem problem) throws Exception {
        return dao.completeProblem(problem);
    }

    @Override
    public List<Problem> unComplete(Problem problem) throws Exception {
        return dao.unCompleteProblem(problem);
    }

    @Override
    public int modifyProblem(Problem problem) throws Exception {
        problem.setUpdateTime(new Date());
        return dao.updateByPrimaryKeySelective(problem);
    }

    @Override
    public int approve(Problem problem) throws Exception {
        return dao.approve(problem);
    }

    @Override
    public List<Problem> getUnApproveList() throws Exception {
        return dao.getUnApproveList();
    }

    @Override
    public int remove(Problem problem) throws Exception {
        return dao.deleteByPrimaryKey(problem.getId());
    }
}
