package com.hfqixin.problem.service;

import com.hfqixin.problem.domain.Problem;

import java.util.List;

public interface ProblemService {

    int addProblem(Problem problem)throws Exception;

    List<Problem> complete(Problem problem)throws Exception;

    List<Problem> unComplete(Problem problem)throws Exception;

    int modifyProblem(Problem problem)throws Exception;

    int approve(Problem problem)throws Exception;

    List<Problem> getUnApproveList()throws Exception;

    int remove(Problem problem)throws Exception;
}
