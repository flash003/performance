package com.hfqixin.problem.service;

import com.hfqixin.problem.domain.PUser;

import java.util.List;

public interface PUserService {

    List<PUser> checkAccount(PUser pUser)throws Exception;

}
