package com.hfqixin.ipqc.controller;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import com.hfqixin.ipqc.bean.IPQC;
import com.hfqixin.ipqc.service.IPQCService;
import com.hfqixin.manager.annotation.AuthenPassport;
import com.hfqixin.manager.annotation.Permission;
import com.hfqixin.manager.bean.DataAuthority;
import com.hfqixin.manager.bean.sys.PageMsg;
import com.hfqixin.manager.controller.BaseController;
import com.hfqixin.manager.util.SessionUtils;

@Controller
@RequestMapping(value = "ipqc")
public class IPQCController extends BaseController {

	@Autowired
	private IPQCService service;
	
	@AuthenPassport
	@Permission
	@RequestMapping(value = "/list", method = RequestMethod.GET)
	public ModelAndView list() {
		return new ModelAndView("/ipqc/ipqcList");
	}
	
	@RequestMapping(value = "/addEdit", method = RequestMethod.GET)
	public ModelAndView addEdit(HttpServletRequest request) throws Exception {
		ModelAndView mv = new ModelAndView();
		mv.addObject("id", request.getParameter("id"));
		mv.setViewName("/ipqc/ipqcAdd");
		return mv;
	}

	@RequestMapping(value = "/updateEdit", method = RequestMethod.GET)
	public ModelAndView updateEdit(HttpServletRequest request, HttpServletResponse response) throws Exception {
		ModelAndView mv = new ModelAndView();
		mv.addObject("id", request.getParameter("id"));
		mv.setViewName("/ipqc/ipqcUpdate");
		return mv;
	}
	
	@AuthenPassport
	@Permission
	@RequestMapping(value = "/listIPQC", method = RequestMethod.POST)
	public void listIPQC(HttpServletRequest request, HttpServletResponse response,
			IPQC ipqc) {
		try {
			DataAuthority dataAuthority = SessionUtils.getCurrDataAuthority(request);
			ipqc.setDataAuth(dataAuthority);
			PageMsg<IPQC> list = service.listIPQC(ipqc);
			respSuccessMsg(response,list, "查询成功");
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
			respErrorMsg(response, "系统异常");
		}
	}
	
	@AuthenPassport
	@Permission
	@RequestMapping(value = "/addIPQC", method = RequestMethod.POST)
	public void addIPQC(HttpServletRequest request, HttpServletResponse response,IPQC ipqc) {
			DataAuthority dataAuthority = SessionUtils.getCurrDataAuthority(request);
			ipqc.setDataAuth(dataAuthority);
		try {
			service.insertIPQC(ipqc);
			respSuccessMsg(response, null, "添加成功");

		} catch (Exception e) {
			// TODO handle exception
			e.printStackTrace();
			respErrorMsg(response, "系统异常");
		}
	}
	
	@AuthenPassport
	@RequestMapping(value = "/getIPQC", method = RequestMethod.POST)
	public void getIPQC(HttpServletRequest request, HttpServletResponse response, String id) {
		try {
			System.err.println("ajax" + id);
			IPQC ipqc = service.getIPQC(id);
			respSuccessMsg(response, ipqc, "查询成功");
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
			respErrorMsg(response, "系统异常");
		}
	}
	
	@RequestMapping(value = "/delete", method = RequestMethod.POST)
	public void delete(HttpServletRequest request, HttpServletResponse response, String id) {
		try {
			service.deleteIPQC(id);
			respSuccessMsg(response, null, "删除成功");
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
			respErrorMsg(response, "系统异常");
		}
	}
	
	@AuthenPassport
	@Permission
	@RequestMapping(value = "/updateIPQC", method = RequestMethod.POST)
	public void updateIPQC(HttpServletRequest request, HttpServletResponse response, IPQC ipqc) {

		try {
			
			service.updateIPQC(ipqc);
			respSuccessMsg(response, null, "修改成功");
		} catch (Exception e) {
			// TODO handle exception
			e.printStackTrace();
			respErrorMsg(response, "系统异常");
		}
	}
}
