package com.hfqixin.ipqc.service;

import java.util.List;

import com.hfqixin.ipqc.bean.IPQC;
import com.hfqixin.manager.bean.sys.PageMsg;

public interface IPQCService {

	public PageMsg<IPQC> listIPQC(IPQC ipqc) throws Exception;

	public int insertIPQC(IPQC ipqc) throws Exception;

	public int deleteIPQC(String id) throws Exception;

	public IPQC getIPQC(String id) throws Exception;

	public int updateIPQC(IPQC ipqc) throws Exception;
	
	public IPQC checkUserLogin(IPQC ipqc)throws Exception;
	
	public List<IPQC> getIPQCList()throws Exception;
}
