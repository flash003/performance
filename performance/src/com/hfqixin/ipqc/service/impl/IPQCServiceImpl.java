package com.hfqixin.ipqc.service.impl;

import java.util.Date;
import java.util.List;
import java.util.UUID;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.hfqixin.ipqc.bean.IPQC;
import com.hfqixin.ipqc.dao.IPQCDao;
import com.hfqixin.ipqc.service.IPQCService;
import com.hfqixin.manager.bean.sys.PageMsg;

@Service
public class IPQCServiceImpl implements IPQCService {

	@Autowired
	private IPQCDao dao;

	@Override
	public PageMsg<IPQC> listIPQC(IPQC ipqc) throws Exception {
		List<IPQC> list = dao.listIPQC(ipqc);
		PageMsg<IPQC> pageObj = new PageMsg<IPQC>();
		pageObj.setListResult(list);
		pageObj.setTotalNum(dao.countIPQC(ipqc));
		pageObj.setPageNum(ipqc.getSize());
		pageObj.setPage(ipqc.getCurr());
		pageObj.setTotalPage();
		return pageObj;
	}

	@Override
	public int insertIPQC(IPQC ipqc) throws Exception {
		ipqc.setId(UUID.randomUUID().toString().replaceAll("-", ""));
		ipqc.setInsertTime(new Date());
		ipqc.setModifyTime(new Date());
		return dao.insertIPQC(ipqc);
	}

	@Override
	public int deleteIPQC(String id) throws Exception {
		return dao.deleteIPQC(id);
	}

	@Override
	public IPQC getIPQC(String id) throws Exception {
		return dao.getIPQC(id);
	}

	@Override
	public int updateIPQC(IPQC ipqc) throws Exception {
		ipqc.setModifyTime(new Date());
		return dao.updateIPQC(ipqc);
	}

	@Override
	public IPQC checkUserLogin(IPQC ipqc) throws Exception {
		return dao.checkUserLogin(ipqc);
	}

	@Override
	public List<IPQC> getIPQCList() throws Exception {
		return dao.getIPQCList();
	}
}
