package com.hfqixin.ipqc.dao;

import java.util.List;

import org.springframework.stereotype.Repository;

import com.hfqixin.ipqc.bean.IPQC;

@Repository
public interface IPQCDao {
    
	public List<IPQC> listIPQC(IPQC ipqc)throws Exception;
	
	public int countIPQC(IPQC ipqc)throws Exception;
	
	public int insertIPQC(IPQC ipqc)throws Exception;
	
	public int deleteIPQC(String id)throws Exception;
	
	public IPQC getIPQC(String id)throws Exception;
	
	public int updateIPQC(IPQC ipqc)throws Exception;
	
	public IPQC checkUserLogin(IPQC ipqc)throws Exception;
	
	public List<IPQC> getIPQCList()throws Exception;
}