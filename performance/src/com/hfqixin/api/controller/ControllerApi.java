package com.hfqixin.api.controller;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.hfqixin.assess.bean.Assess;
import com.hfqixin.assess.bean.AssessInfo;
import com.hfqixin.assess.bean.InputAssessInfo;
import com.hfqixin.assess.bean.OutputAssess;
import com.hfqixin.assess.bean.OutputForMine;
import com.hfqixin.assess.bean.OutputMineForMonitor;
import com.hfqixin.assess.service.AssessInfoService;
import com.hfqixin.assess.service.AssessService;
import com.hfqixin.group.bean.Group;
import com.hfqixin.group.service.GroupService;
import com.hfqixin.manager.bean.DataAuthority;
import com.hfqixin.manager.bean.Dictionary;
import com.hfqixin.manager.controller.BaseController;
import com.hfqixin.manager.service.sys.DictionaryService;
import com.hfqixin.manager.util.DateUtil;
import com.hfqixin.manager.util.SessionUtils;
import com.hfqixin.staff.bean.InputStaff;
import com.hfqixin.staff.bean.Staff;
import com.hfqixin.staff.service.StaffService;

@Controller
@RequestMapping(value = "api")
public class ControllerApi extends BaseController {

	@Autowired
	private StaffService staffService;

	@Autowired
	private AssessService assessService;

	@Autowired
	private AssessInfoService infoService;

	@Autowired
	private GroupService groupService;

	@Autowired
	private DictionaryService dictionaryService;

	@RequestMapping(value = "/getAssessInfo", method = RequestMethod.POST)
	public void getAssessInfo(HttpServletRequest request, HttpServletResponse response,
			@RequestBody InputAssessInfo info) {
		@SuppressWarnings("unused")
		DataAuthority dataAuthority = SessionUtils.getCurrDataAuthority(request);
		try {
			List<AssessInfo> list = infoService.getAssessInfoList(info.getAssessId());
			respSuccessMsg(response, list, "查询成功");
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			respErrorMsg(response, "系统异常");
		}

	}

	@RequestMapping(value = "/assess", method = RequestMethod.POST)
	public void assess(HttpServletRequest request, HttpServletResponse response, @RequestBody InputAssessInfo info) {
		@SuppressWarnings("unused")
		DataAuthority dataAuthority = SessionUtils.getCurrDataAuthority(request);
		List<AssessInfo> list = info.getAssessInfo();
		try {
			Assess assess3 = assessService.getAssess(info.getAssessId());
			for (int i = 0; i < list.size(); i++) {
				if (list.get(i).getOpStatu() == 0) {
					list.get(i).setAssessId(info.getAssessId());
					list.get(i).setCreateTime(list.get(i).getCreateTime());
					if (assess3 != null) {
						list.get(i).setGroupId(assess3.getGroupId());
						list.get(i).setGroupName(assess3.getGroupName());
					}
					infoService.insertAssessInfo(list.get(i));
				} else if (list.get(i).getOpStatu() == 1) {
					list.get(i).setModifyTime(list.get(i).getInsertTime());
					infoService.updateAssessInfo(list.get(i));
				} else if (list.get(i).getOpStatu() == 2) {
					infoService.deleteAssessInfo(list.get(i).getId());
				}
			}
			List<AssessInfo> list2 = infoService.getAssessInfoList(info.getAssessId());
			System.err.println(list2.size());
			Integer assessDay = 0, add = 0, reduce = 0;
			for (int k = 0; k < list2.size(); k++) {
				if (list2.get(k).getValue() < 0) {
					list2.get(k).setStatu(-1);
					reduce += list2.get(k).getValue();
				}
				if (list2.get(k).getValue() > 0) {
					list2.get(k).setStatu(1);
					add += list2.get(k).getValue();
				}
				assessDay += list2.get(k).getValue();
			}
			Assess assess = new Assess();
			assess.setId(info.getAssessId());
			assess.setAssessDay(assessDay);
			assess.setAssessAdd(add);
			assess.setAssessReduce(reduce);
			assess.setAssessFlag(1);
			Dictionary dictionary = dictionaryService.getValueByKey("standard");
			if (dictionary == null) {
				dictionary = new Dictionary();
				dictionary.setKeyValue("100");
			}
			assess.setStandard(Integer.valueOf(dictionary.getKeyValue()));
			assess.setScore(Integer.valueOf(dictionary.getKeyValue()) + assessDay);
			assessService.updateAssess(assess);
			Staff staff = new Staff();
			staff.setAssessTime(new Date());
			staff.setId(info.getStaffId());
			staffService.updateStaff(staff);
			respSuccessMsg(response, null, "考核成功");

		} catch (Exception e) {
			e.printStackTrace();
			respErrorMsg(response, "系统异常");
		}
	}

	@RequestMapping(value = "/getStaffInfo", method = RequestMethod.POST)
	public void getStaffInfo(HttpServletRequest request, HttpServletResponse response, @RequestBody InputStaff input) {
		try {
			@SuppressWarnings("unused")
			DataAuthority dataAuthority = SessionUtils.getCurrDataAuthority(request);
			input.setCurrentTime(new Date());
			Date currentTime1 = input.getCurrentTime();
			input.setCurrTime(new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(currentTime1));
			input.setStartTime(new SimpleDateFormat("yyyy-MM-dd").format(currentTime1));
			input.setEndTime(input.getStartTime() + " 23:59:59");
			System.err.println("currTime:" + input.getCurrTime() + "startTime:" + input.getStartTime() + "endTime:"
					+ input.getEndTime());
			List<OutputAssess> list = assessService.listAssessForGroupIdDay(input);
			respSuccessMsg(response, list, "查询成功");
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			respErrorMsg(response, "系统异常");
		}
	}

	@RequestMapping(value = "/initAssess", method = RequestMethod.POST)
	public void initAssess(HttpServletRequest request, HttpServletResponse response, @RequestBody InputStaff input) {
		@SuppressWarnings("unused")
		DataAuthority dataAuthority = SessionUtils.getCurrDataAuthority(request);
		try {
			Group group = groupService.getGroup(input.getGroupId());
			if (group.getAssessTime() == null) {
				group.setAssessTime(DateUtil.getDate(new Date(), -1));
			}
			String assessTime1 = new SimpleDateFormat("yyyy-MM-dd").format(group.getAssessTime());
			String currTime = new SimpleDateFormat("yyyy-MM-dd").format(new Date());
			if (currTime.compareTo(assessTime1) > 0) {
				List<Staff> list = staffService.getStaffListForGroup(input);
				Staff staff = new Staff();
				staff.setAssessTime(new Date());
				for (int i = 0; i < list.size(); i++) {
					assessService.initAssess(list.get(i));
					staff.setId(list.get(i).getId());
					staffService.updateStaff(staff);
				}
				Group group1 = new Group();
				group1.setId(input.getGroupId());
				group1.setAssessTime(new Date());
				groupService.updateGroup(group1);
				respSuccessMsg(response, null, "初始化成功");
			} else {
				respSuccessMsg(response, null, "当天只能初始化一次");
			}

		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			respErrorMsg(response, "系统异常");
		}
	}

	@RequestMapping(value = "/clockIn", method = RequestMethod.POST)
	public void clockIn(HttpServletRequest request, HttpServletResponse response, @RequestBody InputStaff input) {
		@SuppressWarnings("unused")
		DataAuthority dataAuthority = SessionUtils.getCurrDataAuthority(request);
		try {
			input.setCurrentTime(new Date());
			Date currentTime1 = input.getCurrentTime();
			input.setCurrTime(new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(currentTime1));
			input.setStartTime(new SimpleDateFormat("yyyy-MM-dd").format(currentTime1));
			input.setEndTime(input.getStartTime() + " 23:59:59");
			List<OutputAssess> list = assessService.listAssessForGroupIdDay(input);
			for (int i = 0; i < list.size(); i++) {
				list.get(i).setJobTime(input.getJobTime());
				assessService.clockIn(list.get(i));
			}
			respSuccessMsg(response, null, "打卡成功");
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			respErrorMsg(response, "系统异常");
		}

	}

	@RequestMapping(value = "/login", method = RequestMethod.POST)
	public void postLogin(HttpServletRequest request, HttpServletResponse response, @RequestBody Staff staff) {
		@SuppressWarnings("unused")
		DataAuthority dataAuthority = SessionUtils.getCurrDataAuthority(request);
		try {
			Staff staff1 = staffService.checkUserLogin(staff);
			if (staff1 == null) {
				respErrorMsg(response, "登录失败，请检查帐号和密码");
			} else {
				String staffId = staff1.getId();
				// 写数据到session，并跳转到主页
				// 跳转主页是通过异步请求的方式完成
				SessionUtils.setAttr(request, "staffIdInfo", staffId);
				SessionUtils.setAttr(request, "staffInfo", staff1);
				// 继承父类BaseController的方法
				respSuccessMsg(response, staff1, "登录成功");
			}
		} catch (Exception e) {
			respErrorMsg(response, "登录异常，请联系网站管理员");
		}
	}

	// 我的(班长)
	@RequestMapping(value = "/mineForMonitor", method = RequestMethod.POST)
	public void mineForMonitor(HttpServletRequest request, HttpServletResponse response,
			@RequestBody InputStaff input) {
		@SuppressWarnings("unused")
		DataAuthority dataAuthority = SessionUtils.getCurrDataAuthority(request);
		input.setCurrentTime(new Date());
		Date currentTime1 = input.getCurrentTime();
		input.setCurrTime(new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(currentTime1));
		input.setStartTime(DateUtil.lastMonth(currentTime1)[0]);
		input.setEndTime(DateUtil.lastMonth(currentTime1)[1] + "23:59:59");
		try {
			OutputAssess assess = assessService.listAssessForStaffId(input);
			if (assess == null) {
				assess = new OutputAssess();
			}
			input.setCurrentTime(new Date());
			Date currentTime2 = input.getCurrentTime();
			input.setCurrTime(new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(currentTime2));
			input.setStartTime(new SimpleDateFormat("yyyy-MM-dd").format(currentTime2));
			input.setEndTime(input.getStartTime() + " 23:59:59");
			OutputMineForMonitor mine = new OutputMineForMonitor();
			List<OutputAssess> list = assessService.listAssessForGroupIdDay(input);
			mine.setAssess(assess);
			mine.setAssessList(list);
			respSuccessMsg(response, mine, "查询成功");
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			respErrorMsg(response, "系统异常");
		}
	}

	// 我的
	@RequestMapping(value = "/mineToday", method = RequestMethod.POST)
	public void mineToday(HttpServletRequest request, HttpServletResponse response, @RequestBody InputStaff input) {
		@SuppressWarnings("unused")
		DataAuthority dataAuthority = SessionUtils.getCurrDataAuthority(request);
		try {
			Date startMonth1 = DateUtil.getTimesMonthmorning();
			input.setStartMonth(new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(startMonth1));
			Date endMonth1 = DateUtil.getTimesMonthnight();
			input.setEndMonth(new SimpleDateFormat("yyyy-MM-dd").format(endMonth1));
			input.setCurrentTime(new Date());
			Date currentTime1 = input.getCurrentTime();
			input.setCurrTime(new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(currentTime1));
			input.setStartTime(new SimpleDateFormat("yyyy-MM-dd").format(currentTime1));
			input.setEndTime(input.getStartTime() + " 23:59:59");
			OutputAssess assess = assessService.listAssessForStaffId(input);
			OutputForMine mine = new OutputForMine();
			Integer temScore = assessService.selectScoreForMonth(input);
			mine.setCountScoreForMonth(0);
			if(temScore != null){
				mine.setCountScoreForMonth(temScore);
			}
			
			if (assess != null) {
				List<AssessInfo> list = infoService.getAssessInfoList(assess.getId());
				mine.setInfo(list);
				mine.setAssess(assess);
			}else{
				 assess = new OutputAssess();
				 List<AssessInfo> list = new ArrayList<>();
				 mine.setInfo(list);
				 mine.setAssess(assess);
			}
			respSuccessMsg(response, mine, "查询成功");
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			respErrorMsg(response, "系统异常");
		}
	}

	@RequestMapping(value = "/mineForWeek", method = RequestMethod.POST)
	public void mineForWeek(HttpServletRequest request, HttpServletResponse response, @RequestBody InputStaff input) {
		@SuppressWarnings("unused")
		DataAuthority dataAuthority = SessionUtils.getCurrDataAuthority(request);
		input.setCurrentTime(new Date());
		Date currentTime1 = input.getCurrentTime();
		input.setCurrTime(new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(currentTime1));
		Date startTime1 = DateUtil.startWeek(currentTime1);
		input.setStartTime(new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(startTime1));
		Date endTime1 = DateUtil.endWeek(currentTime1);
		input.setEndTime(new SimpleDateFormat("yyyy-MM-dd").format(endTime1) + " 23:59:59");
		try {
			List<OutputAssess> list = assessService.listAssessForStaffId1(input);
			OutputAssess assess = new OutputAssess();
			OutputForMine mine = new OutputForMine();
			Integer assessAdd = 0, assessReduce = 0, assessDay = 0;
			if (list != null && list.size() > 0) {
				for (int i = 0; i < list.size(); i++) {
					assessAdd += list.get(i).getAssessAdd();
					assessReduce += list.get(i).getAssessReduce();
					assessDay += list.get(i).getAssessDay();
				}
				assess.setName(list.get(0).getName());
				assess.setStaffId(list.get(0).getStaffId());
				assess.setAssessAdd(assessAdd);
				assess.setAssessReduce(assessReduce);
				assess.setAssessDay(assessDay);
				List<AssessInfo> info = infoService.getAssessInfoList(assess.getId());
				mine.setInfo(info);
				mine.setAssess(assess);
			}

			respSuccessMsg(response, mine, "查询成功");
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			respErrorMsg(response, "系统异常");
		}
	}

	@RequestMapping(value = "/mineForMonth", method = RequestMethod.POST)
	public void mineForMonth(HttpServletRequest request, HttpServletResponse response, @RequestBody InputStaff input) {
		@SuppressWarnings("unused")
		DataAuthority dataAuthority = SessionUtils.getCurrDataAuthority(request);
		input.setCurrentTime(new Date());
		Date currentTime1 = input.getCurrentTime();
		input.setCurrTime(new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(currentTime1));
		Date startTime1 = DateUtil.getTimesMonthmorning();
		input.setStartTime(new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(startTime1));
		Date endTime1 = DateUtil.getTimesMonthnight();
		input.setEndTime(new SimpleDateFormat("yyyy-MM-dd").format(endTime1));
		Date startMonth1 = DateUtil.getTimesMonthmorning();
		input.setStartMonth(new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(startMonth1));
		Date endMonth1 = DateUtil.getTimesMonthnight();
		input.setEndMonth(new SimpleDateFormat("yyyy-MM-dd").format(endMonth1));
		try {
			List<OutputAssess> list = assessService.listAssessForStaffId1(input);
			OutputAssess assess = new OutputAssess();
			OutputForMine mine = new OutputForMine();
			Integer temScore = assessService.selectScoreForMonth(input);
			mine.setCountScoreForMonth(0);
			if(temScore != null){
				mine.setCountScoreForMonth(temScore);
			}
			Integer assessAdd = 0, assessReduce = 0, assessDay = 0;
			if (list != null && list.size() > 0) {
				for (int i = 0; i < list.size(); i++) {
					assessAdd += list.get(i).getAssessAdd();
					assessReduce += list.get(i).getAssessReduce();
					assessDay += list.get(i).getAssessDay();
				}
				assess.setName(list.get(0).getName());
				assess.setStaffId(list.get(0).getStaffId());
				assess.setAssessAdd(assessAdd);
				assess.setAssessReduce(assessReduce);
				assess.setAssessDay(assessDay);
				List<AssessInfo> info = infoService.getAssessInfoList(assess.getId());
				mine.setInfo(info);
				mine.setAssess(assess);
			}

			respSuccessMsg(response, mine, "查询成功");
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			respErrorMsg(response, "系统异常");
		}

	}

	@RequestMapping(value = "/mineForLastMonth", method = RequestMethod.POST)
	public void mineForLastMonth(HttpServletRequest request, HttpServletResponse response,
			@RequestBody InputStaff input) {
		@SuppressWarnings("unused")
		DataAuthority dataAuthority = SessionUtils.getCurrDataAuthority(request);
		input.setCurrentTime(new Date());
		Date currentTime1 = input.getCurrentTime();
		input.setCurrTime(new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(currentTime1));
		input.setStartTime(DateUtil.lastMonth(currentTime1)[0]);
		input.setEndTime(DateUtil.lastMonth(currentTime1)[1] + " 23:59:59");
		input.setStartMonth(input.getStartTime());
		input.setEndMonth(input.getEndTime());
		try {
			List<OutputAssess> assessList = assessService.listAssessForStaffId1(input);
			OutputAssess assess = new OutputAssess(); 
			OutputForMine mine = new OutputForMine();
			Integer temScore = assessService.selectScoreForMonth(input);
			mine.setCountScoreForMonth(0);
			if(temScore != null){
				mine.setCountScoreForMonth(temScore);
			}
			if (assessList != null && assessList.size() > 0 ) {
				Integer assessAdd = 0, assessReduce = 0, assessDay = 0,score = 0;
				List<AssessInfo> info = new ArrayList<AssessInfo>();
				assess.setName(assessList.get(0).getName());
				assess.setGroupId(assessList.get(0).getGroupId());
				assess.setStaffId(assessList.get(0).getStaffId());
				for(int i = 0 ; i < assessList.size() ; i++){
					assessAdd += assessList.get(i).getAssessAdd();
					assessReduce += assessList.get(i).getAssessReduce();
					assessDay += assessList.get(i).getAssessDay();
					score += Integer.valueOf(assessList.get(i).getScore());
					List<AssessInfo> info1 = infoService.getAssessInfoList(assessList.get(i).getId());
					info.addAll(info1);
				}
				assess.setAssessAdd(assessAdd);
				assess.setAssessReduce(assessReduce);
				assess.setAssessDay(assessDay);
				assess.setScore(String.valueOf(score));
				mine.setAssess(assess);
				mine.setInfo(info);
			}else{
				 assess = new OutputAssess();
				 List<AssessInfo> list = new ArrayList<>();
				 mine.setInfo(list);
				 mine.setAssess(assess);
			}

			respSuccessMsg(response, mine, "查询成功");
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			respErrorMsg(response, "系统异常");
		}
	}

	@RequestMapping(value = "/mineForYestoady", method = RequestMethod.POST)
	public void mineForYestoady(HttpServletRequest request, HttpServletResponse response,
			@RequestBody InputStaff input) {
		@SuppressWarnings("unused")
		DataAuthority dataAuthority = SessionUtils.getCurrDataAuthority(request);
		input.setCurrentTime(new Date());
		Date currentTime1 = input.getCurrentTime();
		input.setCurrTime(new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(currentTime1));
		input.setStartTime(DateUtil.lastDay(currentTime1)[0]);
		input.setEndTime(DateUtil.lastDay(currentTime1)[0] + " 23:59:59");
		Date startMonth1 = DateUtil.getTimesMonthmorning();
		input.setStartMonth(new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(startMonth1));
		Date endMonth1 = DateUtil.getTimesMonthnight();
		input.setEndMonth(new SimpleDateFormat("yyyy-MM-dd").format(endMonth1));
		try {
			OutputAssess assess = assessService.listAssessForStaffId(input);
			OutputForMine mine = new OutputForMine();
			Integer temScore = assessService.selectScoreForMonth(input);
			mine.setCountScoreForMonth(0);
			if(temScore != null){
				mine.setCountScoreForMonth(temScore);
			}
			if (assess != null) {
				List<AssessInfo> info = infoService.getAssessInfoList(assess.getId());
				mine.setAssess(assess);
				mine.setInfo(info);
			}else{
				 assess = new OutputAssess();
				 List<AssessInfo> list = new ArrayList<>();
				 mine.setInfo(list);
				 mine.setAssess(assess);
			}
			respSuccessMsg(response, mine, "查询成功");
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			respErrorMsg(response, "系统异常");
		}

	}

	public static void main(String[] args) {
		String a = "111";
		String b = "110";
		System.err.println(a.compareTo(b));
	}
}
