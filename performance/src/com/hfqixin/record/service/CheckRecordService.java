package com.hfqixin.record.service;

import java.util.List;

import com.hfqixin.equipment.bean.CheckItem;
import com.hfqixin.ipqc.bean.IPQC;
import com.hfqixin.manager.bean.sys.PageMsg;
import com.hfqixin.record.bean.CheckRecord;

public interface CheckRecordService {

	public PageMsg<CheckRecord> listCheckRecord(CheckRecord record) throws Exception;

	public int insertCheckRecord(CheckRecord record) throws Exception;

	public int deleteCheckRecord(String id) throws Exception;

	public CheckRecord getCheckRecord(String id) throws Exception;

	public int updateCheckRecord(CheckRecord record) throws Exception;

	public List<CheckRecord> getCheckRecordList(CheckItem item) throws Exception;

	public List<CheckRecord> getCheckRecordList1(IPQC ipqc) throws Exception;

	public CheckRecord getCheckPlan(CheckItem item) throws Exception;

	public CheckRecord getCheckPlan1(String itemId) throws Exception;

	public PageMsg<CheckRecord> listCheckPlan(CheckRecord record) throws Exception;
	
	public Integer countDisqualification(CheckRecord record)throws Exception;
	
	public CheckRecord showCheckRecord(String itemId)throws Exception;

}
