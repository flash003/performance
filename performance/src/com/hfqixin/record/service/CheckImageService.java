package com.hfqixin.record.service;

import java.util.List;

import com.hfqixin.manager.bean.sys.PageMsg;
import com.hfqixin.record.bean.CheckImage;
import com.hfqixin.record.bean.CheckRecord;

public interface CheckImageService {

	public PageMsg<CheckImage> listCheckImage(CheckImage image) throws Exception;

	public int insertCheckImage(CheckImage image) throws Exception;

	public int deleteCheckImage(String id) throws Exception;

	public CheckImage getCheckImage(String id) throws Exception;

	public int updateCheckImage(CheckImage image) throws Exception;
	
	public List<CheckImage> getCheckImageList(CheckRecord record)throws Exception;
}
