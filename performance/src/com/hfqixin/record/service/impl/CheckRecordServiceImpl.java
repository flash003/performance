package com.hfqixin.record.service.impl;

import java.util.Date;
import java.util.List;
import java.util.UUID;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.hfqixin.equipment.bean.CheckItem;
import com.hfqixin.ipqc.bean.IPQC;
import com.hfqixin.manager.bean.sys.PageMsg;
import com.hfqixin.record.bean.CheckRecord;
import com.hfqixin.record.dao.CheckRecordDao;
import com.hfqixin.record.service.CheckRecordService;

@Service
public class CheckRecordServiceImpl implements CheckRecordService{
	
	@Autowired
	private CheckRecordDao dao;

	@Override
	public PageMsg<CheckRecord> listCheckRecord(CheckRecord record) throws Exception {
		List<CheckRecord> list = dao.listCheckRecord(record);
		PageMsg<CheckRecord> pageObj = new PageMsg<CheckRecord>();
		pageObj.setListResult(list);
		pageObj.setTotalNum(dao.countCheckRecord(record));
		pageObj.setPageNum(record.getSize());
		pageObj.setPage(record.getCurr());
		pageObj.setTotalPage();
		return pageObj;
	}

	@Override
	public int insertCheckRecord(CheckRecord record) throws Exception {
		record.setId(UUID.randomUUID().toString().replaceAll("-", ""));
		record.setInsertTime(new Date());
		record.setModifyTime(new Date());
		return dao.insertCheckRecord(record);
	}

	@Override
	public int deleteCheckRecord(String id) throws Exception {
		return dao.deleteCheckRecord(id);
	}

	@Override
	public CheckRecord getCheckRecord(String id) throws Exception {
		return dao.getCheckRecord(id);
	}

	@Override
	public int updateCheckRecord(CheckRecord record) throws Exception {
		record.setModifyTime(new Date());
		return dao.updateCheckRecord(record);
	}

	@Override
	public List<CheckRecord> getCheckRecordList(CheckItem item) throws Exception {
		return dao.getCheckRecordList(item);
	}

	@Override
	public List<CheckRecord> getCheckRecordList1(IPQC ipqc) throws Exception {
		return dao.getCheckRecordList1(ipqc);
	}

	@Override
	public CheckRecord getCheckPlan(CheckItem item) throws Exception {
		return dao.getCheckPlan(item);
	}

	@Override
	public CheckRecord getCheckPlan1(String itemId) throws Exception {
		return dao.getCheckPlan1(itemId);
	}

	@Override
	public PageMsg<CheckRecord> listCheckPlan(CheckRecord record) throws Exception {
		List<CheckRecord> list = dao.listCheckPlan(record);
		PageMsg<CheckRecord> pageObj = new PageMsg<CheckRecord>();
		pageObj.setListResult(list);
		pageObj.setTotalNum(dao.countCheckPlan(record));
		pageObj.setPageNum(record.getSize());
		pageObj.setPage(record.getCurr());
		pageObj.setTotalPage();
		return pageObj;
	}

	@Override
	public Integer countDisqualification(CheckRecord record) throws Exception {
		return dao.countDisqualification(record);
	}

	@Override
	public CheckRecord showCheckRecord(String itemId) throws Exception {
		return dao.showCheckRecord(itemId);
	}

	
}
