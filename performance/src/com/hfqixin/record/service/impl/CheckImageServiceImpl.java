package com.hfqixin.record.service.impl;

import java.util.List;
import java.util.UUID;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.hfqixin.manager.bean.sys.PageMsg;
import com.hfqixin.record.bean.CheckImage;
import com.hfqixin.record.bean.CheckRecord;
import com.hfqixin.record.dao.CheckImageDao;
import com.hfqixin.record.service.CheckImageService;

@Service
public class CheckImageServiceImpl implements CheckImageService {
	
	@Autowired
	private CheckImageDao dao;

	@Override
	public PageMsg<CheckImage> listCheckImage(CheckImage image) throws Exception {
		return null;
	}

	@Override
	public int insertCheckImage(CheckImage image) throws Exception {
		image.setId(UUID.randomUUID().toString().replaceAll("-", ""));
		return dao.insertCheckImage(image);
	}

	@Override
	public int deleteCheckImage(String id) throws Exception {
		return dao.deleteCheckImage(id);
	}

	@Override
	public CheckImage getCheckImage(String id) throws Exception {
		return dao.getCheckImage(id);
	}

	@Override
	public int updateCheckImage(CheckImage image) throws Exception {
		return dao.updateCheckImage(image);
	}

	@Override
	public List<CheckImage> getCheckImageList(CheckRecord record) throws Exception {
		return dao.getCheckImageList(record);
	}
}
