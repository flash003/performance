package com.hfqixin.record.bean;

import java.io.Serializable;

import com.hfqixin.manager.bean.form.PageForm;

public class CheckImage extends PageForm implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1949085763774345L;

	private String id;

	private String rId;

	private String picUrl;

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id == null ? null : id.trim();
	}

	public String getrId() {
		return rId;
	}

	public void setrId(String rId) {
		this.rId = rId;
	}

	public String getPicUrl() {
		return picUrl;
	}

	public void setPicUrl(String picUrl) {
		this.picUrl = picUrl == null ? null : picUrl.trim();
	}
}