package com.hfqixin.record.bean;

import java.io.Serializable;

import com.hfqixin.equipment.bean.CheckItem;
import com.hfqixin.equipment.bean.Equipment;

public class History implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 40954917972655725L;
	
	private String uId;
	
	private Equipment equipment;
	
	private CheckItem item;
	
	private CheckRecord record;
	

	public String getuId() {
		return uId;
	}

	public void setuId(String uId) {
		this.uId = uId;
	}

	public Equipment getEquipment() {
		return equipment;
	}

	public void setEquipment(Equipment equipment) {
		this.equipment = equipment;
	}

	public CheckItem getItem() {
		return item;
	}

	public void setItem(CheckItem item) {
		this.item = item;
	}

	public CheckRecord getRecord() {
		return record;
	}

	public void setRecord(CheckRecord record) {
		this.record = record;
	}
	
	

}
