package com.hfqixin.record.bean;

import java.io.Serializable;
import java.util.List;

public class OutputHistory implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 3207707353269381756L;

	private String result;

	private String checkTime;

	private List<CheckImage> imageList;

	private String content;

	private String remark;

	private String checkSite;

	private String equipmentName;

	private String itemName;

	public String getCheckSite() {
		return checkSite;
	}

	public void setCheckSite(String checkSite) {
		this.checkSite = checkSite;
	}

	public String getEquipmentName() {
		return equipmentName;
	}

	public void setEquipmentName(String equipmentName) {
		this.equipmentName = equipmentName;
	}

	public String getItemName() {
		return itemName;
	}

	public void setItemName(String itemName) {
		this.itemName = itemName;
	}

	public String getContent() {
		return content;
	}

	public void setContent(String content) {
		this.content = content;
	}

	public String getRemark() {
		return remark;
	}

	public void setRemark(String remark) {
		this.remark = remark;
	}

	public String getResult() {
		return result;
	}

	public void setResult(String result) {
		this.result = result;
	}

	public String getCheckTime() {
		return checkTime;
	}

	public void setCheckTime(String checkTime) {
		this.checkTime = checkTime;
	}

	public List<CheckImage> getImageList() {
		return imageList;
	}

	public void setImageList(List<CheckImage> imageList) {
		this.imageList = imageList;
	}

}
