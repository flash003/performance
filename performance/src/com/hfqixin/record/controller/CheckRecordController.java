package com.hfqixin.record.controller;

import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import com.hfqixin.equipment.bean.Equipment;
import com.hfqixin.equipment.service.EquipmentService;
import com.hfqixin.ipqc.bean.IPQC;
import com.hfqixin.ipqc.service.IPQCService;
import com.hfqixin.manager.annotation.AuthenPassport;
import com.hfqixin.manager.annotation.Permission;
import com.hfqixin.manager.bean.DataAuthority;
import com.hfqixin.manager.bean.sys.PageMsg;
import com.hfqixin.manager.controller.BaseController;
import com.hfqixin.manager.util.SessionUtils;
import com.hfqixin.record.bean.CheckImage;
import com.hfqixin.record.bean.CheckRecord;
import com.hfqixin.record.service.CheckImageService;
import com.hfqixin.record.service.CheckRecordService;

@Controller
@RequestMapping( value = "checkRecord")
public class CheckRecordController extends BaseController {

	@Autowired
	private CheckRecordService service; 
	
	@Autowired
	private IPQCService ipqcService;
	
	@Autowired
	private EquipmentService equipmentServcie;
	
	@Autowired
	private CheckImageService imageService;
	
	@AuthenPassport
	@Permission
	@RequestMapping(value = "/list", method = RequestMethod.GET)
	public ModelAndView list() throws Exception {
		ModelAndView mv = new ModelAndView();
		List<Equipment> list1 = equipmentServcie.getAllEquipment();
		mv.addObject("equipmentList", list1);
		List<IPQC> list2 = ipqcService.getIPQCList();
		mv.addObject("ipqcList", list2);
		mv.setViewName("/record/checkRecordList");
		return mv;
	}
	
	@AuthenPassport
	@Permission
	@RequestMapping(value = "/plan", method = RequestMethod.GET)
	public ModelAndView plan() throws Exception {
		ModelAndView mv = new ModelAndView();
		List<Equipment> list1 = equipmentServcie.getAllEquipment();
		mv.addObject("equipmentList", list1);
		List<IPQC> list2 = ipqcService.getIPQCList();
		mv.addObject("ipqcList", list2);
		mv.setViewName("/record/checkPlanList");
		return mv;
	}
	
	@RequestMapping(value = "/addEdit", method = RequestMethod.GET)
	public ModelAndView addEdit(HttpServletRequest request) throws Exception {
		ModelAndView mv = new ModelAndView();
		mv.addObject("id", request.getParameter("id"));
		mv.setViewName("/record/checkRecordAdd");
		return mv;
	}

	@RequestMapping(value = "/updateEdit", method = RequestMethod.GET)
	public ModelAndView updateEdit(HttpServletRequest request, HttpServletResponse response) throws Exception {
		ModelAndView mv = new ModelAndView();
		mv.addObject("id", request.getParameter("id"));
		mv.setViewName("/record/checkRecordUpdate");
		return mv;
	}
	
	@RequestMapping(value = "/detailEdit", method = RequestMethod.GET)
	public ModelAndView detailEdit(HttpServletRequest request, HttpServletResponse response) throws Exception {
		ModelAndView mv = new ModelAndView();
		mv.addObject("id", request.getParameter("id"));
		CheckRecord record1 = service.getCheckRecord(request.getParameter("id"));
		List<CheckImage> imageList= imageService.getCheckImageList(record1);
		mv.addObject("imageList", imageList);
		mv.setViewName("/record/checkRecordDetail");
		return mv;
	}
	
	@AuthenPassport
	@Permission
	@RequestMapping(value = "/listCheckRecord", method = RequestMethod.POST)
	public void listCheckRecord(HttpServletRequest request, HttpServletResponse response,
			CheckRecord record) {
		try {
			DataAuthority dataAuthority = SessionUtils.getCurrDataAuthority(request);
			record.setDataAuth(dataAuthority);
			PageMsg<CheckRecord> list = service.listCheckRecord(record);
			respSuccessMsg(response,list, "查询成功");
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
			respErrorMsg(response, "系统异常");
		}
	}
	
	@AuthenPassport
	@Permission
	@RequestMapping(value = "/listCheckPlan", method = RequestMethod.POST)
	public void listCheckPlan(HttpServletRequest request, HttpServletResponse response,
			CheckRecord record) {
		try {
			DataAuthority dataAuthority = SessionUtils.getCurrDataAuthority(request);
			record.setDataAuth(dataAuthority);
			PageMsg<CheckRecord> list = service.listCheckPlan(record);
			respSuccessMsg(response,list, "查询成功");
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
			respErrorMsg(response, "系统异常");
		}
	}
	
	
	@AuthenPassport
	@Permission
	@RequestMapping(value = "/addCheckRecord", method = RequestMethod.POST)
	public void addCheckRecord(HttpServletRequest request, HttpServletResponse response,CheckRecord record) {
			DataAuthority dataAuthority = SessionUtils.getCurrDataAuthority(request);
			record.setDataAuth(dataAuthority);
		try {
			service.insertCheckRecord(record);
			respSuccessMsg(response, null, "添加成功");

		} catch (Exception e) {
			// TODO handle exception
			e.printStackTrace();
			respErrorMsg(response, "系统异常");
		}
	}
	
	@AuthenPassport
	@RequestMapping(value = "/getCheckRecord", method = RequestMethod.POST)
	public void getCheckRecord(HttpServletRequest request, HttpServletResponse response, String id) {
		try {
			System.err.println("ajax" + id);
			CheckRecord record = service.getCheckRecord(id);
			respSuccessMsg(response, record, "查询成功");
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
			respErrorMsg(response, "系统异常");
		}
	}
	
	@RequestMapping(value = "/delete", method = RequestMethod.POST)
	public void delete(HttpServletRequest request, HttpServletResponse response, String id) {
		try {
			service.deleteCheckRecord(id);
			respSuccessMsg(response, null, "删除成功");
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
			respErrorMsg(response, "系统异常");
		}
	}
	
	@AuthenPassport
	@Permission
	@RequestMapping(value = "/updateEquipment", method = RequestMethod.POST)
	public void updateEquipment(HttpServletRequest request, HttpServletResponse response, CheckRecord record) {

		try {
			service.updateCheckRecord(record);
			respSuccessMsg(response, null, "修改成功");
		} catch (Exception e) {
			// TODO handle exception
			e.printStackTrace();
			respErrorMsg(response, "系统异常");
		}
	}
}
