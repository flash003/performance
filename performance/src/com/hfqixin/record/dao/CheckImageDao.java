package com.hfqixin.record.dao;

import java.util.List;

import org.springframework.stereotype.Repository;

import com.hfqixin.record.bean.CheckImage;
import com.hfqixin.record.bean.CheckRecord;

@Repository
public interface CheckImageDao {
	
	public List<CheckImage> listCheckImage(CheckImage image)throws Exception;
	
	public int countCheckImage(CheckImage image)throws Exception;
	
	public int insertCheckImage(CheckImage image)throws Exception;
	
	public int deleteCheckImage(String id)throws Exception;
	
	public CheckImage getCheckImage(String id)throws Exception;
	
	public int updateCheckImage(CheckImage image)throws Exception;
	
	public List<CheckImage> getCheckImageList(CheckRecord record)throws Exception;
}