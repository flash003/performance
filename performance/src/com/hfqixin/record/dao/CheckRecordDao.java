package com.hfqixin.record.dao;

import java.util.List;

import org.springframework.stereotype.Repository;

import com.hfqixin.equipment.bean.CheckItem;
import com.hfqixin.ipqc.bean.IPQC;
import com.hfqixin.record.bean.CheckRecord;

@Repository
public interface CheckRecordDao {

	public List<CheckRecord> listCheckRecord(CheckRecord record) throws Exception;

	public int countCheckRecord(CheckRecord record) throws Exception;

	public int insertCheckRecord(CheckRecord record) throws Exception;

	public int deleteCheckRecord(String id) throws Exception;

	public CheckRecord getCheckRecord(String id) throws Exception;

	public int updateCheckRecord(CheckRecord record) throws Exception;

	// 根据检查项id查询检查记录
	public List<CheckRecord> getCheckRecordList(CheckItem item) throws Exception;

	// 根据巡检员的id查询巡检记录
	public List<CheckRecord> getCheckRecordList1(IPQC ipqc) throws Exception;

	// 根据检查项id查询检查计划 status=1
	public CheckRecord getCheckPlan(CheckItem item) throws Exception;
	
	public CheckRecord getCheckPlan1(String itemId) throws Exception;
	
	//根据检查项id查询最近一条检查记录
	public CheckRecord showCheckRecord(String itemId)throws Exception;
	
	public List<CheckRecord> listCheckPlan(CheckRecord record) throws Exception;
	
	public int countCheckPlan(CheckRecord record) throws Exception;
	
	//查询不合格记录数量
	public Integer countDisqualification(CheckRecord record)throws Exception;

}