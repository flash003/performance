package com.hfqixin.staff.controller;

import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.hfqixin.assess.service.AssessService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import com.hfqixin.group.bean.Group;
import com.hfqixin.group.service.GroupService;
import com.hfqixin.manager.annotation.AuthenPassport;
import com.hfqixin.manager.annotation.Permission;
import com.hfqixin.manager.bean.DataAuthority;
import com.hfqixin.manager.bean.sys.PageMsg;
import com.hfqixin.manager.controller.BaseController;
import com.hfqixin.manager.util.SessionUtils;
import com.hfqixin.staff.bean.Staff;
import com.hfqixin.staff.service.StaffService;

@Controller
@RequestMapping(value = "staff")
public class StaffController extends BaseController {

	@Autowired
	private StaffService service;
	
	@Autowired
	private GroupService groupService;

	@Autowired
	private AssessService assessService;
	
	@AuthenPassport
	@Permission
	@RequestMapping(value = "/list", method = RequestMethod.GET)
	public ModelAndView list() {
		return new ModelAndView("/staff/staffList");
	}
	
	@AuthenPassport
	@Permission
	@RequestMapping(value = "/monitorList", method = RequestMethod.GET)
	public ModelAndView monitorList() {
		return new ModelAndView("/staff/monitorList");
	}
	
	@AuthenPassport
	@Permission
	@RequestMapping(value = "/waitAllotList", method = RequestMethod.GET)
	public ModelAndView waitAllotList() {
		return new ModelAndView("/staff/waitAllotList");
	}

	@RequestMapping(value = "/addEdit", method = RequestMethod.GET)
	public ModelAndView addEdit(HttpServletRequest request) {
		return new ModelAndView("/staff/staffAdd");
	}

	@RequestMapping(value = "/updateEdit", method = RequestMethod.GET)
	public ModelAndView updateEdit(HttpServletRequest request, HttpServletResponse response) throws Exception {
		ModelAndView mv = new ModelAndView();
		mv.addObject("id", request.getParameter("id"));
		mv.setViewName("/staff/staffUpdate");
		return mv;
	}
	
	@RequestMapping(value = "/allotEdit", method = RequestMethod.GET)
	public ModelAndView allotEdit(HttpServletRequest request, HttpServletResponse response) throws Exception {
		ModelAndView mv = new ModelAndView();
		mv.addObject("id", request.getParameter("id"));
		List<Group> list = groupService.getGroupList();
		mv.addObject("groupList", list);
		mv.setViewName("/staff/allot");
		return mv;
	}
	

	@AuthenPassport
	@Permission
	@RequestMapping(value = "/listMonitor", method = RequestMethod.POST)
	public void listMonitor(HttpServletRequest request, HttpServletResponse response,
			Staff staff) {
		try {
			DataAuthority dataAuthority = SessionUtils.getCurrDataAuthority(request);
			staff.setDataAuth(dataAuthority);
			PageMsg<Staff> list = service.listMonitor(staff);
			respSuccessMsg(response, list, "查询成功");
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
			respErrorMsg(response, "系统异常");
		}
	}
	
	@AuthenPassport
	@Permission
	@RequestMapping(value = "/listWaitAllot", method = RequestMethod.POST)
	public void listWaitAllot(HttpServletRequest request, HttpServletResponse response,
			Staff staff) {
		try {
			DataAuthority dataAuthority = SessionUtils.getCurrDataAuthority(request);
			staff.setDataAuth(dataAuthority);
			PageMsg<Staff> list = service.listWaitAllot(staff);
			respSuccessMsg(response, list, "查询成功");
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
			respErrorMsg(response, "系统异常");
		}
	}
	
	@AuthenPassport
	@Permission
	@RequestMapping(value = "/listStaff", method = RequestMethod.POST)
	public void listStaff(HttpServletRequest request, HttpServletResponse response,
			Staff staff) {
		try {
			DataAuthority dataAuthority = SessionUtils.getCurrDataAuthority(request);
			staff.setDataAuth(dataAuthority);
			PageMsg<Staff> list = service.listStaff(staff);
			respSuccessMsg(response, list, "查询成功");
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
			respErrorMsg(response, "系统异常");
		}
	}

	@AuthenPassport
	@Permission
	@RequestMapping(value = "/addStaff", method = RequestMethod.POST)
	public void addStaff(HttpServletRequest request, HttpServletResponse response, Staff staff) {
		try {
			service.insertStaff(staff);
			//assessService.initAssess(staff);
			respSuccessMsg(response, null, "添加成功");
		} catch (Exception e) {
			// TODO handle exception
			e.printStackTrace();
			respErrorMsg(response, "系统异常");
		}
	}
	
	@AuthenPassport
	@RequestMapping(value = "/getStaff", method = RequestMethod.POST)
	public void getStaff(HttpServletRequest request, HttpServletResponse response, String id) {
		try {
			System.err.println("ajax" + id);
			Staff staff = service.getStaff(id);
			respSuccessMsg(response, staff, "查询成功");
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
			respErrorMsg(response, "系统异常");
		}
	}
	
	@RequestMapping(value = "/delete", method = RequestMethod.POST)
	public void delete(HttpServletRequest request, HttpServletResponse response, String id) {
		try {
			service.deleteStaff(id);
			assessService.deleteAssessForStaffId(id);
			respSuccessMsg(response, null, "删除成功");
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
			respErrorMsg(response, "系统异常");
		}
	}
	
	@AuthenPassport
	@Permission
	@RequestMapping(value = "/updateStaff", method = RequestMethod.POST)
	public void updateStaff(HttpServletRequest request, HttpServletResponse response, Staff staff) {

		try {
			service.updateStaff(staff);
			respSuccessMsg(response, null, "添加成功");

		} catch (Exception e) {
			// TODO handle exception
			e.printStackTrace();
			respErrorMsg(response, "系统异常");
		}
	}
	
	@AuthenPassport
	@Permission
	@RequestMapping(value = "/cancelMonitor", method = RequestMethod.POST)
	public void cancelMonitor(HttpServletRequest request, HttpServletResponse response, String id) {

		try {
			service.cancelMonitor(id);
			respSuccessMsg(response, null, "撤销成功");

		} catch (Exception e) {
			// TODO handle exception
			e.printStackTrace();
			respErrorMsg(response, "系统异常");
		}
	}
	
	@AuthenPassport
	@Permission
	@RequestMapping(value = "/setMonitor", method = RequestMethod.POST)
	public void setMonitor(HttpServletRequest request, HttpServletResponse response, String id) {

		try {
			service.setMonitor(id);
			respSuccessMsg(response, null, "班长设定成功");

		} catch (Exception e) {
			// TODO handle exception
			e.printStackTrace();
			respErrorMsg(response, "系统异常");
		}
	}
	
	@AuthenPassport
	@Permission
	@RequestMapping(value = "/allot", method = RequestMethod.POST)
	public void allot(HttpServletRequest request, HttpServletResponse response, Staff staff) {
		try {
			Group group = groupService.getGroup(staff.getGroupid());
			staff.setGroupname(group.getName());
			service.allot(staff);
			Staff staff1 = service.getStaff(staff.getId());
			assessService.initAssess(staff1);
			staff.setAssessTime(new Date());
			service.updateStaff(staff);
			respSuccessMsg(response, null, "分配成功");

		} catch (Exception e) {
			// TODO handle exception
			e.printStackTrace();
			respErrorMsg(response, "系统异常");
		}
	}
}
