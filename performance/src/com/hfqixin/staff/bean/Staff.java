package com.hfqixin.staff.bean;

import java.io.Serializable;
import java.util.Date;

import com.hfqixin.manager.bean.form.PageForm;

public class Staff extends PageForm implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = -4886815172737145141L;

	private String id;

	private String name;

	private Integer sex;

	private String jobno;

	private String groupid;

	private String groupname;

	private String monitorid;

	private String monitorname;

	private Date inserttime;

	private Date modifytime;

	private Integer delFlag;

	private Integer ordernum;

	private Integer ismonitor;

	private Integer isallot;

	private String phone;

	private String loginname;

	private String loginpass;

	private Date assessTime;

	private Date registerTime;

	private String tempId;

	public String getPhone() {
		return phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

	public String getTempId() {
		return tempId;
	}

	public void setTempId(String tempId) {
		this.tempId = tempId;
	}

	public Integer getIsallot() {
		return isallot;
	}

	public void setIsallot(Integer isallot) {
		this.isallot = isallot;
	}

	public Date getAssessTime() {
		return assessTime;
	}

	public void setAssessTime(Date assessTime) {
		this.assessTime = assessTime;
	}

	public Date getRegisterTime() {
		return registerTime;
	}

	public void setRegisterTime(Date registerTime) {
		this.registerTime = registerTime;
	}

	public String getLoginname() {
		return loginname;
	}

	public void setLoginname(String loginname) {
		this.loginname = loginname;
	}

	public String getLoginpass() {
		return loginpass;
	}

	@Override
	public String toString() {
		return "Staff [id=" + id + ", name=" + name + ", sex=" + sex + ", jobno=" + jobno + ", groupid=" + groupid
				+ ", groupname=" + groupname + ", monitorid=" + monitorid + ", monitorname=" + monitorname
				+ ", inserttime=" + inserttime + ", modifytime=" + modifytime + ", delFlag=" + delFlag + ", ordernum="
				+ ordernum + ", ismonitor=" + ismonitor + ", isallot=" + isallot + ", loginname=" + loginname
				+ ", loginpass=" + loginpass + ", assessTime=" + assessTime + ", registerTime=" + registerTime
				+ ", tempId=" + tempId + "]";
	}

	public void setLoginpass(String loginpass) {
		this.loginpass = loginpass;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id == null ? null : id.trim();
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name == null ? null : name.trim();
	}

	public Integer getSex() {
		return sex;
	}

	public void setSex(Integer sex) {
		this.sex = sex;
	}

	public String getJobno() {
		return jobno;
	}

	public void setJobno(String jobno) {
		this.jobno = jobno == null ? null : jobno.trim();
	}

	public String getGroupid() {
		return groupid;
	}

	public void setGroupid(String groupid) {
		this.groupid = groupid == null ? null : groupid.trim();
	}

	public String getGroupname() {
		return groupname;
	}

	public void setGroupname(String groupname) {
		this.groupname = groupname == null ? null : groupname.trim();
	}

	public String getMonitorid() {
		return monitorid;
	}

	public void setMonitorid(String monitorid) {
		this.monitorid = monitorid == null ? null : monitorid.trim();
	}

	public String getMonitorname() {
		return monitorname;
	}

	public void setMonitorname(String monitorname) {
		this.monitorname = monitorname == null ? null : monitorname.trim();
	}

	public Date getInserttime() {
		return inserttime;
	}

	public void setInserttime(Date inserttime) {
		this.inserttime = inserttime;
	}

	public Date getModifytime() {
		return modifytime;
	}

	public void setModifytime(Date modifytime) {
		this.modifytime = modifytime;
	}

	public Integer getDelFlag() {
		return delFlag;
	}

	public void setDelFlag(Integer delFlag) {
		this.delFlag = delFlag;
	}

	public Integer getOrdernum() {
		return ordernum;
	}

	public void setOrdernum(Integer ordernum) {
		this.ordernum = ordernum;
	}

	public Integer getIsmonitor() {
		return ismonitor;
	}

	public void setIsmonitor(Integer ismonitor) {
		this.ismonitor = ismonitor;
	}
}