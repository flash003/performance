package com.hfqixin.staff.dao;

import com.hfqixin.staff.bean.InputStaff;
import com.hfqixin.staff.bean.Staff;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface StaffDao {

	public List<Staff> listStaff(Staff staff) throws Exception;

	public int countStaff(Staff staff) throws Exception;

	public int insertStaff(Staff staff) throws Exception;

	public int deleteStaff(String id) throws Exception;


	public Staff getStaff(String id) throws Exception;

	public int updateStaff(Staff staff) throws Exception;

	public int setMonitor(String id) throws Exception;

	public List<Staff> listMonitor(Staff staff) throws Exception;

	public int countMonitor(Staff staff) throws Exception;

	public int cancelMonitor(String id) throws Exception;

	public int cancelRelevance(String monitorId1) throws Exception;

	public List<Staff> listWaitAllot(Staff staff) throws Exception;

	public int countWaitAllot(Staff staff) throws Exception;

	public int allot(Staff staff) throws Exception;

	public List<Staff> getMonitorList() throws Exception;
	
	public List<Staff> getStaffNameList(InputStaff input)throws Exception; 
	
	public List<Staff> getStaffListForGroup(InputStaff input)throws Exception;
	
	//验证用户登陆
	public Staff checkUserLogin(Staff staff)throws Exception;
	

}