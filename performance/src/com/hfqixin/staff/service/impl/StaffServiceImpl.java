package com.hfqixin.staff.service.impl;

import java.util.Date;
import java.util.List;
import java.util.UUID;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.hfqixin.manager.bean.sys.PageMsg;
import com.hfqixin.staff.bean.InputStaff;
import com.hfqixin.staff.bean.Staff;
import com.hfqixin.staff.dao.StaffDao;
import com.hfqixin.staff.service.StaffService;

@Service
public class StaffServiceImpl implements StaffService {
	
	@Autowired
	private StaffDao dao;

	@Override
	public PageMsg<Staff> listStaff(Staff staff) throws Exception {
		List<Staff> list = dao.listStaff(staff);
		PageMsg<Staff> pageObj = new PageMsg<Staff>();
		pageObj.setListResult(list);
		pageObj.setTotalNum(dao.countStaff(staff));
		pageObj.setPageNum(staff.getSize());
		pageObj.setPage(staff.getCurr());
		pageObj.setTotalPage();
		return pageObj;
	}

	@Override
	public int insertStaff(Staff staff) throws Exception {
		staff.setInserttime(new Date());
		staff.setModifytime(new Date());
		staff.setId(UUID.randomUUID().toString().replaceAll("-", ""));
		return dao.insertStaff(staff);
	}

	@Override
	public int deleteStaff(String id) throws Exception {
		return dao.deleteStaff(id);
	}

	@Override
	public Staff getStaff(String id) throws Exception {
		return dao.getStaff(id);
	}

	@Override
	public int updateStaff(Staff staff) throws Exception {
		staff.setModifytime(new Date());
		return dao.updateStaff(staff);
	}

	@Override
	public PageMsg<Staff> listMonitor(Staff staff) throws Exception {
		List<Staff> list = dao.listMonitor(staff);
		PageMsg<Staff> pageObj = new PageMsg<Staff>();
		pageObj.setListResult(list);
		pageObj.setTotalNum(dao.countMonitor(staff));
		pageObj.setPageNum(staff.getSize());
		pageObj.setPage(staff.getCurr());
		pageObj.setTotalPage();
		return pageObj;
	}

	@Override
	public void cancelMonitor(String id) throws Exception {
		dao.cancelMonitor(id);
		dao.cancelRelevance(id);
	}

	@Override
	public PageMsg<Staff> listWaitAllot(Staff staff) throws Exception {
		List<Staff> list = dao.listWaitAllot(staff);
		PageMsg<Staff> pageObj = new PageMsg<Staff>();
		pageObj.setListResult(list);
		pageObj.setTotalNum(dao.countWaitAllot(staff));
		pageObj.setPageNum(staff.getSize());
		pageObj.setPage(staff.getCurr());
		pageObj.setTotalPage();
		return pageObj;
	}

	@Override
	public int allot(Staff staff) throws Exception {
		staff.setIsallot(1);
		return dao.allot(staff);
	}

	@Override
	public int setMonitor(String id) throws Exception {
		return dao.setMonitor(id);
	}

	@Override
	public List<Staff> getMonitorList() throws Exception {
		return dao.getMonitorList();
	}

	@Override
	public List<Staff> getStaffNameList(InputStaff input) throws Exception {
		return dao.getStaffNameList(input);
	}

	@Override
	public Staff checkUserLogin(Staff staff) throws Exception {
		return dao.checkUserLogin(staff);
	}

	@Override
	public List<Staff> getStaffListForGroup(InputStaff input) throws Exception {
		return dao.getStaffListForGroup(input);
	}
	
}
