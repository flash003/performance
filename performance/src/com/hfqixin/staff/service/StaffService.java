package com.hfqixin.staff.service;

import java.util.List;

import com.hfqixin.manager.bean.sys.PageMsg;
import com.hfqixin.staff.bean.InputStaff;
import com.hfqixin.staff.bean.Staff;

public interface StaffService {

	public PageMsg<Staff> listStaff(Staff staff) throws Exception;

	public int insertStaff(Staff staff) throws Exception;

	public int deleteStaff(String id) throws Exception;

	public Staff getStaff(String id) throws Exception;

	public int updateStaff(Staff staff) throws Exception;

	public int setMonitor(String id) throws Exception;

	public PageMsg<Staff> listMonitor(Staff staff) throws Exception;

	public void cancelMonitor(String id) throws Exception;

	public PageMsg<Staff> listWaitAllot(Staff staff) throws Exception;

	public int allot(Staff staff) throws Exception;
	
	public List<Staff> getMonitorList()throws Exception;
	
	public List<Staff> getStaffNameList(InputStaff input)throws Exception; 
	
	public List<Staff> getStaffListForGroup(InputStaff input)throws Exception;
	
	public Staff checkUserLogin(Staff staff)throws Exception;
	
}
