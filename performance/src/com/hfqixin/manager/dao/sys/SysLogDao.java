package com.hfqixin.manager.dao.sys;

import java.util.List;

import org.springframework.stereotype.Repository;

import com.hfqixin.manager.bean.form.SyslogForm;
import com.hfqixin.manager.bean.sys.SysLog;

/**
 * @title: SysLogDao2.java
 * @pacjage: com.hzdracom.manager.dao.sys
 * @description: TODO
 * @author: 楂樿緣
 * @date: 2017骞�4鏈�10鏃� 涓嬪崍3:55:30
 */

@Repository
public interface SysLogDao {
	
	public List<SysLog> getSyslogList(SyslogForm form);
	
	public int getSyslogListTotal(SyslogForm form);
	
	public int insertLog(SysLog form);
}
