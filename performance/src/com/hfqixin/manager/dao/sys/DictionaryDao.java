package com.hfqixin.manager.dao.sys;

import java.util.List;

import org.springframework.stereotype.Repository;

import com.hfqixin.manager.bean.Dictionary;

@Repository
public interface DictionaryDao {

	public List<Dictionary> listDictionary(Dictionary dictionary) throws Exception;

	public int countDictionary(Dictionary dictionary) throws Exception;

	public int addDictionary(Dictionary dictionary) throws Exception;

	public int deleteDictionary(String id) throws Exception;

	public Dictionary getDictionary(String id) throws Exception;

	public int updateDictionary(Dictionary dictionary) throws Exception;

	public Dictionary getValueByKey(String key) throws Exception;

}