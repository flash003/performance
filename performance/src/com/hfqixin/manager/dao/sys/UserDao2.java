package com.hfqixin.manager.dao.sys;

import java.util.List;

import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import com.hfqixin.manager.bean.form.UserForm;
import com.hfqixin.manager.bean.form.UserSettiingForm;
import com.hfqixin.manager.bean.sys.Role;
import com.hfqixin.manager.bean.sys.User;

/**
 * @title: UserDao2.java
 * @pacjage: com.hzdracom.manager.dao.sys
 * @description: TODO
 * @author: 楂樿緣
 * @date: 2017骞�4鏈�7鏃� 涓嬪崍3:04:56
 */

@Repository
public interface UserDao2 {
	
	public List<User> getUserList(UserForm user) throws Exception;
	
	public int getUserListTotal(UserForm user) throws Exception;
	
	public int addUser(User user) throws Exception;
	
	public int addUserRole(User user) throws Exception;
	
	public int delUserRole(User user) throws Exception;
	
	public List<User> getLoginName(User user);
	
	//鍔ㄦ�佸垽鏂櫥褰曞悕閲嶅
	public int selLoginname(String loginname);
	
	public int update(User user);
	
	List<Role> roleSelect(String officeId);
	
	User getUser(String userId);
	
	int delete(String userId);

	public void updateUserLoginIp(@Param("ip")String ip,@Param("userId") String userId);
	
	
	// 鐢ㄦ埛鐧诲綍  
	public User login(@Param("account")String account,@Param("pwd")String pwd);

	/**
	 * 鏇存柊鐢ㄦ埛涓汉淇℃伅 锛岀敤鎴疯嚜宸变慨鏀�
	 */
	public void updateUserInfo(UserSettiingForm form);

	/**
	 *  鏌ヨ瓒呯鎵�瀵瑰簲鐨�  鐢ㄦ埛ID鍒楄〃
	 */
	public List<String> querySupperAdminUserId();
	
}
