package com.hfqixin.manager.controller.sys;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import com.hfqixin.manager.annotation.AuthenPassport;
import com.hfqixin.manager.annotation.Permission;
import com.hfqixin.manager.bean.DataAuthority;
import com.hfqixin.manager.bean.Dictionary;
import com.hfqixin.manager.bean.sys.PageMsg;
import com.hfqixin.manager.controller.BaseController;
import com.hfqixin.manager.service.sys.DictionaryService;
import com.hfqixin.manager.util.SessionUtils;

@Controller
@RequestMapping(value = "dictionary")
public class DictionaryController extends BaseController {

	@Autowired
	private DictionaryService service;
	
	@AuthenPassport
	@Permission
	@RequestMapping(value = "/list", method = RequestMethod.GET)
	public ModelAndView list() {
		return new ModelAndView("/sys/dictionaryList");
	}
	
	@RequestMapping(value = "/addEdit", method = RequestMethod.GET)
	public ModelAndView addEdit(HttpServletRequest request) {
		return new ModelAndView("/sys/dictionaryAdd");
	}

	@RequestMapping(value = "/updateEdit", method = RequestMethod.GET)
	public ModelAndView updateEdit(HttpServletRequest request, HttpServletResponse response) throws Exception {
		ModelAndView mv = new ModelAndView();
		mv.addObject("id", request.getParameter("id"));
		mv.setViewName("/sys/dictionaryUpdate");
		return mv;
	}
	
	@AuthenPassport
	@Permission
	@RequestMapping(value = "/listDictionary", method = RequestMethod.POST)
	public void listDictionary(HttpServletRequest request, HttpServletResponse response,
			Dictionary dictionary) {
		try {
			DataAuthority dataAuthority = SessionUtils.getCurrDataAuthority(request);
			dictionary.setDataAuth(dataAuthority);
			PageMsg<Dictionary> list = service.listDictionary(dictionary);
			respSuccessMsg(response,list, "查询成功");
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
			respErrorMsg(response, "系统异常");
		}
	}
	
	@AuthenPassport
	@Permission
	@RequestMapping(value = "/addDictionary", method = RequestMethod.POST)
	public void addDictionary(HttpServletRequest request, HttpServletResponse response, Dictionary dictionary) {

		try {
			service.addDictionary(dictionary);
			respSuccessMsg(response, null, "添加成功");

		} catch (Exception e) {
			// TODO handle exception
			e.printStackTrace();
			respErrorMsg(response, "系统异常");
		}
	}
	
	@AuthenPassport
	@RequestMapping(value = "/getDictionary", method = RequestMethod.POST)
	public void getDictionary(HttpServletRequest request, HttpServletResponse response, String id) {
		try {
			System.err.println("ajax" + id);
			Dictionary dictionary = service.getDictionary(id);
			respSuccessMsg(response, dictionary, "查询成功");
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
			respErrorMsg(response, "系统异常");
		}
	}
	
	@RequestMapping(value = "/delete", method = RequestMethod.POST)
	public void delete(HttpServletRequest request, HttpServletResponse response, String id) {
		try {
			service.deleteDictionary(id);
			respSuccessMsg(response, null, "删除成功");
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
			respErrorMsg(response, "系统异常");
		}
	}
	
	@AuthenPassport
	@Permission
	@RequestMapping(value = "/updateDictionary", method = RequestMethod.POST)
	public void updateDictionary(HttpServletRequest request, HttpServletResponse response, Dictionary dictionary) {

		try {
			service.updateDictionary(dictionary);
			respSuccessMsg(response, null, "修改成功");
		} catch (Exception e) {
			// TODO handle exception
			e.printStackTrace();
			respErrorMsg(response, "系统异常");
		}
	}
}
