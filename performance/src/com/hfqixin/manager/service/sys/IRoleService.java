package com.hfqixin.manager.service.sys;

import java.util.List;

import com.hfqixin.manager.bean.DataAuthority;
import com.hfqixin.manager.bean.form.RoleForm;
import com.hfqixin.manager.bean.sys.Menu;
import com.hfqixin.manager.bean.sys.Office;
import com.hfqixin.manager.bean.sys.PageMsg;
import com.hfqixin.manager.bean.sys.Role;


public interface IRoleService {
	
	
	public PageMsg<Role> getRoleList(RoleForm form) throws Exception;//
	
	public List<Menu> getMenuList(DataAuthority dataAuthority) throws Exception;//
	
	public List<Office> getOfficeList(DataAuthority dataAuthority) throws Exception;//
	
	public boolean doAddOrUpdate(Role role) throws Exception;//
	
	public Role getRole(int id) throws Exception;//
	
	/**
	 *  查询数据权限
	 */
	public List<Integer> queryRoleOfficeIds(Integer roleId);
	
	public boolean delete(String roleId) throws Exception;//
}
