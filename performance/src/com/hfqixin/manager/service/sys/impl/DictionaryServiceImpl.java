package com.hfqixin.manager.service.sys.impl;

import java.util.List;
import java.util.UUID;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.hfqixin.manager.bean.Dictionary;
import com.hfqixin.manager.bean.sys.PageMsg;
import com.hfqixin.manager.dao.sys.DictionaryDao;
import com.hfqixin.manager.service.sys.DictionaryService;

@Service
public class DictionaryServiceImpl implements DictionaryService {

	@Autowired
	private DictionaryDao dao;

	@Override
	public PageMsg<Dictionary> listDictionary(Dictionary dictionary) throws Exception {
		List<Dictionary> list = dao.listDictionary(dictionary);
		PageMsg<Dictionary> pageObj = new PageMsg<Dictionary>();
		pageObj.setListResult(list);
		pageObj.setTotalNum(dao.countDictionary(dictionary));
		pageObj.setPageNum(dictionary.getSize());
		pageObj.setPage(dictionary.getCurr());
		pageObj.setTotalPage();
		return pageObj;
	}

	@Override
	public int addDictionary(Dictionary dictionary) throws Exception {
		dictionary.setId(UUID.randomUUID().toString().replaceAll("-", ""));
		return dao.addDictionary(dictionary);
	}

	@Override
	public int deleteDictionary(String id) throws Exception {
		return dao.deleteDictionary(id);
	}

	@Override
	public Dictionary getDictionary(String id) throws Exception {
		return dao.getDictionary(id);
	}

	@Override
	public int updateDictionary(Dictionary dictionary) throws Exception {
		return dao.updateDictionary(dictionary);
	}

	@Override
	public Dictionary getValueByKey(String key) throws Exception {
		return dao.getValueByKey(key);
	}
}
