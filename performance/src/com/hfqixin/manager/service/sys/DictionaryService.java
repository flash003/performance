package com.hfqixin.manager.service.sys;

import com.hfqixin.manager.bean.Dictionary;
import com.hfqixin.manager.bean.sys.PageMsg;

public interface DictionaryService {

	public PageMsg<Dictionary> listDictionary(Dictionary dictionary) throws Exception;

	public int addDictionary(Dictionary dictionary) throws Exception;

	public int deleteDictionary(String id) throws Exception;

	public Dictionary getDictionary(String id) throws Exception;

	public int updateDictionary(Dictionary dictionary) throws Exception;
	
	public Dictionary getValueByKey(String key) throws Exception;
}
