package com.hfqixin.group.service.impl;

import java.util.Date;
import java.util.List;
import java.util.UUID;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.hfqixin.group.bean.Group;
import com.hfqixin.group.dao.GroupDao;
import com.hfqixin.group.service.GroupService;
import com.hfqixin.manager.bean.sys.PageMsg;

@Service
public class GroupServiceImpl implements GroupService {

	@Autowired
	private GroupDao dao;
	
	@Override
	public PageMsg<Group> listGroup(Group group) throws Exception {
		List<Group> list = dao.listGroup(group);
		PageMsg<Group> pageObj = new PageMsg<Group>();
		pageObj.setListResult(list);
		pageObj.setTotalNum(dao.countGroup(group));
		pageObj.setPageNum(group.getSize());
		pageObj.setPage(group.getCurr());
		pageObj.setTotalPage();
		return pageObj;
	}

	@Override
	public int insertGroup(Group group) throws Exception {
		group.setId(UUID.randomUUID().toString().replaceAll("-", ""));
		group.setInsertTime(new Date());
		group.setModifyTime(new Date());
		return dao.insertGroup(group);
	}

	@Override
	public int deleteGroup(String id) throws Exception {
		return dao.deleteGroup(id);
	}

	@Override
	public Group getGroup(String id) throws Exception {
		return dao.getGroup(id);
	}

	@Override
	public int updateGroup(Group group) throws Exception {
		group.setModifyTime(new Date());
		return dao.updateGroup(group);
	}

	@Override
	public List<Group> getGroupList() throws Exception {
		return dao.getGroupList();
	}

}
