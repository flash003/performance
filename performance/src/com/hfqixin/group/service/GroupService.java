package com.hfqixin.group.service;

import java.util.List;

import com.hfqixin.group.bean.Group;
import com.hfqixin.manager.bean.sys.PageMsg;

public interface GroupService {

	public PageMsg<Group> listGroup(Group group) throws Exception;

	public int insertGroup(Group group) throws Exception;

	public int deleteGroup(String id) throws Exception;

	public Group getGroup(String id) throws Exception;

	public int updateGroup(Group group) throws Exception;

	public List<Group> getGroupList() throws Exception;
}
