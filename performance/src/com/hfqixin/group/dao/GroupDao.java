package com.hfqixin.group.dao;

import java.util.List;

import org.springframework.stereotype.Repository;

import com.hfqixin.group.bean.Group;

@Repository
public interface GroupDao {

	public List<Group> listGroup(Group group) throws Exception;

	public int countGroup(Group group) throws Exception;

	public int insertGroup(Group group) throws Exception;

	public int deleteGroup(String id) throws Exception;

	public Group getGroup(String id) throws Exception;

	public int updateGroup(Group group) throws Exception;
	
	public List<Group> getGroupList()throws Exception; 
}