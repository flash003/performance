package com.hfqixin.group.controller;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import com.hfqixin.group.bean.Group;
import com.hfqixin.group.service.GroupService;
import com.hfqixin.manager.annotation.AuthenPassport;
import com.hfqixin.manager.annotation.Permission;
import com.hfqixin.manager.bean.DataAuthority;
import com.hfqixin.manager.bean.sys.PageMsg;
import com.hfqixin.manager.controller.BaseController;
import com.hfqixin.manager.util.SessionUtils;
import com.hfqixin.staff.bean.Staff;
import com.hfqixin.staff.service.StaffService;

@Controller
@RequestMapping( value = "group")
public class GroupController extends BaseController {

	@Autowired
	private GroupService service;
	
	@Autowired
	private StaffService satffService;
	
	@AuthenPassport
	@Permission
	@RequestMapping(value = "/list", method = RequestMethod.GET)
	public ModelAndView list() {
		return new ModelAndView("/group/groupList");
	}
	
	@RequestMapping(value = "/addEdit", method = RequestMethod.GET)
	public ModelAndView addEdit(HttpServletRequest request) throws Exception {
		ModelAndView mv = new ModelAndView();
		mv.addObject("id", request.getParameter("id"));
		List<Staff> list = satffService.getMonitorList();
		mv.addObject("monitorList", list);
		mv.setViewName("/group/groupAdd");
		return mv;
	}

	@RequestMapping(value = "/updateEdit", method = RequestMethod.GET)
	public ModelAndView updateEdit(HttpServletRequest request, HttpServletResponse response) throws Exception {
		ModelAndView mv = new ModelAndView();
		mv.addObject("id", request.getParameter("id"));
		Group group = service.getGroup(request.getParameter("id"));
		List<Staff> list = satffService.getMonitorList();
		Map<String,Object> map = new HashMap<String,Object>();
		map.put("group", group);
		map.put("monitorList", list);
		mv.addObject("map", map);
		mv.setViewName("/group/groupUpdate");
		return mv;
	}
	
	@AuthenPassport
	@Permission
	@RequestMapping(value = "/listGroup", method = RequestMethod.POST)
	public void listGroup(HttpServletRequest request, HttpServletResponse response,
			Group group) {
		try {
			DataAuthority dataAuthority = SessionUtils.getCurrDataAuthority(request);
			dataAuthority.setSupperAdmin(true);
			group.setDataAuth(dataAuthority);
			PageMsg<Group> list = service.listGroup(group);
			respSuccessMsg(response,list, "查询成功");
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
			respErrorMsg(response, "系统异常");
		}
	}
	
	@AuthenPassport
	@Permission
	@RequestMapping(value = "/addGroup", method = RequestMethod.POST)
	public void addGroup(HttpServletRequest request, HttpServletResponse response,Group group) {
			DataAuthority dataAuthority = SessionUtils.getCurrDataAuthority(request);
			group.setDataAuth(dataAuthority);
		try {
			service.insertGroup(group);
			respSuccessMsg(response, null, "添加成功");

		} catch (Exception e) {
			// TODO handle exception
			e.printStackTrace();
			respErrorMsg(response, "系统异常");
		}
	}
	
	@AuthenPassport
	@RequestMapping(value = "/getGroup", method = RequestMethod.POST)
	public void getGroup(HttpServletRequest request, HttpServletResponse response, String id) {
		try {
			System.err.println("ajax" + id);
			Group group = service.getGroup(id);
			respSuccessMsg(response, group, "查询成功");
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
			respErrorMsg(response, "系统异常");
		}
	}
	
	@RequestMapping(value = "/delete", method = RequestMethod.POST)
	public void delete(HttpServletRequest request, HttpServletResponse response, String id) {
		try {
			service.deleteGroup(id);
			respSuccessMsg(response, null, "删除成功");
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
			respErrorMsg(response, "系统异常");
		}
	}
	
	@AuthenPassport
	@Permission
	@RequestMapping(value = "/updateGroup", method = RequestMethod.POST)
	public void updateGroup(HttpServletRequest request, HttpServletResponse response, Group group) {

		try {
			
			service.updateGroup(group);
			respSuccessMsg(response, null, "修改成功");
		} catch (Exception e) {
			// TODO handle exception
			e.printStackTrace();
			respErrorMsg(response, "系统异常");
		}
	}
}
