package com.hfqixin.equipment.bean;

import java.io.Serializable;

public class Relationship implements Serializable {
    /**
	 * 
	 */
	private static final long serialVersionUID = -7074940011572174375L;

	private String id;

    private String equipmentId;

    private String ipqcId;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id == null ? null : id.trim();
    }

    public String getEquipmentId() {
        return equipmentId;
    }

    public void setEquipmentId(String equipmentId) {
        this.equipmentId = equipmentId == null ? null : equipmentId.trim();
    }

    public String getIpqcId() {
        return ipqcId;
    }

    public void setIpqcId(String ipqcId) {
        this.ipqcId = ipqcId == null ? null : ipqcId.trim();
    }
}