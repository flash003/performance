package com.hfqixin.equipment.bean;

import java.io.Serializable;
import java.util.List;

public class OutputHome implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 8862107291272827390L;

	private String id;

	private String equipmentName;

	private String picUrl;

	private String site;

	private Integer total;

	private Integer checkedItem;

	private Integer disqualification;

	private List<CheckItem> itemList;

	public Integer getDisqualification() {
		return disqualification;
	}

	public void setDisqualification(Integer disqualification) {
		this.disqualification = disqualification;
	}

	public Integer getTotal() {
		return total;
	}

	public void setTotal(Integer total) {
		this.total = total;
	}

	public Integer getCheckedItem() {
		return checkedItem;
	}

	public void setCheckedItem(Integer checkedItem) {
		this.checkedItem = checkedItem;
	}

	public String getSite() {
		return site;
	}

	public void setSite(String site) {
		this.site = site;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getEquipmentName() {
		return equipmentName;
	}

	public void setEquipmentName(String equipmentName) {
		this.equipmentName = equipmentName;
	}

	public String getPicUrl() {
		return picUrl;
	}

	public void setPicUrl(String picUrl) {
		this.picUrl = picUrl;
	}

	public List<CheckItem> getItemList() {
		return itemList;
	}

	public void setItemList(List<CheckItem> itemList) {
		this.itemList = itemList;
	}

}
