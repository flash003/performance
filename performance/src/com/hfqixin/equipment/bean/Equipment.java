package com.hfqixin.equipment.bean;

import java.io.Serializable;
import java.util.Date;

import com.hfqixin.manager.bean.form.PageForm;

public class Equipment extends PageForm implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = -6789138257146707466L;

	private String id;

	private String name;

	private String equipmentNo;

	private String ipqcId;

	private String ipqcName;

	private String information;

	private String picUrl1;

	private String picUrl2;

	private String picUrl3;

	private Date insertTime;

	private Date modifyTime;

	private Integer delFlag;

	private Integer status;

	private String site;

	private String startTime2;

	private String endTime2;

	public String getStartTime2() {
		return startTime2;
	}

	public void setStartTime2(String startTime2) {
		this.startTime2 = startTime2;
	}

	public String getEndTime2() {
		return endTime2;
	}

	public void setEndTime2(String endTime2) {
		this.endTime2 = endTime2;
	}

	public String getSite() {
		return site;
	}

	public void setSite(String site) {
		this.site = site;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id == null ? null : id.trim();
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name == null ? null : name.trim();
	}

	public String getEquipmentNo() {
		return equipmentNo;
	}

	public void setEquipmentNo(String equipmentNo) {
		this.equipmentNo = equipmentNo == null ? null : equipmentNo.trim();
	}

	public String getIpqcId() {
		return ipqcId;
	}

	public void setIpqcId(String ipqcId) {
		this.ipqcId = ipqcId == null ? null : ipqcId.trim();
	}

	public String getIpqcName() {
		return ipqcName;
	}

	public void setIpqcName(String ipqcName) {
		this.ipqcName = ipqcName == null ? null : ipqcName.trim();
	}

	public String getInformation() {
		return information;
	}

	public void setInformation(String information) {
		this.information = information;
	}

	public String getPicUrl1() {
		return picUrl1;
	}

	public void setPicUrl1(String picUrl1) {
		this.picUrl1 = picUrl1 == null ? null : picUrl1.trim();
	}

	public String getPicUrl2() {
		return picUrl2;
	}

	public void setPicUrl2(String picUrl2) {
		this.picUrl2 = picUrl2 == null ? null : picUrl2.trim();
	}

	public String getPicUrl3() {
		return picUrl3;
	}

	public void setPicUrl3(String picUrl3) {
		this.picUrl3 = picUrl3 == null ? null : picUrl3.trim();
	}

	public Date getInsertTime() {
		return insertTime;
	}

	public void setInsertTime(Date insertTime) {
		this.insertTime = insertTime;
	}

	public Date getModifyTime() {
		return modifyTime;
	}

	public void setModifyTime(Date modifyTime) {
		this.modifyTime = modifyTime;
	}

	public Integer getDelFlag() {
		return delFlag;
	}

	public void setDelFlag(Integer delFlag) {
		this.delFlag = delFlag;
	}

	public Integer getStatus() {
		return status;
	}

	public void setStatus(Integer status) {
		this.status = status;
	}
}