package com.hfqixin.equipment.bean;

import java.io.Serializable;
import java.util.Date;

import com.hfqixin.manager.bean.form.PageForm;

public class CheckItem extends PageForm implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = -7518138954456419456L;

	private String id;

	private String equipmentId;

	private String equipmentName;

	private String name;

	private String information;

	private Integer frequency;

	private Date checkTime;

	private String checkPoint;

	private String checkSite;

	private String iqpcId;

	private String iqpcName;

	private Date insertTime;

	private Date modifyTime;

	private Integer delFlag;

	private Integer status;

	private String remark;

	private String nextCheckTime;

	private Date deadLine;

	private String content;

	private boolean checkToday;
	
	private Date checkTime1;
	

	public Date getCheckTime1() {
		return checkTime1;
	}

	public void setCheckTime1(Date checkTime1) {
		this.checkTime1 = checkTime1;
	}

	public boolean isCheckToday() {
		return checkToday;
	}

	public void setCheckToday(boolean checkToday) {
		this.checkToday = checkToday;
	}

	public String getContent() {
		return content;
	}

	public void setContent(String content) {
		this.content = content;
	}

	public Date getDeadLine() {
		return deadLine;
	}

	public void setDeadLine(Date deadLine) {
		this.deadLine = deadLine;
	}

	public String getNextCheckTime() {
		return nextCheckTime;
	}

	public void setNextCheckTime(String nextCheckTime) {
		this.nextCheckTime = nextCheckTime;
	}

	public String getInformation() {
		return information;
	}

	public void setInformation(String information) {
		this.information = information;
	}

	public String getRemark() {
		return remark;
	}

	public void setRemark(String remark) {
		this.remark = remark;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id == null ? null : id.trim();
	}

	public String getEquipmentId() {
		return equipmentId;
	}

	public void setEquipmentId(String equipmentId) {
		this.equipmentId = equipmentId == null ? null : equipmentId.trim();
	}

	public String getEquipmentName() {
		return equipmentName;
	}

	public void setEquipmentName(String equipmentName) {
		this.equipmentName = equipmentName == null ? null : equipmentName.trim();
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name == null ? null : name.trim();
	}

	public Integer getFrequency() {
		return frequency;
	}

	public void setFrequency(Integer frequency) {
		this.frequency = frequency;
	}

	public Date getCheckTime() {
		return checkTime;
	}

	public void setCheckTime(Date checkTime) {
		this.checkTime = checkTime;
	}

	public String getCheckPoint() {
		return checkPoint;
	}

	public void setCheckPoint(String checkPoint) {
		this.checkPoint = checkPoint == null ? null : checkPoint.trim();
	}

	public String getCheckSite() {
		return checkSite;
	}

	public void setCheckSite(String checkSite) {
		this.checkSite = checkSite == null ? null : checkSite.trim();
	}

	public String getIqpcId() {
		return iqpcId;
	}

	public void setIqpcId(String iqpcId) {
		this.iqpcId = iqpcId == null ? null : iqpcId.trim();
	}

	public String getIqpcName() {
		return iqpcName;
	}

	public void setIqpcName(String iqpcName) {
		this.iqpcName = iqpcName == null ? null : iqpcName.trim();
	}

	public Date getInsertTime() {
		return insertTime;
	}

	public void setInsertTime(Date insertTime) {
		this.insertTime = insertTime;
	}

	public Date getModifyTime() {
		return modifyTime;
	}

	public void setModifyTime(Date modifyTime) {
		this.modifyTime = modifyTime;
	}

	public Integer getDelFlag() {
		return delFlag;
	}

	public void setDelFlag(Integer delFlag) {
		this.delFlag = delFlag;
	}

	public Integer getStatus() {
		return status;
	}

	public void setStatus(Integer status) {
		this.status = status;
	}
}