package com.hfqixin.equipment.bean;

import java.io.Serializable;
import java.util.List;

public class OutputEquipment implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 6662975149877292822L;

	private Equipment equipment;

	private List<CheckItem> item;

	public Equipment getEquipment() {
		return equipment;
	}

	public void setEquipment(Equipment equipment) {
		this.equipment = equipment;
	}

	public List<CheckItem> getItem() {
		return item;
	}

	public void setItem(List<CheckItem> item) {
		this.item = item;
	}

}
