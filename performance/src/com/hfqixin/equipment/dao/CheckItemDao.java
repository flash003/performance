package com.hfqixin.equipment.dao;

import java.util.List;

import org.springframework.stereotype.Repository;

import com.hfqixin.equipment.bean.CheckItem;
import com.hfqixin.equipment.bean.Equipment;
import com.hfqixin.ipqc.bean.IPQC;

@Repository
public interface CheckItemDao {

	public List<CheckItem> listCheckItem(CheckItem item) throws Exception;

	public int countCheckItem(CheckItem item) throws Exception;

	public int insertCheckItem(CheckItem item) throws Exception;

	public int deleteCheckItem(String id) throws Exception;

	public CheckItem getCheckItem(String id) throws Exception;

	public int updateCheckItem(CheckItem item) throws Exception;

	// 根据巡检员id获取所有检查项
	public List<CheckItem> getCheckItemList(IPQC ipqc) throws Exception;
	
	public List<CheckItem> getCheckItemList1(IPQC ipqc) throws Exception;

	// 根据设备id 查询检查项
	public List<CheckItem> getCheckItemList2(Equipment equipment) throws Exception;
	
	//根据设备查询今日要检查的检查项
	public List<CheckItem> getCheckItemList3(Equipment equipment)throws Exception;
	
	//根据设备id查询所有的检查项并按紧急程度排序
	public List<CheckItem> getCheckItemList4(Equipment equipment)throws Exception;
	
	

}