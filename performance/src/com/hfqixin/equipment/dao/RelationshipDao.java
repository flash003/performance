package com.hfqixin.equipment.dao;

import org.springframework.stereotype.Repository;

import com.hfqixin.equipment.bean.Relationship;

@Repository
public interface RelationshipDao {
  
	public int insertRelationship(Relationship relationship)throws Exception;
	
	public Relationship getRelationship(Relationship relationship)throws Exception;
}