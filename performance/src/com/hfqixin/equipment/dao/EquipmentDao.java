package com.hfqixin.equipment.dao;

import java.util.List;

import org.springframework.stereotype.Repository;

import com.hfqixin.equipment.bean.Equipment;
import com.hfqixin.ipqc.bean.IPQC;

@Repository
public interface EquipmentDao {

	 List<Equipment> listEquipment(Equipment equipment) throws Exception;

	 int countEquipment(Equipment equipment) throws Exception;

	 int insertEquipment(Equipment equipment) throws Exception;

	 int deleteEquipment(String id) throws Exception;

	 Equipment getEquipment(String id) throws Exception;

	 int updateEquipment(Equipment equipment) throws Exception;

	// 通过巡检员id查询管理的设备
	 List<Equipment> getEquipmentList(IPQC ipqc) throws Exception;

	// 查询所有设备
	 List<Equipment> getAllEquipment() throws Exception;

}