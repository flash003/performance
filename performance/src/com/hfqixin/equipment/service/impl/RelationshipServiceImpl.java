package com.hfqixin.equipment.service.impl;

import java.util.UUID;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.hfqixin.equipment.bean.Relationship;
import com.hfqixin.equipment.dao.RelationshipDao;
import com.hfqixin.equipment.service.RelationshipService;

@Service
public class RelationshipServiceImpl implements RelationshipService {

	@Autowired
	private RelationshipDao dao;
	
	@Override
	public int insertRelationship(Relationship relationship) throws Exception {
		relationship.setId(UUID.randomUUID().toString().replaceAll("-", ""));
		return dao.insertRelationship(relationship);
	}

	@Override
	public Relationship getRelationship(Relationship relationship) throws Exception {
		return dao.getRelationship(relationship);
	}

}
