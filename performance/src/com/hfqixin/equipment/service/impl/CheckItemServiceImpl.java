package com.hfqixin.equipment.service.impl;

import java.util.Date;
import java.util.List;
import java.util.UUID;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.hfqixin.equipment.bean.CheckItem;
import com.hfqixin.equipment.bean.Equipment;
import com.hfqixin.equipment.dao.CheckItemDao;
import com.hfqixin.equipment.service.CheckItemService;
import com.hfqixin.ipqc.bean.IPQC;
import com.hfqixin.manager.bean.sys.PageMsg;

@Service
public class CheckItemServiceImpl implements CheckItemService {

	@Autowired
	private CheckItemDao dao;
	
	@Override
	public PageMsg<CheckItem> listCheckItem(CheckItem item) throws Exception {
		List<CheckItem> list = dao.listCheckItem(item);
		PageMsg<CheckItem> pageObj = new PageMsg<CheckItem>();
		pageObj.setListResult(list);
		pageObj.setTotalNum(dao.countCheckItem(item));
		pageObj.setPageNum(item.getSize());
		pageObj.setPage(item.getCurr());
		pageObj.setTotalPage();
		return pageObj;
	}

	@Override
	public int insertCheckItem(CheckItem item) throws Exception {
		item.setId(UUID.randomUUID().toString().replaceAll("-", ""));
		item.setInsertTime(new Date());
		item.setModifyTime(new Date());
		return dao.insertCheckItem(item);
	}

	@Override
	public int deleteCheckItem(String id) throws Exception {
		return dao.deleteCheckItem(id);
	}

	@Override
	public CheckItem getCheckItem(String id) throws Exception {
		return dao.getCheckItem(id);
	}

	@Override
	public int updateCheckItem(CheckItem item) throws Exception {
		item.setModifyTime(new Date());
		return dao.updateCheckItem(item);
	}

	@Override
	public List<CheckItem> getCheckItemList(IPQC ipqc) throws Exception {
		return dao.getCheckItemList(ipqc);
	}

	@Override
	public List<CheckItem> getCheckItemList2(Equipment equipment) throws Exception {
		return dao.getCheckItemList2(equipment);
	}

	@Override
	public List<CheckItem> getCheckItemList1(IPQC ipqc) throws Exception {
		return dao.getCheckItemList1(ipqc);
	}

	@Override
	public List<CheckItem> getCheckItemList3(Equipment equipment) throws Exception {
		return dao.getCheckItemList3(equipment);
	}

	@Override
	public List<CheckItem> getCheckItemList4(Equipment equipment) throws Exception {
		return dao.getCheckItemList4(equipment);
	}

}
