package com.hfqixin.equipment.service.impl;

import java.util.Date;
import java.util.List;
import java.util.UUID;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.hfqixin.equipment.bean.Equipment;
import com.hfqixin.equipment.dao.EquipmentDao;
import com.hfqixin.equipment.service.EquipmentService;
import com.hfqixin.ipqc.bean.IPQC;
import com.hfqixin.manager.bean.sys.PageMsg;

@Service
public class EquipmentServiceImpl implements EquipmentService {
	
	@Autowired
	private EquipmentDao dao;

	@Override
	public PageMsg<Equipment> listEquipment(Equipment equipment) throws Exception {
		List<Equipment> list = dao.listEquipment(equipment);
		PageMsg<Equipment> pageObj = new PageMsg<Equipment>();
		pageObj.setListResult(list);
		pageObj.setTotalNum(dao.countEquipment(equipment));
		pageObj.setPageNum(equipment.getSize());
		pageObj.setPage(equipment.getCurr());
		pageObj.setTotalPage();
		return pageObj;
	}

	@Override
	public int insertEquipment(Equipment equipment) throws Exception {
		equipment.setId(UUID.randomUUID().toString().replaceAll("-", ""));
		equipment.setInsertTime(new Date());
		equipment.setModifyTime(new Date());
		return dao.insertEquipment(equipment);
	}

	@Override
	public int deleteEquipment(String id) throws Exception {
		return dao.deleteEquipment(id);
	}

	@Override
	public Equipment getEquipment(String id) throws Exception {
		return dao.getEquipment(id);
	}

	@Override
	public int updateEquipment(Equipment equipment) throws Exception {
		equipment.setModifyTime(new Date());
		return dao.updateEquipment(equipment);
	}

	@Override
	public List<Equipment> getEquipmentList(IPQC ipqc) throws Exception {
		return dao.getEquipmentList(ipqc);
	}

	@Override
	public List<Equipment> getAllEquipment() throws Exception {
		return dao.getAllEquipment();
	}
	
	
}
