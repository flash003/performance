package com.hfqixin.equipment.service;

import java.util.List;

import com.hfqixin.equipment.bean.Equipment;
import com.hfqixin.ipqc.bean.IPQC;
import com.hfqixin.manager.bean.sys.PageMsg;

public interface EquipmentService {

	public PageMsg<Equipment> listEquipment(Equipment equipment) throws Exception;

	public int insertEquipment(Equipment equipment) throws Exception;

	public int deleteEquipment(String id) throws Exception;

	public Equipment getEquipment(String id) throws Exception;

	public int updateEquipment(Equipment equipment) throws Exception;
	
	public List<Equipment> getEquipmentList(IPQC ipqc)throws Exception;
	
	public List<Equipment> getAllEquipment() throws Exception;
}
