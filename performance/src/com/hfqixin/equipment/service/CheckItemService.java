package com.hfqixin.equipment.service;

import java.util.List;

import com.hfqixin.equipment.bean.CheckItem;
import com.hfqixin.equipment.bean.Equipment;
import com.hfqixin.ipqc.bean.IPQC;
import com.hfqixin.manager.bean.sys.PageMsg;

public interface CheckItemService {

	public PageMsg<CheckItem> listCheckItem(CheckItem item) throws Exception;

	public int insertCheckItem(CheckItem item) throws Exception;

	public int deleteCheckItem(String id) throws Exception;

	public CheckItem getCheckItem(String id) throws Exception;

	public int updateCheckItem(CheckItem item) throws Exception;

	public List<CheckItem> getCheckItemList(IPQC ipqc) throws Exception;

	public List<CheckItem> getCheckItemList2(Equipment equipment) throws Exception;

	public List<CheckItem> getCheckItemList1(IPQC ipqc) throws Exception;
	
	public List<CheckItem> getCheckItemList3(Equipment equipment)throws Exception;
	
	public List<CheckItem> getCheckItemList4(Equipment equipment)throws Exception;
	

}
