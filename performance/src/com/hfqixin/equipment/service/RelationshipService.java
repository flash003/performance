package com.hfqixin.equipment.service;

import com.hfqixin.equipment.bean.Relationship;

public interface RelationshipService {

	public int insertRelationship(Relationship relationship)throws Exception;
	
	public Relationship getRelationship(Relationship relationship)throws Exception;
}
