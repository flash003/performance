package com.hfqixin.equipment.controller;

import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import com.hfqixin.equipment.bean.Equipment;
import com.hfqixin.equipment.bean.Relationship;
import com.hfqixin.equipment.service.EquipmentService;
import com.hfqixin.equipment.service.RelationshipService;
import com.hfqixin.ipqc.bean.IPQC;
import com.hfqixin.ipqc.service.IPQCService;
import com.hfqixin.manager.annotation.AuthenPassport;
import com.hfqixin.manager.annotation.Permission;
import com.hfqixin.manager.bean.DataAuthority;
import com.hfqixin.manager.bean.sys.PageMsg;
import com.hfqixin.manager.controller.BaseController;
import com.hfqixin.manager.util.SessionUtils;

@Controller
@RequestMapping( value = "equipment")
public class EquipmentController extends BaseController {
	
	@Autowired
	private EquipmentService service; 
	
	@Autowired
	private IPQCService ipqcService;
	
	@Autowired
	private RelationshipService relationshipService;
	
	@AuthenPassport
	@Permission
	@RequestMapping(value = "/list", method = RequestMethod.GET)
	public ModelAndView list() {
		return new ModelAndView("/equipment/equipmentList");
	}
	
	@RequestMapping(value = "/addEdit", method = RequestMethod.GET)
	public ModelAndView addEdit(HttpServletRequest request) throws Exception {
		ModelAndView mv = new ModelAndView();
		mv.addObject("id", request.getParameter("id"));
		List<IPQC> list = ipqcService.getIPQCList();
		mv.addObject("ipqcList", list);
		mv.setViewName("/equipment/equipmentAdd");
		return mv;
	}
	
	@RequestMapping(value = "/allotEdit", method = RequestMethod.GET)
	public ModelAndView allotEdit(HttpServletRequest request, HttpServletResponse response) throws Exception {
		ModelAndView mv = new ModelAndView();
		mv.addObject("equipmentId", request.getParameter("id"));
		List<IPQC> list = ipqcService.getIPQCList();
		mv.addObject("ipqcList", list);
		mv.setViewName("/equipment/allot");
		return mv;
	}

	@RequestMapping(value = "/updateEdit", method = RequestMethod.GET)
	public ModelAndView updateEdit(HttpServletRequest request, HttpServletResponse response) throws Exception {
		ModelAndView mv = new ModelAndView();
		mv.addObject("id", request.getParameter("id"));
		List<IPQC> list = ipqcService.getIPQCList();
		mv.addObject("ipqcList", list);
		Equipment equipment = service.getEquipment(request.getParameter("id"));
		mv.addObject("equipment", equipment);
		mv.setViewName("/equipment/equipmentUpdate");
		return mv;
	}
	
	@AuthenPassport
	@Permission
	@RequestMapping(value = "/listEquipment", method = RequestMethod.POST)
	public void listEquipment(HttpServletRequest request, HttpServletResponse response,
			Equipment equipment) {
		try {
			DataAuthority dataAuthority = SessionUtils.getCurrDataAuthority(request);
			equipment.setDataAuth(dataAuthority);
			PageMsg<Equipment> list = service.listEquipment(equipment);
			respSuccessMsg(response,list, "查询成功");
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
			respErrorMsg(response, "系统异常");
		}
	}
	
	@AuthenPassport
	@Permission
	@RequestMapping(value = "/addEquipment", method = RequestMethod.POST)
	public void addEquipment(HttpServletRequest request, HttpServletResponse response,Equipment equipment) {
			DataAuthority dataAuthority = SessionUtils.getCurrDataAuthority(request);
			equipment.setDataAuth(dataAuthority);
		try {
			IPQC ipqc = ipqcService.getIPQC(equipment.getIpqcId());
			if(ipqc != null){
				equipment.setIpqcName(ipqc.getName());
			}
			
			service.insertEquipment(equipment);
			respSuccessMsg(response, null, "添加成功");

		} catch (Exception e) {
			// TODO handle exception
			e.printStackTrace();
			respErrorMsg(response, "系统异常");
		}
	}
	
	@AuthenPassport
	@RequestMapping(value = "/getEquipment", method = RequestMethod.POST)
	public void getEquipment(HttpServletRequest request, HttpServletResponse response, String id) {
		try {
			System.err.println("ajax" + id);
			Equipment equipment = service.getEquipment(id);
			respSuccessMsg(response, equipment, "查询成功");
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
			respErrorMsg(response, "系统异常");
		}
	}
	
	
	@RequestMapping(value = "/delete", method = RequestMethod.POST)
	public void delete(HttpServletRequest request, HttpServletResponse response, String id) {
		try {
			service.deleteEquipment(id);
			respSuccessMsg(response, null, "删除成功");
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
			respErrorMsg(response, "系统异常");
		}
	}
	
	@AuthenPassport
	@Permission
	@RequestMapping(value = "/updateEquipment", method = RequestMethod.POST)
	public void updateEquipment(HttpServletRequest request, HttpServletResponse response, Equipment equipment) {

		try {
			IPQC ipqc = ipqcService.getIPQC(equipment.getIpqcId());
			if(ipqc != null){
				equipment.setIpqcName(ipqc.getName());
			}
			service.updateEquipment(equipment);
			respSuccessMsg(response, null, "修改成功");
		} catch (Exception e) {
			// TODO handle exception
			e.printStackTrace();
			respErrorMsg(response, "系统异常");
		}
	}
	
	@AuthenPassport
	@Permission
	@RequestMapping(value = "/allot", method = RequestMethod.POST)
	public void allot(HttpServletRequest request, HttpServletResponse response, Relationship relationship) {
		try {
			Relationship relationship1 = relationshipService.getRelationship(relationship);
			Equipment equipment = service.getEquipment(relationship.getEquipmentId());
			Equipment equipment1 = new Equipment();
			equipment1.setId(equipment.getId());
			IPQC ipqc = ipqcService.getIPQC(relationship.getIpqcId());
			if(null == equipment.getIpqcName() || equipment.getIpqcName() == "" ){
				equipment.setIpqcName("");
			}else{
				equipment.setIpqcName(equipment.getIpqcName()+"、");
			}
			if(relationship1 == null){
				relationshipService.insertRelationship(relationship);
				equipment1.setIpqcName(equipment.getIpqcName()+ipqc.getName());
				service.updateEquipment(equipment1);
				respSuccessMsg(response, null, "分配成功");
			}else{
				respErrorMsg(response, "分配失败（设备已经分配给该巡检员！）");
			}
		} catch (Exception e) {
			// TODO handle exception
			e.printStackTrace();
			respErrorMsg(response, "系统异常");
		}
	}
}
