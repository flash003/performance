package com.hfqixin.equipment.controller;

import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import com.hfqixin.equipment.bean.CheckItem;
import com.hfqixin.equipment.bean.Equipment;
import com.hfqixin.equipment.service.CheckItemService;
import com.hfqixin.equipment.service.EquipmentService;
import com.hfqixin.manager.annotation.AuthenPassport;
import com.hfqixin.manager.annotation.Permission;
import com.hfqixin.manager.bean.DataAuthority;
import com.hfqixin.manager.bean.sys.PageMsg;
import com.hfqixin.manager.controller.BaseController;
import com.hfqixin.manager.util.SessionUtils;

@Controller
@RequestMapping(value = "checkItem")
public class CheckItemController extends BaseController {

	@Autowired
	private CheckItemService service;
	
	@Autowired
	private EquipmentService equipmentService;
	
	@AuthenPassport
	@Permission
	@RequestMapping(value = "/list", method = RequestMethod.GET)
	public ModelAndView list() {
		return new ModelAndView("/equipment/checkItemList");
	}
	
	@RequestMapping(value = "/addEdit", method = RequestMethod.GET)
	public ModelAndView addEdit(HttpServletRequest request) throws Exception {
		ModelAndView mv = new ModelAndView();
		mv.addObject("id", request.getParameter("id"));
		List<Equipment> list = equipmentService.getAllEquipment();
		mv.addObject("equipmentList", list);
		mv.setViewName("/equipment/checkItemAdd");
		return mv;
	}

	@RequestMapping(value = "/updateEdit", method = RequestMethod.GET)
	public ModelAndView updateEdit(HttpServletRequest request, HttpServletResponse response) throws Exception {
		ModelAndView mv = new ModelAndView();
		mv.addObject("id", request.getParameter("id"));
		List<Equipment> list = equipmentService.getAllEquipment();
		mv.addObject("equipmentList", list);
		CheckItem item = service.getCheckItem(request.getParameter("id"));
		mv.addObject("item", item);
		mv.setViewName("/equipment/checkItemUpdate");
		return mv;
	}
	
	@AuthenPassport
	@Permission
	@RequestMapping(value = "/listCheckItem", method = RequestMethod.POST)
	public void listCheckItem(HttpServletRequest request, HttpServletResponse response,
			CheckItem  item) {
		try {
			DataAuthority dataAuthority = SessionUtils.getCurrDataAuthority(request);
			item.setDataAuth(dataAuthority);
			PageMsg<CheckItem> list = service.listCheckItem(item);
			respSuccessMsg(response,list, "查询成功");
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
			respErrorMsg(response, "系统异常");
		}
	}
	
	@AuthenPassport
	@Permission
	@RequestMapping(value = "/addCheckItem", method = RequestMethod.POST)
	public void addCheckItem(HttpServletRequest request, HttpServletResponse response,CheckItem item) {
			DataAuthority dataAuthority = SessionUtils.getCurrDataAuthority(request);
			item.setDataAuth(dataAuthority);
		try {
			Equipment equipment = equipmentService.getEquipment(item.getEquipmentId());
			if(equipment != null){
				item.setEquipmentName(equipment.getName());
				item.setInformation(equipment.getInformation());
				item.setCheckSite(equipment.getSite());
			}
			service.insertCheckItem(item);
			respSuccessMsg(response, null, "添加成功");

		} catch (Exception e) {
			// TODO handle exception
			e.printStackTrace();
			respErrorMsg(response, "系统异常");
		}
	}
	
	@AuthenPassport
	@RequestMapping(value = "/getCheckItem", method = RequestMethod.POST)
	public void getCheckItem(HttpServletRequest request, HttpServletResponse response, String id) {
			
		try {
			System.err.println("ajax" + id);
			CheckItem item = service.getCheckItem(id);
			respSuccessMsg(response, item, "查询成功");
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
			respErrorMsg(response, "系统异常");
		}
	}
	
	@RequestMapping(value = "/delete", method = RequestMethod.POST)
	public void delete(HttpServletRequest request, HttpServletResponse response, String id) {
		try {
			service.deleteCheckItem(id);
			respSuccessMsg(response, null, "删除成功");
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
			respErrorMsg(response, "系统异常");
		}
	}
	
	@AuthenPassport
	@Permission
	@RequestMapping(value = "/updateCheckItem", method = RequestMethod.POST)
	public void updateCheckItem(HttpServletRequest request, HttpServletResponse response, CheckItem item) {
		DataAuthority dataAuthority = SessionUtils.getCurrDataAuthority(request);
		item.setDataAuth(dataAuthority);
		try {
			Equipment equipment = equipmentService.getEquipment(item.getEquipmentId());
			if(equipment != null){
				item.setEquipmentName(equipment.getName());
			}
			service.updateCheckItem(item);
			respSuccessMsg(response, null, "修改成功");
		} catch (Exception e) {
			// TODO handle exception
			e.printStackTrace();
			respErrorMsg(response, "系统异常");
		}
	}
}
