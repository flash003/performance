package com.hfqixin.assess.dao;

import java.util.List;

import org.springframework.stereotype.Repository;

import com.hfqixin.assess.bean.AssessInfo;

@Repository
public interface AssessInfoDao {

	public List<AssessInfo> listAssessInfo(AssessInfo info) throws Exception;

	public int countAssessInfo(AssessInfo info) throws Exception;

	public int insertAssessInfo(AssessInfo info) throws Exception;

	public int deleteAssessInfo(String id) throws Exception;

	public AssessInfo getAssessInfo(String id) throws Exception;
	
	public List<AssessInfo> getAssessInfoList(String assessId)throws Exception;

	public int updateAssessInfo(AssessInfo info) throws Exception;
	
	//通过绩效考核得id查询具体考核信息
	public List<AssessInfo> listAssessInfoDetail(String id)throws Exception; 
}