package com.hfqixin.assess.dao;

import java.util.List;

import org.springframework.stereotype.Repository;

import com.hfqixin.assess.bean.Assess;
import com.hfqixin.assess.bean.OutputAssess;
import com.hfqixin.staff.bean.InputStaff;
import com.hfqixin.staff.bean.Staff;

@Repository
public interface AssessDao {

	public List<Assess> listAssess(Assess assess) throws Exception;

	public int countAssess(Assess assess) throws Exception;

	public List<Assess> listMonitorAssess(Assess assess) throws Exception;

	public int countMonitorAssess(Assess assess) throws Exception;

	public int insertAssess(Assess assess) throws Exception;

	public int deleteAssess(String id) throws Exception;

	int deleteAssessForStaffId(String staffId) throws Exception;

	public Assess getAssess(String id) throws Exception;

	public int updateAssess(Assess assess) throws Exception;

	public OutputAssess listAssessForStaffId(InputStaff input) throws Exception;

	public List<OutputAssess> listAssessForStaffId1(InputStaff input) throws Exception;

	public int initAssess(Staff staff) throws Exception;

	public List<OutputAssess> listAssessForMonitorDay(InputStaff input) throws Exception;

	public List<OutputAssess> listAssessForGroupIdDay(InputStaff input) throws Exception;

	public int clockIn(OutputAssess assess) throws Exception;

	public int insertMonitorAssess(Assess assess) throws Exception;

	public int updateMonitorAssess(Assess assess) throws Exception;

	//根据考核月份查询考核信息
	public Assess getMonitorAssessForMonth(Assess assess) throws Exception;
	
	//查询本月累计得分
	public Integer selectScoreForMonth(InputStaff input)throws Exception;
}