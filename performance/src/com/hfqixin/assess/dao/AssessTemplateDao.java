package com.hfqixin.assess.dao;

import java.util.List;

import org.springframework.stereotype.Repository;

import com.hfqixin.assess.bean.AssessTemplate;

@Repository
public interface AssessTemplateDao {

	public List<AssessTemplate> listAssessTemplate(AssessTemplate template) throws Exception;

	public int countAssessTemplate(AssessTemplate template) throws Exception;

	public int insertAssessTemplate(AssessTemplate template) throws Exception;

	public int deleteAssessTemplate(String id) throws Exception;

	public AssessTemplate getAssessTemplate(String id) throws Exception;

	public int updateAssessTemplate(AssessTemplate template) throws Exception;
}