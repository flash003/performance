package com.hfqixin.assess.bean;

import java.io.Serializable;
import java.util.Date;

import com.hfqixin.manager.bean.form.PageForm;

public class AssessTemplate extends PageForm implements Serializable {
    /**
	 * 
	 */
	private static final long serialVersionUID = 2371510246587412516L;

	private String id;

    private String assessName;

    private Integer value;

    private String content;

    private Date insertTime;

    private Date modifyTime;

    private Integer delFlag;

    private Integer statu;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id == null ? null : id.trim();
    }

    public String getAssessName() {
        return assessName;
    }

    public void setAssessName(String assessName) {
        this.assessName = assessName == null ? null : assessName.trim();
    }

    public Integer getValue() {
        return value;
    }

    public void setValue(Integer value) {
        this.value = value;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content == null ? null : content.trim();
    }

    public Date getInsertTime() {
        return insertTime;
    }

    public void setInsertTime(Date insertTime) {
        this.insertTime = insertTime;
    }

    public Date getModifyTime() {
        return modifyTime;
    }

    public void setModifyTime(Date modifyTime) {
        this.modifyTime = modifyTime;
    }

    public Integer getDelFlag() {
        return delFlag;
    }

    public void setDelFlag(Integer delFlag) {
        this.delFlag = delFlag;
    }

    public Integer getStatu() {
        return statu;
    }

    public void setStatu(Integer statu) {
        this.statu = statu;
    }
}