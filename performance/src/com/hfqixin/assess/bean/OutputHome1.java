package com.hfqixin.assess.bean;

import java.io.Serializable;
import java.util.List;

import com.hfqixin.equipment.bean.OutputHome;

public class OutputHome1 implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -5630441549132793374L;

	private Integer checkTodayTotol;

	private Integer overdueTotal;

	private List<OutputHome> homeList;

	public Integer getCheckTodayTotol() {
		return checkTodayTotol;
	}

	public void setCheckTodayTotol(Integer checkTodayTotol) {
		this.checkTodayTotol = checkTodayTotol;
	}

	public Integer getOverdueTotal() {
		return overdueTotal;
	}

	public void setOverdueTotal(Integer overdueTotal) {
		this.overdueTotal = overdueTotal;
	}

	public List<OutputHome> getHomeList() {
		return homeList;
	}

	public void setHomeList(List<OutputHome> homeList) {
		this.homeList = homeList;
	}

}
