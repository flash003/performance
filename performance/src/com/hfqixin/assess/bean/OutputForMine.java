package com.hfqixin.assess.bean;

import java.io.Serializable;
import java.util.List;

/**
 * @author dev-003
 *
 */
public class OutputForMine implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -1600433041896877036L;

	private Integer countScoreForMonth;

	private OutputAssess assess;

	private List<AssessInfo> info;

	public Integer getCountScoreForMonth() {
		return countScoreForMonth;
	}

	public void setCountScoreForMonth(Integer countScoreForMonth) {
		this.countScoreForMonth = countScoreForMonth;
	}

	public OutputAssess getAssess() {
		return assess;
	}

	public void setAssess(OutputAssess assess) {
		this.assess = assess;
	}

	public List<AssessInfo> getInfo() {
		return info;
	}

	public void setInfo(List<AssessInfo> info) {
		this.info = info;
	}

}
