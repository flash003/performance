package com.hfqixin.assess.bean;

import java.io.Serializable;
import java.util.List;



public class OutputMineForMonitor implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -4148447291678860218L;

	private OutputAssess assess;

	private List<OutputAssess> AssessList;

	public List<OutputAssess> getAssessList() {
		return AssessList;
	}

	public void setAssessList(List<OutputAssess> assessList) {
		AssessList = assessList;
	}

	public OutputAssess getAssess() {
		return assess;
	}

	public void setAssess(OutputAssess assess) {
		this.assess = assess;
	}

	

}
