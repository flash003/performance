package com.hfqixin.assess.bean;

import java.io.Serializable;
import java.util.Date;

import com.hfqixin.manager.bean.form.PageForm;

public class Assess extends PageForm implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -2179169505160929956L;

	private String id;

	private String staffId;

	private String name;

	private String jobno;

	private Integer assessDay;

	private Integer score;

	private Integer standard;

	private Integer assessAdd;

	private Integer assessReduce;

	private String assessPerson;

	private String assessPersonId;

	private Date insertTime;

	private Date modifyTime;

	private Integer delFlag;

	private Integer orderNum;

	private String jobTime;

	private Date clockInTime;

	private String groupId;

	private String groupName;

	private Integer clockInFlag;

	private Integer assessFlag;

	private String startTime;

	private String endTime;

	private String addExplain;

	private String reduceExplain;

	private String assessMonth;

	public Integer getStandard() {
		return standard;
	}

	public void setStandard(Integer standard) {
		this.standard = standard;
	}

	public String getAssessMonth() {
		return assessMonth;
	}

	public void setAssessMonth(String assessMonth) {
		this.assessMonth = assessMonth;
	}

	public String getAddExplain() {
		return addExplain;
	}

	public void setAddExplain(String addExplain) {
		this.addExplain = addExplain;
	}

	public String getReduceExplain() {
		return reduceExplain;
	}

	public void setReduceExplain(String reduceExplain) {
		this.reduceExplain = reduceExplain;
	}

	public String getGroupName() {
		return groupName;
	}

	public void setGroupName(String groupName) {
		this.groupName = groupName;
	}

	public String getStartTime() {
		return startTime;
	}

	public void setStartTime(String startTime) {
		this.startTime = startTime;
	}

	public String getEndTime() {
		return endTime;
	}

	public void setEndTime(String endTime) {
		this.endTime = endTime;
	}

	public Integer getAssessFlag() {
		return assessFlag;
	}

	public void setAssessFlag(Integer assessFlag) {
		this.assessFlag = assessFlag;
	}

	public Integer getClockInFlag() {
		return clockInFlag;
	}

	public void setClockInFlag(Integer clockInFlag) {
		this.clockInFlag = clockInFlag;
	}

	public String getGroupId() {
		return groupId;
	}

	public void setGroupId(String groupId) {
		this.groupId = groupId;
	}

	public Date getClockInTime() {
		return clockInTime;
	}

	public void setClockInTime(Date clockInTime) {
		this.clockInTime = clockInTime;
	}

	public String getJobTime() {
		return jobTime;
	}

	public void setJobTime(String jobTime) {
		this.jobTime = jobTime;
	}

	public Integer getAssessAdd() {
		return assessAdd;
	}

	public void setAssessAdd(Integer assessAdd) {
		this.assessAdd = assessAdd;
	}

	public Integer getAssessReduce() {
		return assessReduce;
	}

	public void setAssessReduce(Integer assessReduce) {
		this.assessReduce = assessReduce;
	}

	public String getStaffId() {
		return staffId;
	}

	public void setStaffId(String staffId) {
		this.staffId = staffId;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id == null ? null : id.trim();
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name == null ? null : name.trim();
	}

	public String getJobno() {
		return jobno;
	}

	public void setJobno(String jobno) {
		this.jobno = jobno == null ? null : jobno.trim();
	}

	public Integer getAssessDay() {
		return assessDay;
	}

	public void setAssessDay(Integer assessDay) {
		this.assessDay = assessDay;
	}

	public Integer getScore() {
		return score;
	}

	public void setScore(Integer score) {
		this.score = score;
	}

	public String getAssessPerson() {
		return assessPerson;
	}

	public void setAssessPerson(String assessPerson) {
		this.assessPerson = assessPerson == null ? null : assessPerson.trim();
	}

	public String getAssessPersonId() {
		return assessPersonId;
	}

	public void setAssessPersonId(String assessPersonId) {
		this.assessPersonId = assessPersonId == null ? null : assessPersonId.trim();
	}

	public Date getInsertTime() {
		return insertTime;
	}

	public void setInsertTime(Date insertTime) {
		this.insertTime = insertTime;
	}

	public Date getModifyTime() {
		return modifyTime;
	}

	public void setModifyTime(Date modifyTime) {
		this.modifyTime = modifyTime;
	}

	public Integer getDelFlag() {
		return delFlag;
	}

	public void setDelFlag(Integer delFlag) {
		this.delFlag = delFlag;
	}

	public Integer getOrderNum() {
		return orderNum;
	}

	public void setOrderNum(Integer orderNum) {
		this.orderNum = orderNum;
	}
}