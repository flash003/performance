package com.hfqixin.assess.bean;

import java.io.Serializable;
import java.util.Date;

public class OutputAssess implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1086133312916510522L;

	private String id;

	private String staffId;

	private String name;

	private Integer assessAdd;

	private Integer assessReduce;

	private Integer assessDay;

	private String score;

	private Date modifyTime;

	private String jobTime;

	private Integer clockInFlag;

	private Date clockInTime;

	private String groupId;

	private Integer assessFlag;

	private Integer standard;

	public Integer getStandard() {
		return standard;
	}

	public void setStandard(Integer standard) {
		this.standard = standard;
	}

	public String getScore() {
		return score;
	}

	public void setScore(String score) {
		this.score = score;
	}

	public Integer getAssessFlag() {
		return assessFlag;
	}

	public void setAssessFlag(Integer assessFlag) {
		this.assessFlag = assessFlag;
	}

	public Integer getClockInFlag() {
		return clockInFlag;
	}

	public void setClockInFlag(Integer clockInFlag) {
		this.clockInFlag = clockInFlag;
	}

	public String getGroupId() {
		return groupId;
	}

	public void setGroupId(String groupId) {
		this.groupId = groupId;
	}

	public String getJobTime() {
		return jobTime;
	}

	public void setJobTime(String jobTime) {
		this.jobTime = jobTime;
	}

	public Date getClockInTime() {
		return clockInTime;
	}

	public void setClockInTime(Date clockInTime) {
		this.clockInTime = clockInTime;
	}

	public Date getModifyTime() {
		return modifyTime;
	}

	public void setModifyTime(Date modifyTime) {
		this.modifyTime = modifyTime;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getStaffId() {
		return staffId;
	}

	public void setStaffId(String staffId) {
		this.staffId = staffId;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Integer getAssessAdd() {
		return assessAdd;
	}

	public void setAssessAdd(Integer assessAdd) {
		this.assessAdd = assessAdd;
	}

	public Integer getAssessReduce() {
		return assessReduce;
	}

	public void setAssessReduce(Integer assessReduce) {
		this.assessReduce = assessReduce;
	}

	public Integer getAssessDay() {
		return assessDay;
	}

	public void setAssessDay(Integer assessDay) {
		this.assessDay = assessDay;
	}

}
