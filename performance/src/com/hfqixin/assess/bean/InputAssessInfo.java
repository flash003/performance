package com.hfqixin.assess.bean;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

public class InputAssessInfo implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 6261530538301972973L;

	private String monitorId;

	private String staffId;

	private String assessId;

	private Date currentTime;

	private String currTime;

	private String startTime;

	private String endTime;

	private String startMonth;

	private String endMonth;

	public String getStartMonth() {
		return startMonth;
	}

	public void setStartMonth(String startMonth) {
		this.startMonth = startMonth;
	}

	public String getEndMonth() {
		return endMonth;
	}

	public void setEndMonth(String endMonth) {
		this.endMonth = endMonth;
	}

	private List<AssessInfo> assessInfo;

	public String getAssessId() {
		return assessId;
	}

	public void setAssessId(String assessId) {
		this.assessId = assessId;
	}

	public String getMonitorId() {
		return monitorId;
	}

	public void setMonitorId(String monitorId) {
		this.monitorId = monitorId;
	}

	public String getStaffId() {
		return staffId;
	}

	public void setStaffId(String staffId) {
		this.staffId = staffId;
	}

	public Date getCurrentTime() {
		return currentTime;
	}

	public void setCurrentTime(Date currentTime) {
		this.currentTime = currentTime;
	}

	public String getCurrTime() {
		return currTime;
	}

	public void setCurrTime(String currTime) {
		this.currTime = currTime;
	}

	public String getStartTime() {
		return startTime;
	}

	public void setStartTime(String startTime) {
		this.startTime = startTime;
	}

	public String getEndTime() {
		return endTime;
	}

	public void setEndTime(String endTime) {
		this.endTime = endTime;
	}

	public List<AssessInfo> getAssessInfo() {
		return assessInfo;
	}

	public void setAssessInfo(List<AssessInfo> assessInfo) {
		this.assessInfo = assessInfo;
	}

}
