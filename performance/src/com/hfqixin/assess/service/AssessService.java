package com.hfqixin.assess.service;

import java.util.List;

import com.hfqixin.assess.bean.Assess;
import com.hfqixin.assess.bean.OutputAssess;
import com.hfqixin.manager.bean.sys.PageMsg;
import com.hfqixin.staff.bean.InputStaff;
import com.hfqixin.staff.bean.Staff;

public interface AssessService {

	public PageMsg<Assess> listAssess(Assess assess) throws Exception;
	
	public PageMsg<Assess> listMonitorAssess(Assess assess) throws Exception;

	public int insertAssess(Assess assess) throws Exception;

	public int deleteAssess(String id) throws Exception;

	int deleteAssessForStaffId(String staffId) throws Exception;

	public Assess getAssess(String id) throws Exception;

	public int updateAssess(Assess assess) throws Exception;

	public OutputAssess listAssessForStaffId(InputStaff input) throws Exception;

	public List<OutputAssess> listAssessForStaffId1(InputStaff input) throws Exception;

	public int initAssess(Staff staff) throws Exception;

	public List<OutputAssess> listAssessForMonitorDay(InputStaff input) throws Exception;
	
	public List<OutputAssess> listAssessForGroupIdDay(InputStaff input)throws Exception;

	public int clockIn(OutputAssess assess)throws Exception;
	
	public int insertMonitorAssess(Assess assess)throws Exception;
	
	public int updateMonitorAssess(Assess assess) throws Exception;
	
	public Assess getMonitorAssessForMonth(Assess assess) throws Exception;
	
	public Integer selectScoreForMonth(InputStaff input)throws Exception;
}
