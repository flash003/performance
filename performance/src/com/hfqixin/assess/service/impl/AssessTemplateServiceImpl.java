package com.hfqixin.assess.service.impl;

import java.util.Date;
import java.util.List;
import java.util.UUID;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.hfqixin.assess.bean.AssessTemplate;
import com.hfqixin.assess.dao.AssessTemplateDao;
import com.hfqixin.assess.service.AssessTemplateService;
import com.hfqixin.manager.bean.sys.PageMsg;

@Service
public class AssessTemplateServiceImpl implements AssessTemplateService {

	@Autowired
	private AssessTemplateDao dao;

	@Override
	public PageMsg<AssessTemplate> listAssessTemplate(AssessTemplate template) throws Exception {
		List<AssessTemplate> list = dao.listAssessTemplate(template);
		PageMsg<AssessTemplate> pageObj = new PageMsg<AssessTemplate>();
		pageObj.setListResult(list);
		pageObj.setTotalNum(dao.countAssessTemplate(template));
		pageObj.setPageNum(template.getSize());
		pageObj.setPage(template.getCurr());
		pageObj.setTotalPage();
		return pageObj;
	}

	@Override
	public int insertAssessTemplate(AssessTemplate template) throws Exception {
		template.setId(UUID.randomUUID().toString().replaceAll("-", ""));
		template.setInsertTime(new Date());
		template.setModifyTime(new Date());
		return dao.insertAssessTemplate(template);
	}

	@Override
	public int deleteAssessTemplate(String id) throws Exception {
		return dao.deleteAssessTemplate(id);
	}

	@Override
	public AssessTemplate getAssessTemplate(String id) throws Exception {
		return dao.getAssessTemplate(id);
	}

	@Override
	public int updateAssessTemplate(AssessTemplate template) throws Exception {
		template.setModifyTime(new Date());
		return dao.updateAssessTemplate(template);
	}

}
