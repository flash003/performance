package com.hfqixin.assess.service.impl;

import java.util.Date;
import java.util.List;
import java.util.UUID;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.hfqixin.assess.bean.AssessInfo;
import com.hfqixin.assess.dao.AssessInfoDao;
import com.hfqixin.assess.service.AssessInfoService;
import com.hfqixin.manager.bean.sys.PageMsg;

@Service
public class AssessInfoServiceImpl implements AssessInfoService {

	@Autowired
	private AssessInfoDao dao;
	
	@Override
	public PageMsg<AssessInfo> listAssessInfo(AssessInfo info) throws Exception {
		List<AssessInfo> list = dao.listAssessInfo(info);
		PageMsg<AssessInfo> pageObj = new PageMsg<AssessInfo>();
		pageObj.setListResult(list);
		pageObj.setTotalNum(dao.countAssessInfo(info));
		pageObj.setPageNum(info.getSize());
		pageObj.setPage(info.getCurr());
		pageObj.setTotalPage();
		return pageObj;
	}

	@Override
	public int insertAssessInfo(AssessInfo info) throws Exception {
		info.setId(UUID.randomUUID().toString().replaceAll("-", ""));
		info.setInsertTime(new Date());
		info.setModifyTime(new Date());
		return dao.insertAssessInfo(info);
	}

	@Override
	public int deleteAssessInfo(String id) throws Exception {
		return dao.deleteAssessInfo(id);
	}

	@Override
	public AssessInfo getAssessInfo(String id) throws Exception {
		return dao.getAssessInfo(id);
	}

	@Override
	public int updateAssessInfo(AssessInfo info) throws Exception {
		return dao.updateAssessInfo(info);
	}

	@Override
	public List<AssessInfo> getAssessInfoList(String assessId) throws Exception {
		return dao.getAssessInfoList(assessId);
	}

	@Override
	public List<AssessInfo> listAssessInfoDetail(String id) throws Exception {
		return dao.listAssessInfoDetail(id);
	}

}
