package com.hfqixin.assess.service.impl;

import java.util.Date;
import java.util.List;
import java.util.UUID;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.hfqixin.assess.bean.Assess;
import com.hfqixin.assess.bean.OutputAssess;
import com.hfqixin.assess.dao.AssessDao;
import com.hfqixin.assess.service.AssessService;
import com.hfqixin.manager.bean.sys.PageMsg;
import com.hfqixin.staff.bean.InputStaff;
import com.hfqixin.staff.bean.Staff;

@Service
public class AssessServiceImpl implements AssessService {
	
	@Autowired
	private AssessDao dao;
	
	@Override
	public PageMsg<Assess> listAssess(Assess assess) throws Exception {
		List<Assess> list = dao.listAssess(assess);
		PageMsg<Assess> pageObj = new PageMsg<Assess>();
		pageObj.setListResult(list);
		pageObj.setTotalNum(dao.countAssess(assess));
		pageObj.setPageNum(assess.getSize());
		pageObj.setPage(assess.getCurr());
		pageObj.setTotalPage();
		return pageObj;
	}
	
	@Override
	public PageMsg<Assess> listMonitorAssess(Assess assess) throws Exception {
		List<Assess> list = dao.listMonitorAssess(assess);
		PageMsg<Assess> pageObj = new PageMsg<Assess>();
		pageObj.setListResult(list);
		pageObj.setTotalNum(dao.countMonitorAssess(assess));
		pageObj.setPageNum(assess.getSize());
		pageObj.setPage(assess.getCurr());
		pageObj.setTotalPage();
		return pageObj;
	}


	@Override
	public int insertAssess(Assess assess) throws Exception {
		assess.setInsertTime(new Date());
		assess.setModifyTime(new Date());
		return dao.insertAssess(assess);
	}

	@Override
	public int deleteAssess(String id) throws Exception {
		return dao.deleteAssess(id);
	}

	@Override
	public int deleteAssessForStaffId(String staffId) throws Exception {
		return dao.deleteAssessForStaffId(staffId);
	}

	@Override
	public Assess getAssess(String id) throws Exception {
		return dao.getAssess(id);
	}

	@Override
	public int updateAssess(Assess assess) throws Exception {
		assess.setModifyTime(new Date());
		return dao.updateAssess(assess);
	}

	@Override
	public OutputAssess listAssessForStaffId(InputStaff input) throws Exception {
		return dao.listAssessForStaffId(input);
	}

	@Override
	public int initAssess(Staff staff) throws Exception {
		staff.setTempId(UUID.randomUUID().toString().replaceAll("-", ""));
		staff.setInserttime(new Date());
		staff.setModifytime(new Date());
		return dao.initAssess(staff);
	}

	@Override
	public List<OutputAssess> listAssessForMonitorDay(InputStaff input) throws Exception {
		return dao.listAssessForMonitorDay(input);
	}

	@Override
	public List<OutputAssess> listAssessForStaffId1(InputStaff input) throws Exception {
		return dao.listAssessForStaffId1(input);
	}

	@Override
	public int clockIn(OutputAssess assess) throws Exception {
		assess.setClockInTime(new Date());
		assess.setClockInFlag(1);
		return dao.clockIn(assess);
	}

	@Override
	public List<OutputAssess> listAssessForGroupIdDay(InputStaff input) throws Exception {
		return dao.listAssessForGroupIdDay(input);
	}

	@Override
	public int insertMonitorAssess(Assess assess) throws Exception {
		assess.setId(UUID.randomUUID().toString().replaceAll("-", ""));
		assess.setInsertTime(new Date());
		assess.setModifyTime(new Date());
		assess.setDelFlag(2);
		return dao.insertMonitorAssess(assess);
	}

	@Override
	public int updateMonitorAssess(Assess assess) throws Exception {
		assess.setModifyTime(new Date());
		return dao.updateMonitorAssess(assess);
	}

	@Override
	public Assess getMonitorAssessForMonth(Assess assess) throws Exception {
		return dao.getMonitorAssessForMonth(assess);
	}

	@Override
	public Integer selectScoreForMonth(InputStaff input) throws Exception {
		return dao.selectScoreForMonth(input);
	}

	
}
