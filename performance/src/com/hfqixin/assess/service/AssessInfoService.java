package com.hfqixin.assess.service;

import java.util.List;

import com.hfqixin.assess.bean.AssessInfo;
import com.hfqixin.manager.bean.sys.PageMsg;

public interface AssessInfoService {

	public PageMsg<AssessInfo> listAssessInfo(AssessInfo info) throws Exception;

	public int insertAssessInfo(AssessInfo info) throws Exception;

	public int deleteAssessInfo(String id) throws Exception;

	public AssessInfo getAssessInfo(String id) throws Exception;

	public List<AssessInfo> getAssessInfoList(String assessId) throws Exception;

	public int updateAssessInfo(AssessInfo info) throws Exception;
	
	public List<AssessInfo> listAssessInfoDetail(String id)throws Exception; 
}
