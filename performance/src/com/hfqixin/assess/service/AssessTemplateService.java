package com.hfqixin.assess.service;

import com.hfqixin.assess.bean.AssessTemplate;
import com.hfqixin.manager.bean.sys.PageMsg;

public interface AssessTemplateService {

	public PageMsg<AssessTemplate> listAssessTemplate(AssessTemplate template) throws Exception;

	public int insertAssessTemplate(AssessTemplate template) throws Exception;

	public int deleteAssessTemplate(String id) throws Exception;

	public AssessTemplate getAssessTemplate(String id) throws Exception;

	public int updateAssessTemplate(AssessTemplate template) throws Exception;
}
