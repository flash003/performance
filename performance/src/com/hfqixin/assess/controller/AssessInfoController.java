package com.hfqixin.assess.controller;

import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import com.hfqixin.assess.bean.AssessInfo;
import com.hfqixin.assess.service.AssessInfoService;
import com.hfqixin.group.bean.Group;
import com.hfqixin.group.service.GroupService;
import com.hfqixin.manager.annotation.AuthenPassport;
import com.hfqixin.manager.annotation.Permission;
import com.hfqixin.manager.bean.DataAuthority;
import com.hfqixin.manager.bean.sys.PageMsg;
import com.hfqixin.manager.controller.BaseController;
import com.hfqixin.manager.util.SessionUtils;

@Controller
@RequestMapping(value = "assessInfo")
public class AssessInfoController extends BaseController {

	@Autowired
	private AssessInfoService service;

	@Autowired
	private GroupService groupService;

	@AuthenPassport
	@Permission
	@RequestMapping(value = "/list", method = RequestMethod.GET)
	public ModelAndView list() throws Exception {
		ModelAndView mv = new ModelAndView();
		List<Group> list = groupService.getGroupList();
		mv.addObject("groupList", list);
		mv.setViewName("/assess/assessInfoList");
		return mv;
	}

	@RequestMapping(value = "/addEdit", method = RequestMethod.GET)
	public ModelAndView addEdit(HttpServletRequest request) {
		return new ModelAndView("/assess/assessInfoAdd");
	}

	@RequestMapping(value = "/updateEdit", method = RequestMethod.GET)
	public ModelAndView updateEdit(HttpServletRequest request, HttpServletResponse response) throws Exception {
		ModelAndView mv = new ModelAndView();
		mv.addObject("id", request.getParameter("id"));
		mv.setViewName("/assess/assessInfoUpdate");
		return mv;
	}

	@AuthenPassport
	@Permission
	@RequestMapping(value = "/listAssessInfo", method = RequestMethod.POST)
	public void listAssessInfo(HttpServletRequest request, HttpServletResponse response, AssessInfo info) {
		try {
			DataAuthority dataAuthority = SessionUtils.getCurrDataAuthority(request);
			info.setDataAuth(dataAuthority);
			PageMsg<AssessInfo> list = service.listAssessInfo(info);
			respSuccessMsg(response, list, "查询成功");
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
			respErrorMsg(response, "系统异常");
		}
	}

	@AuthenPassport
	@Permission
	@RequestMapping(value = "/addAssessInfo", method = RequestMethod.POST)
	public void addAssessInfo(HttpServletRequest request, HttpServletResponse response, AssessInfo info) {

		try {
			service.insertAssessInfo(info);
			respSuccessMsg(response, null, "添加成功");

		} catch (Exception e) {
			// TODO handle exception
			e.printStackTrace();
			respErrorMsg(response, "系统异常");
		}
	}

	@AuthenPassport
	@RequestMapping(value = "/getAssessInfo", method = RequestMethod.POST)
	public void getAssessInfo(HttpServletRequest request, HttpServletResponse response, String id) {
		try {
			System.err.println("ajax" + id);
			AssessInfo info = service.getAssessInfo(id);
			respSuccessMsg(response, info, "查询成功");
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
			respErrorMsg(response, "系统异常");
		}
	}

	@RequestMapping(value = "/delete", method = RequestMethod.POST)
	public void delete(HttpServletRequest request, HttpServletResponse response, String id) {
		try {
			service.deleteAssessInfo(id);
			respSuccessMsg(response, null, "删除成功");
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
			respErrorMsg(response, "系统异常");
		}
	}

	@AuthenPassport
	@Permission
	@RequestMapping(value = "/updateAssessInfo", method = RequestMethod.POST)
	public void updateAssessInfo(HttpServletRequest request, HttpServletResponse response, AssessInfo info) {

		try {
			service.updateAssessInfo(info);
			respSuccessMsg(response, null, "添加成功");

		} catch (Exception e) {
			// TODO handle exception
			e.printStackTrace();
			respErrorMsg(response, "系统异常");
		}
	}

}
