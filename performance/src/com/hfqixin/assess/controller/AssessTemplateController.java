package com.hfqixin.assess.controller;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import com.hfqixin.assess.bean.AssessTemplate;
import com.hfqixin.assess.service.AssessTemplateService;
import com.hfqixin.manager.annotation.AuthenPassport;
import com.hfqixin.manager.annotation.Permission;
import com.hfqixin.manager.bean.DataAuthority;
import com.hfqixin.manager.bean.sys.PageMsg;
import com.hfqixin.manager.controller.BaseController;
import com.hfqixin.manager.util.SessionUtils;

@Controller
@RequestMapping( value = "assessTemplate")
public class AssessTemplateController extends BaseController {

	@Autowired
	private AssessTemplateService service;
	
	@AuthenPassport
	@Permission
	@RequestMapping(value = "/list", method = RequestMethod.GET)
	public ModelAndView list() {
		return new ModelAndView("/assess/assessTemplateList");
	}
	
	@RequestMapping(value = "/addEdit", method = RequestMethod.GET)
	public ModelAndView addEdit(HttpServletRequest request) {
		return new ModelAndView("/assess/assessTemplateAdd");
	}

	@RequestMapping(value = "/updateEdit", method = RequestMethod.GET)
	public ModelAndView updateEdit(HttpServletRequest request, HttpServletResponse response) throws Exception {
		ModelAndView mv = new ModelAndView();
		mv.addObject("id", request.getParameter("id"));
		mv.setViewName("/assess/assessTemplateUpdate");
		return mv;
	}
	
	@AuthenPassport
	@Permission
	@RequestMapping(value = "/listAssessTemplate", method = RequestMethod.POST)
	public void listAssessTemplate(HttpServletRequest request, HttpServletResponse response,
			AssessTemplate template) {
		try {
			DataAuthority dataAuthority = SessionUtils.getCurrDataAuthority(request);
			template.setDataAuth(dataAuthority);
			PageMsg<AssessTemplate> list = service.listAssessTemplate(template);
			respSuccessMsg(response,list, "查询成功");
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
			respErrorMsg(response, "系统异常");
		}
	}
	
	@AuthenPassport
	@Permission
	@RequestMapping(value = "/addAssessTemplate", method = RequestMethod.POST)
	public void addAssessTemplate(HttpServletRequest request, HttpServletResponse response, AssessTemplate template) {

		try {
			service.insertAssessTemplate(template);
			respSuccessMsg(response, null, "添加成功");

		} catch (Exception e) {
			// TODO handle exception
			e.printStackTrace();
			respErrorMsg(response, "系统异常");
		}
	}
	
	@AuthenPassport
	@RequestMapping(value = "/getAssessTemplate", method = RequestMethod.POST)
	public void getAssessTemplate(HttpServletRequest request, HttpServletResponse response, String id) {
		try {
			System.err.println("ajax" + id);
			AssessTemplate template = service.getAssessTemplate(id);
			respSuccessMsg(response, template, "查询成功");
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
			respErrorMsg(response, "系统异常");
		}
	}
	
	@RequestMapping(value = "/delete", method = RequestMethod.POST)
	public void delete(HttpServletRequest request, HttpServletResponse response, String id) {
		try {
			service.deleteAssessTemplate(id);
			respSuccessMsg(response, null, "删除成功");
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
			respErrorMsg(response, "系统异常");
		}
	}
	
	@AuthenPassport
	@Permission
	@RequestMapping(value = "/updateAssessTemplate", method = RequestMethod.POST)
	public void updateAssessTemplate(HttpServletRequest request, HttpServletResponse response, AssessTemplate template) {

		try {
			service.updateAssessTemplate(template);
			respSuccessMsg(response, null, "修改成功");
		} catch (Exception e) {
			// TODO handle exception
			e.printStackTrace();
			respErrorMsg(response, "系统异常");
		}
	}
	
}
