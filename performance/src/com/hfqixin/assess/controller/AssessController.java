package com.hfqixin.assess.controller;

import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import com.hfqixin.assess.bean.Assess;
import com.hfqixin.assess.bean.AssessInfo;
import com.hfqixin.assess.service.AssessInfoService;
import com.hfqixin.assess.service.AssessService;
import com.hfqixin.group.bean.Group;
import com.hfqixin.group.service.GroupService;
import com.hfqixin.manager.annotation.AuthenPassport;
import com.hfqixin.manager.annotation.Permission;
import com.hfqixin.manager.bean.DataAuthority;
import com.hfqixin.manager.bean.sys.PageMsg;
import com.hfqixin.manager.controller.BaseController;
import com.hfqixin.manager.util.SessionUtils;
import com.hfqixin.staff.bean.Staff;
import com.hfqixin.staff.service.StaffService;

@Controller
@RequestMapping(value = "assess")
public class AssessController extends BaseController {

	@Autowired
	private AssessService service;

	@Autowired
	private StaffService staffService;

	@Autowired
	private GroupService groupService;
	
	@Autowired
	private AssessInfoService infoService;

	@AuthenPassport
	@Permission
	@RequestMapping(value = "/list", method = RequestMethod.GET)
	public ModelAndView list() throws Exception {
		ModelAndView mv = new ModelAndView();
		List<Group> list = groupService.getGroupList();
		mv.addObject("groupList", list);
		mv.setViewName("/assess/assessList");
		return mv;
	}

	@AuthenPassport
	@Permission
	@RequestMapping(value = "/monitorList", method = RequestMethod.GET)
	public ModelAndView monitorList() throws Exception {
		ModelAndView mv = new ModelAndView();
		List<Group> list = groupService.getGroupList();
		mv.addObject("groupList", list);
		mv.setViewName("/assess/monitorAssessList");
		return mv;
	}

	@RequestMapping(value = "/addEdit", method = RequestMethod.GET)
	public ModelAndView addEdit(HttpServletRequest request) {
		return new ModelAndView("/assess/assessAdd");
	}

	@RequestMapping(value = "/updateEdit", method = RequestMethod.GET)
	public ModelAndView updateEdit(HttpServletRequest request, HttpServletResponse response) throws Exception {
		ModelAndView mv = new ModelAndView();
		mv.addObject("id", request.getParameter("id"));
		mv.setViewName("/assess/assessMonitorUpdate");
		return mv;
	}
	
	@RequestMapping(value = "/detail", method = RequestMethod.GET)
	public ModelAndView detail(HttpServletRequest request, HttpServletResponse response) throws Exception {
		ModelAndView mv = new ModelAndView();
		mv.addObject("id", request.getParameter("id"));
		System.err.println(request.getParameter("id"));
		mv.setViewName("/assess/assessInfoDetail");
		return mv;
	}

	@RequestMapping(value = "/addEdit1", method = RequestMethod.GET)
	public ModelAndView addEdit1(HttpServletRequest request) throws Exception {
		ModelAndView mv = new ModelAndView();
		mv.addObject("id", request.getParameter("id"));
		List<Staff> list = staffService.getMonitorList();
		mv.addObject("monitorList", list);
		mv.setViewName("/assess/assessMonitorAdd");
		return mv;
	}

	@RequestMapping(value = "/updateEdit1", method = RequestMethod.GET)
	public ModelAndView updateEdit1(HttpServletRequest request, HttpServletResponse response) throws Exception {
		ModelAndView mv = new ModelAndView();
		mv.addObject("id", request.getParameter("id"));
		mv.setViewName("/assess/assessMonitorUpdate");
		return mv;
	}

	@AuthenPassport
	@Permission
	@RequestMapping(value = "/listAssess", method = RequestMethod.POST)
	public void listAssess(HttpServletRequest request, HttpServletResponse response, Assess assess) {
		try {
			DataAuthority dataAuthority = SessionUtils.getCurrDataAuthority(request);
			assess.setDataAuth(dataAuthority);
			PageMsg<Assess> list = service.listAssess(assess);
			respSuccessMsg(response, list, "查询成功");
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
			respErrorMsg(response, "系统异常");
		}
	}
	
	@AuthenPassport
	@Permission
	@RequestMapping(value = "/listAssessInfoDetail", method = RequestMethod.POST)
	public void listAssessInfoDetail(HttpServletRequest request, HttpServletResponse response, String id) {
		try {
			@SuppressWarnings("unused")
			DataAuthority dataAuthority = SessionUtils.getCurrDataAuthority(request);
			System.err.println("xxxx"+ id);
			System.err.println("eeee"+request.getParameter("id"));
			List<AssessInfo> list = infoService.listAssessInfoDetail(id);
			respSuccessMsg(response, list, "查询成功");
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
			respErrorMsg(response, "系统异常");
		}
	}

	@AuthenPassport
	@Permission
	@RequestMapping(value = "/listMonitorAssess", method = RequestMethod.POST)
	public void listMonitorAssess(HttpServletRequest request, HttpServletResponse response, Assess assess) {
		try {
			DataAuthority dataAuthority = SessionUtils.getCurrDataAuthority(request);
			assess.setDataAuth(dataAuthority);
			PageMsg<Assess> list = service.listMonitorAssess(assess);
			respSuccessMsg(response, list, "查询成功");
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
			respErrorMsg(response, "系统异常");
		}
	}

	@AuthenPassport
	@Permission
	@RequestMapping(value = "/addAssess", method = RequestMethod.POST)
	public void addAssess(HttpServletRequest request, HttpServletResponse response, Assess assess) {

		try {
			service.insertAssess(assess);
			respSuccessMsg(response, null, "添加成功");

		} catch (Exception e) {
			// TODO handle exception
			e.printStackTrace();
			respErrorMsg(response, "系统异常");
		}
	}

	@AuthenPassport
	@Permission
	@RequestMapping(value = "/addAssessMonitor", method = RequestMethod.POST)
	public void addAssessMonitor(HttpServletRequest request, HttpServletResponse response, Assess assess) {

		try {
			Staff staff = staffService.getStaff(request.getParameter("staffId"));
			Assess monitorAssess = service.getMonitorAssessForMonth(assess);
			if(monitorAssess == null){
				assess.setStaffId(request.getParameter("staffId"));
				if (staff != null) {
					assess.setName(staff.getName());
					assess.setGroupId(staff.getGroupid());
					assess.setGroupName(staff.getGroupname());
				}

				if (assess.getAssessReduce() == null) {
					assess.setAssessReduce(0);;
				}
				if (assess.getAssessAdd() == null) {
					assess.setAssessAdd(0);
				}
				assess.setAssessReduce(0 - assess.getAssessReduce());
				assess.setAssessDay(assess.getAssessAdd() + assess.getAssessReduce());
				assess.setScore(100+assess.getAssessDay());
				service.insertMonitorAssess(assess);
				respSuccessMsg(response, null, "添加成功");
			}else{
				respErrorMsg(response, "同一月份只能考核一次");
			}
			

		} catch (Exception e) {
			// TODO handle exception
			e.printStackTrace();
			respErrorMsg(response, "系统异常");
		}
	}

	@AuthenPassport
	@RequestMapping(value = "/getAssess", method = RequestMethod.POST)
	public void getAssess(HttpServletRequest request, HttpServletResponse response, String id) {
		try {
			System.err.println("ajax" + id);
			Assess assess = service.getAssess(id);
			respSuccessMsg(response, assess, "查询成功");
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
			respErrorMsg(response, "系统异常");
		}
	}

	@RequestMapping(value = "/delete", method = RequestMethod.POST)
	public void delete(HttpServletRequest request, HttpServletResponse response, String id) {
		try {
			service.deleteAssess(id);
			respSuccessMsg(response, null, "删除成功");
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
			respErrorMsg(response, "系统异常");
		}
	}

	@AuthenPassport
	@Permission
	@RequestMapping(value = "/updateAssess", method = RequestMethod.POST)
	public void updateAssess(HttpServletRequest request, HttpServletResponse response, Assess assess) {

		try {
			service.updateAssess(assess);
			respSuccessMsg(response, null, "添加成功");

		} catch (Exception e) {
			// TODO handle exception
			e.printStackTrace();
			respErrorMsg(response, "系统异常");
		}
	}

	@AuthenPassport
	@Permission
	@RequestMapping(value = "/updateMonitorAssess", method = RequestMethod.POST)
	public void updateMonitorAssess(HttpServletRequest request, HttpServletResponse response, Assess assess) {

		try {
			assess.setAssessDay(assess.getAssessAdd() + assess.getAssessReduce());
			service.updateMonitorAssess(assess);
			respSuccessMsg(response, null, "修改成功");

		} catch (Exception e) {
			// TODO handle exception
			e.printStackTrace();
			respErrorMsg(response, "系统异常");
		}
	}
}
