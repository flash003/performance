<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%
	String path = request.getContextPath();
	String basePath = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort()
			+ path + "/";
%>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>

<title>巡检员编辑</title>

<%@include file="../../../resource.jsp"%>

<link
	href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css"
	rel="stylesheet">
<link href="<%=basePath%>js/bootstrap-fileinput/css/fileinput.css"
	media="all" rel="stylesheet" type="text/css" />
<link
	href="<%=basePath%>/js/bootstrap-fileinput/themes/explorer/theme.css"
	media="all" rel="stylesheet" type="text/css" />
<script src="<%=basePath%>js/bootstrap-fileinput/js/fileinput.js"
	type="text/javascript"></script>
<script src="<%=basePath%>js/bootstrap-fileinput/js/locales/zh.js"
	type="text/javascript"></script>
<script
	src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"
	type="text/javascript"></script>
<style type="text/css">
.site-demo-upload img {
	width: 200px;
	height: 200px;
}

.layui-upload-button input {
	position: absolute;
	left: 0;
	top: 0;
	z-index: 10;
	font-size: 100px;
	width: 100%;
	height: 100%;
}

.Validform_checktip {
	display: inline-block;
	width: 90px;
	overflow: visible;
}

.layui-form-label {
	width: 150px;
}

body {
	padding: 20px;
}

.progress {
	display: none;
}

a:hover {
	color: #777;
}

a, a:hover, a:focus {
	color: #333333;
	text-decoration: none;
	outline: none;
	-webkit-transition: all 0.3s;
	transition: all 0.3s;
	-moz-transition: all 0.3s;
	-o-transition: all 0.3s;
}
</style>
</head>

<body>
	<div id="addOrUpdate">
		<fieldset class="layui-elem-field">
			<div class="layui-field-box">
				<form id="menuObj" class="layui-form" method="post"
					action="updateIPQC.do">

					<input type="hidden" id="Id" name="id" >


					<div class="layui-form-item">
						<label class="layui-form-label"><spring:message
								code="姓名" /></label><span class="required_field">*</span>
						<div class="layui-input-inline">
							<input type="text" id="name" name="name" datatype="*1-100"
								autocomplete="off"
								class="layui-input">
						</div>
						<div class="Validform_checktip"></div>
					</div>
					
					
					<div class="layui-form-item">
						<label class="layui-form-label"><spring:message
								code="电话" /></label>
						<div class="layui-input-inline">
							<input type="text" id="phone" name="phone" 
								autocomplete="off"
								class="layui-input">
						</div>
						<div class="Validform_checktip"></div>
					</div>
					
					
					
					
					<div class="layui-form-item">
						<label class="layui-form-label"><spring:message
								code="登陆密码" /></label>
						<div class="layui-input-inline">
							<input type="text" id="loginpass" name="loginpass" 
								autocomplete="off"
								class="layui-input">
						</div>
						<div class="Validform_checktip"></div>
					</div>
					
					
					<div class="layui-form-item">
						<div class="layui-input-block">
							<button class="layui-btn" lay-filter="formDemo" type="submit">
								<spring:message code="op.submit" />
							</button>
							<button type="button" class="layui-btn layui-btn-primary"
								onclick="doClose();">
								<spring:message code="op.cancel" />
							</button>
						</div>
					</div>

				</form>
			</div>
		</fieldset>
	</div>
</body>
<script type="text/javascript">
function basePath(){
    //获取当前网址，如： http://localhost:8080/ems/Pages/Basic/Person.jsp
    var curWwwPath = window.document.location.href;
    //获取主机地址之后的目录，如： /ems/Pages/Basic/Person.jsp
    var pathName = window.document.location.pathname;
    var pos = curWwwPath.indexOf(pathName);
    //获取主机地址，如： http://localhost:8080
    var localhostPath = curWwwPath.substring(0, pos);
    //获取带"/"的项目名，如：/ems
    var projectName = pathName.substring(0, pathName.substr(1).indexOf('/') + 1);
    //获取项目的basePath   http://localhost:8080/ems/
    var basePath=localhostPath+projectName+"/";
    return basePath;
};
	var basePath = basePath();
    $(function (){
	
	 }); 
    
 	layui.use('form', function(){
	  	var form = layui.form();
	  	form.on('submit(formDemo)', function(data){
	    	return false;
	  	}); 
	}); 
  </script>
<script type="text/javascript">


window.onload = function (){

	var id = '${id}'; 
		$.ajax({
		url : 'getIPQC.do',
		type : 'POST',
		data : {id:id}, 
		async : false,
		dataType : 'json',
		timeout : 10000,
		error : function() {layer.msg('<spring:message code="prompted.request" />');},
		success : function(result) {
			if (result.code == 200) {
				var data = result.data;
				$("#Id").val(data.id);
				$("#name").val(data.name);
				$("#phone").val(data.phone);
				$("#loginpass").val(data.loginpass);
				
		treeChkDisabled(id,parentId,"parentId_tree");
		}else{
			layer.msg(result.msg);
			}
		}		
	});		
		
		
}
 
	 	$(document).ready(function(){
			ajaxSubmitFormAndValid("menuObj",function(data){
				if(data.code == 200) {
					parent.getEntityList();
					var index = parent.layer.getFrameIndex(window.name);
					parent.layer.close(index);
					parent.layer.msg(data.msg); 
				} else {
					layer.msg(data.msg);
				}
			},function(msg,o,cssctl){
				//msg：提示信息;
				//o:{obj:*,type:*,curform:*}, obj指向的是当前验证的表单元素（或表单对象），type指示提示的状态，值为1、2、3、4， 1：正在检测/提交数据，2：通过验证，3：验证失败，4：提示ignore状态, curform为当前form对象;
				//cssctl:内置的提示信息样式控制函数，该函数需传入两个参数：显示提示信息的对象 和 当前提示的状态（既形参o中的type）;
				if(!o.obj.is("form")){//验证表单元素时o.obj为该表单元素，全部验证通过提交表单时o.obj为该表单对象;
				console.log(o.obj);	
				var objtip=o.obj.parent(".layui-input-inline,.layui-input-block").nextAll(".Validform_checktip");
				if(objtip.length > 1) {
					objtip = objtip[0];
				}	
				cssctl(objtip,o.type);
					objtip.text(msg);
				}else{
					/* var objtip=o.obj.find("#msgdemo");
					cssctl(objtip,o.type);
					objtip.text(msg); */
				}
			});
		});
		
		layui.use('upload', function(){
		  layui.upload({
		    url: '' //上传接口
		    ,success: function(res){ //上传成功后的回调
		      console.log(res)
		    }
		  });
		  
		  layui.upload({
		    url: '/test/upload.json'
		    ,elem: '#test' //指定原始元素，默认直接查找class="layui-upload-file"
		    ,method: 'get' //上传接口的http类型
		    ,success: function(res){
		      LAY_demo_upload.src = res.url;
		    }
		  });
		});
		
	 	function doAddOrUpdate(data){
			$.ajax({
				url : 'doAddOrUpdate.do',
				type : 'POST',
				data : data,
				async : false,
				dataType : 'json',
				timeout : 10000,
				error : function() {layer.msg('<spring:message code="prompted.request" />');},
				success : function(result) {
					if (result.code == 200) {
						layer.msg("<spring:message code='prompted.success' />");
						var index = parent.layer.getFrameIndex(window.name); //先得到当前iframe层的索引
						parent.layer.close(index);
						//重新加载数据及树桩表格
						getEntityList();
			  			parent.nowEntity();
					} else {
						layer.msg(result.msg);
					}
				}
			});
		} 
		
		//新增数据，添加下级时使用
    	function treeChkDisabledThis(parentId,treeId){
			var zTree = $.fn.zTree.getZTreeObj(treeId);
			if(parentId == null || parentId == 0) {
				return;
			}
			var nodes = zTree.getNodeByParam("id", parentId);
			// 选中父节点
			zTree.checkNode(nodes, true, true,true);
			$("#"+treeId).trigger("tree.select.change");
    	}
    	
    	//修改数据时    设置选中   且自身及以下不可选
    	function treeChkDisabled(id,parentId,treeId){
			var zTree = $.fn.zTree.getZTreeObj(treeId);
			// 设置自身节点不能选
			zTree.setChkDisabled(zTree.getNodeByParam("id", id),true,false ,true );
			if(parentId == null || parentId == 0) {
				return;
			}
			var nodes = zTree.getNodeByParam("id", parentId);
			// 选中父节点
			zTree.checkNode(nodes, true, true,true);
			$("#"+treeId).trigger("tree.select.change");
    	}
    	
    	function doClose(){
    		var index = parent.layer.getFrameIndex(window.name); //先得到当前iframe层的索引
			parent.layer.close(index);
    	}
  </script>
</html>
