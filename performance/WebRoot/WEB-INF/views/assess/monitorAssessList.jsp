<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%
String path = request.getContextPath();
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
  <head>
  	<base href="<%=basePath%>">
    <%@include file="../../../resource.jsp" %>
    <%--  <link rel="stylesheet" href="<%=basePath%>/layui-v2.2.5/layui/css/layui.css"  media="all"> --%>
    <title>班长考核</title>
    <script type="text/javascript">
    	//数据初始化  下移  文件末尾
    	
        function getEntityList(){
        	var queryParam = getFormJson("#query_param");
			queryParam.curr = 1;
			queryParam.size = 10;
			commmonAjax({url:"assess/listMonitorAssess.do",data:queryParam,success:function(data){
				if(data.code == 200) {
					tableData(data);
					commonPager({url:"assess/listMonitorAssess.do",data:queryParam,total:data.data.totalNum,size:data.data.pageNum,curr:data.data.page,success:tableData});
				}
			}});
        }
        
        function tableData(data){
			list = data.data.listResult;
			var html = autoGenerateTableHtml("table",list);
			$("table").find("tbody").html(html);
		}
		
    	//进入修改
    	function updEntity(id){
    		layer.open({
			  type: 2,
			  title :'<spring:message code="" />',
			  content: 'assess/updateEdit1.do?id='+id,
			  closeBtn:0,
			  area: ['100%', '100%'],
			  offset: ['0px', '0px']
			});
    	}
    	//进入添加下级 
    	function addEntity(){
    		layer.open({
			  type: 2,
			  title :'<spring:message code="" />',
			  content: 'assess/addEdit1.do?',
			  closeBtn:0,
			  area: ['100%', '100%'],
			  offset: ['0px', '0px']
			});
    	}
    	
    
    	
    	function delEntity(id){
    		layer.confirm('<spring:message code="删除绩效记录" />', {icon: 3, title:'<spring:message code="确认删除此绩效记录？" />'}, function(index){
			  //do something
				commmonAjax({url:"assess/delete.do",data:{id:id},success:function(data){
					if(data.code == 200) {
						layer.msg(data.msg);
						//重新加载数据及树桩表格
				  		getEntityList();
					}
				}});
			});
    	}
    	
        function exportExcel(){
        	var queryParam = getFormJson("#query_param");
			location.href="<%=basePath %>supplier/exportExcel.do";
		}
    </script>
    
    
    
     <style type="text/css">
    	.site-demo-upload img {
		    width: 200px;
		    height: 200px;
		}
		.layui-upload-button input {
		    position: absolute;
		    left: 0;
		    top: 0;
		    z-index: 10;
		    font-size: 100px;
		    width: 100%;
		    height: 100%;
		}
		.Validform_checktip{
			display: inline-block;
			width: 90px;
			overflow: visible;
		}
		body{
			padding: 20px;
			margin: 0px;
		}
	</style>
  </head>
  
  <body style="padding: 20px;">
  <div>
  	<form action="" id="query_param" name="query_param" class='layui-form' >
  	<div class="layui-form-item">
	   
	   
	<div class="layui-inline">
      <label class="layui-form-label">请选择月份</label>
      <div class="layui-input-inline">
        <input class="layui-input" id="test3" placeholder="yyyy-MM" type="text" name="assessMonth" >
      </div>
    </div>
  		
  		
  		<div class="layui-input-inline d16" >
	    <select name="groupId">
	    		<option value="-1">==请选择班组==</option>
	       		<c:forEach var="cur" items="${groupList}" varStatus="vs"  >	 
	       			<option value="${cur.id}" >${cur.name}</option>
	       		</c:forEach>
	    	</select>
	    </div>
  		
  	
	    <label class="layui-form-label l4"><spring:message code="姓名" />：</label>
	    <div class="layui-input-inline d16">
	      <input type="text" id="name" name="name" autocomplete="off" placeholder="" class="layui-input">
	    </div>
	    
	    
		<a class="layui-btn"  onclick="getEntityList()"><spring:message code="op.query" /></a>
   		<a class="layui-btn"  onclick="addEntity()"><spring:message code="班长考核" /></a>
   		<%-- <a class="layui-btn"  onclick="exportExcel()"><spring:message code="导出为xls" /></a> --%>
  	  </div>
  	  </form>
  </div>
    
  

<div class="layui-form">
  <table class="layui-table   table-resize">
    <thead>
      <tr>
        <th data-field="name"><spring:message code="姓名" /></th>
         <th data-field="groupName"><spring:message code="班组" /></th> 
        <th data-field="score"><spring:message code="分数" /></th>
        <th data-field="assessDay"><spring:message code="本月得分" /></th>
        <th data-field="assessAdd"><spring:message code="加分" /></th>
         <th data-field="addExplain"><spring:message code="加分说明" /></th>
        <th data-field="assessReduce"><spring:message code="减分" /></th>
        <th data-field="reduceExplain"><spring:message code="减分说明" /></th>
      <%--   <th data-field="assessPerson"><spring:message code="考核人" /></th> --%>
        <th data-field="assessMonth" ><spring:message code="考核月份" /></th>
        <th data-field="Id" formatter="optionFormatter" ><spring:message code="op" /></th>
      </tr> 
    </thead>
    <tbody>
    </tbody>
  </table>
   <div class="pager">
		<div id="pager" class="pager"></div>
	</div>
</div>

  </body>
  <script src="<%=basePath%>/layui-v2.2.5/layui/layui.all.js" charset="utf-8"></script>
  <script type="text/javascript">
  		$(function (){
    		getEntityList();
    		
    
   		});
  		
  		/* layui.use('form', function(){
		  	var form = layui.form();
		  	form.on('submit(formDemo)', function(data){
		    	//layer.msg(JSON.stringify(data.field));
		    	//doAddOrUpdate(data.field);
		    	return false;
		  	}); 
		}); */
  		
  		layui.use('laydate', function(){
  		  var laydate = layui.laydate;
  		  

  		  //年月选择器
  		  laydate.render({
  		    elem: '#test3'
  		    ,type: 'month'
  		  });

  		});
  
    function registerTime(data,row,index){
		return dateToStr(data,"yyyy-MM-dd hh:mm:ss")
	}
	
	function optionFormatter(data,row,index){
		console.log(row);
		return "<a href='javascript:void()'  onclick='updEntity(\""+row.id+"\")' ><spring:message code='编辑' /></a>&nbsp;&nbsp; <a href='javascript:void()' onclick='delEntity(\""+row.id+"\")' ><spring:message code='op.delete' /></a>";
	}
	
    </script>
</html>
