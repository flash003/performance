<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%
String path = request.getContextPath();
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
  <head>
  	<base href="<%=basePath%>">
    <%@include file="../../../resource.jsp" %>
    <title>绩效考核</title>
    <script type="text/javascript">
    	//数据初始化  下移  文件末尾
    	
        function getEntityList(){
        	var queryParam = getFormJson("#query_param");
			queryParam.curr = 1;
			queryParam.size = 10;
			commmonAjax({url:"assess/listAssess.do",data:queryParam,success:function(data){
				if(data.code == 200) {
					tableData(data);
					commonPager({url:"assess/listAssess.do",data:queryParam,total:data.data.totalNum,size:data.data.pageNum,curr:data.data.page,success:tableData});
				}
			}});
        }
        
        function tableData(data){
			list = data.data.listResult;
			var html = autoGenerateTableHtml("table",list);
			$("table").find("tbody").html(html);
			
			var assessFlag = $("tbody").find("tr").find("td:eq(3)");
			$(assessFlag).each(function(){
				 if($(this).html() == 0){
					 $(this).html("未考核");
					 $(this).attr("style","color:red");
				}
				if($(this).html() == 1){
					$(this).html("已考核");
					$(this).attr("style","color:green");
				} 
			})
			
		}
		
    	//进入修改
    	function updEntity(id){
    		layer.open({
			  type: 2,
			  title :'<spring:message code="" />',
			  content: 'assess/updateEdit.do?id='+id,
			  closeBtn:0,
			  area: ['100%', '100%'],
			  offset: ['0px', '0px']
			});
    	}
    	
    	//进入修改
    	function detail(id){
    		layer.open({
			  type: 2,
			  title :'<spring:message code="" />',
			  content: 'assess/detail.do?id='+id,
			  closeBtn:0,
			  area: ['100%', '100%'],
			  offset: ['0px', '0px']
			});
    	}
    	
    	
    	//进入添加下级 
    	function addEntity(){
    		layer.open({
			  type: 2,
			  title :'<spring:message code="" />',
			  content: 'assess/addEdit.do?',
			  closeBtn:0,
			  area: ['100%', '100%'],
			  offset: ['0px', '0px']
			});
    	}
    	
    
    	
    	function delEntity(id){
    		layer.confirm('<spring:message code="删除绩效记录" />', {icon: 3, title:'<spring:message code="确认删除此绩效记录？" />'}, function(index){
			  //do something
				commmonAjax({url:"assess/delete.do",data:{id:id},success:function(data){
					if(data.code == 200) {
						layer.msg(data.msg);
						//重新加载数据及树桩表格
				  		getEntityList();
					}
				}});
			});
    	}
    	
        function exportExcel(){
        	var queryParam = getFormJson("#query_param");
			location.href="<%=basePath %>supplier/exportExcel.do";
		}
    </script>
    
    
    
     <style type="text/css">
    	.site-demo-upload img {
		    width: 200px;
		    height: 200px;
		}
		.layui-upload-button input {
		    position: absolute;
		    left: 0;
		    top: 0;
		    z-index: 10;
		    font-size: 100px;
		    width: 100%;
		    height: 100%;
		}
		.Validform_checktip{
			display: inline-block;
			width: 90px;
			overflow: visible;
		}
		body{
			padding: 20px;
			margin: 0px;
		}
	</style>
  </head>
  
  <body style="padding: 20px;">
  <div>
  	<form action="" id="query_param" name="query_param" class='layui-form' >
  	<div class="layui-form-item">
  	
  	
  		<div class="layui-inline">
	   
	    <div class="layui-input-inline d16">
	      <input class="layui-input" placeholder="<spring:message code="label.startdate" />" id="LAY_demorange_s" name="startTime">
	    </div>
	    <div class="layui-input-inline d16">
	      <input class="layui-input" placeholder="<spring:message code="label.enddate" />" id="LAY_demorange_e" name="endTime">
	    </div>
	    </div>
  		
  		
  		<div class="layui-input-inline d16" >
	    <select name="groupId">
	    		<option value="-1">==请选择班组==</option>
	       		<c:forEach var="cur" items="${groupList}" varStatus="vs"  >	 
	       			<option value="${cur.id}" >${cur.name}</option>
	       		</c:forEach>
	    	</select>
	    </div>
	    
	    <div class="layui-input-inline d16" >
	    <select name="jobTime">
	    		<option value="-1">==请选择班次==</option>
	    		<option value="7-5点">7-5点</option>
	       		<option value="早班(08:00~15:00)">早班(08:00~15:00)</option>
	       		<option value="中班(15:00~12:00)">中班(15:00~12:00)</option>
	       		<option value="夜班(12:00~08:00)">夜班(12:00~08:00)</option>
	    	</select>
	    </div>
	    
	     <div class="layui-input-inline d16" >
	    <select name="assessFlag">
	    		<option value="1">已考核</option>
	       		<option value="0">未考核</option>
	    	</select>
	    </div>
  		
  	
	   
	    <div class="layui-input-inline d16">
	      <input type="text" id="name" name="name" autocomplete="off" placeholder="姓名" class="layui-input">
	    </div>
	    
	    
		<a class="layui-btn"  onclick="getEntityList()"><spring:message code="op.query" /></a>
  <%--  		<a class="layui-btn"  onclick="addEntity()"><spring:message code="添加员工" /></a> --%>
   		<%-- <a class="layui-btn"  onclick="exportExcel()"><spring:message code="导出为xls" /></a> --%>
  	  </div>
  	  </form>
  </div>
    
  

<div class="layui-form">
  <table class="layui-table   table-resize">
    <thead>
      <tr>
        <th data-field="name"><spring:message code="姓名" /></th>
        <th data-field="groupName"><spring:message code="班组" /></th>
         <th data-field="jobTime"><spring:message code="班次" /></th>
         <th data-field="assessFlag"><spring:message code="是否考核" /></th>
        <th data-field="assessDay"><spring:message code="今日得分" /></th>
        <th data-field="score"><spring:message code="分数" /></th>
        <th data-field="assessAdd"><spring:message code="加分" /></th>
        <th data-field="assessReduce"><spring:message code="减分" /></th>
       <%--  <th data-field="assessPerson"><spring:message code="考核人" /></th> --%>
        <th data-field="modifyTime" formatter="registerTime"><spring:message code="添加时间" /></th>
        <th data-field="Id" formatter="optionFormatter" ><spring:message code="op" /></th> 
      </tr> 
    </thead>
    <tbody>
    </tbody>
  </table>
   <div class="pager">
		<div id="pager" class="pager"></div>
	</div>
</div>

  </body>
  <script type="text/javascript">
  		$(function (){
    		getEntityList();
    
   		});
  		
  		layui.use('laydate', function(){
  		  var laydate = layui.laydate;
  		  
  		  var start = {
  		    min: '1999-06-16 23:59:59'
  		    ,max: '2099-06-16 23:59:59'
  		   	,istime:false
  		    ,istoday: true
  		    ,choose: function(datas){
  		      end.min = datas; //开始日选好后，重置结束日的最小日期
  		      end.start = datas //将结束日的初始值设定为开始日
  		    }
  		  };
  		  
  		  var end = {
  		    min: '1999-06-16 23:59:59'
  		    ,max: '2099-06-16 23:59:59'
  		   	,istime:false
  		    ,istoday: true
  		    ,choose: function(datas){
  		      start.max = datas; //结束日选好后，重置开始日的最大日期
  		    }
  		  };
  		    document.getElementById('LAY_demorange_s').onclick = function(){
  		     start.elem = this; 
  		    laydate(start);
  		  }
  		  document.getElementById('LAY_demorange_e').onclick = function(){
  		    end.elem = this 
  		    laydate(end);
  		  }
  		  
  		});
		
		layui.use('form', function(){
		  	var form = layui.form();
		  	form.on('submit(formDemo)', function(data){
		    	//layer.msg(JSON.stringify(data.field));
		    	//doAddOrUpdate(data.field);
		    	return false;
		  	}); 
		});
  
    function registerTime(data,row,index){
		return dateToStr(data,"yyyy-MM-dd hh:mm:ss")
	}
	
	function optionFormatter(data,row,index){
		console.log(row);
		return "<a href='javascript:void()' onclick='detail(\""+row.id+"\")' ><spring:message code='考核详情' /></a>";
	}
	

	
    </script>
</html>
