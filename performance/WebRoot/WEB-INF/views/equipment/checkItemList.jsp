<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%
String path = request.getContextPath();
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
  <head>
  	<base href="<%=basePath%>">
    <%@include file="../../../resource.jsp" %>
    <title>检查项</title>
    <script type="text/javascript">
    	//数据初始化  下移  文件末尾
    	
        function getEntityList(){
        	var queryParam = getFormJson("#query_param");
			queryParam.curr = 1;
			queryParam.size = 10;
			commmonAjax({url:"checkItem/listCheckItem.do",data:queryParam,success:function(data){
				if(data.code == 200) {
					tableData(data);
					commonPager({url:"checkItem/listCheckItem.do",data:queryParam,total:data.data.totalNum,size:data.data.pageNum,curr:data.data.page,success:tableData});
				}
			}});
        }
        
        function tableData(data){
			list = data.data.listResult;
			var html = autoGenerateTableHtml("table",list);
			$("table").find("tbody").html(html);
		}
		
    	//进入修改
    	function updEntity(id){
    		layer.open({
			  type: 2,
			  title :'<spring:message code="检查项编辑" />',
			  content: 'checkItem/updateEdit.do?id='+id,
			  closeBtn:0,
			  area: ['100%', '100%'],
			  offset: ['0px', '0px']
			});
    	}
    	//进入添加下级 
    	function addEntity(){
    		layer.open({
			  type: 2,
			  title :'<spring:message code="添加检查项" />',
			  content: 'checkItem/addEdit.do?',
			  closeBtn:0,
			  area: ['100%', '100%'],
			  offset: ['0px', '0px']
			});
    	}
    	
    
    	
    	function delEntity(id){
    		layer.confirm('<spring:message code="删除检查项" />', {icon: 3, title:'<spring:message code="确认删除此检查项？" />'}, function(index){
			  //do something
				commmonAjax({url:"checkItem/delete.do",data:{id:id},success:function(data){
					if(data.code == 200) {
						layer.msg(data.msg);
						//重新加载数据及树桩表格
				  		getEntityList();
					}
				}});
			});
    	}
    	
        function exportExcel(){
        	var queryParam = getFormJson("#query_param");
			location.href="<%=basePath %>supplier/exportExcel.do";
		}
    </script>
    
    
    
     <style type="text/css">
    	.site-demo-upload img {
		    width: 200px;
		    height: 200px;
		}
		.layui-upload-button input {
		    position: absolute;
		    left: 0;
		    top: 0;
		    z-index: 10;
		    font-size: 100px;
		    width: 100%;
		    height: 100%;
		}
		.Validform_checktip{
			display: inline-block;
			width: 90px;
			overflow: visible;
		}
		body{
			padding: 20px;
			margin: 0px;
		}
	</style>
  </head>
  
  <body style="padding: 20px;">
  <div>
  	<form action="" id="query_param" name="query_param" >
  	<div class="layui-form-item">
  	
  		<label class="layui-form-label l4"><spring:message code="设备" />：</label>
	    <div class="layui-input-inline d16">
	      <input type="text" id="equipmentName" name="equipmentName" autocomplete="off" placeholder="设备" class="layui-input">
	    </div>
  	
	    <label class="layui-form-label l4"><spring:message code="检查项" />：</label>
	    <div class="layui-input-inline d16">
	      <input type="text" id="name" name="name" autocomplete="off" placeholder="检查项" class="layui-input">
	    </div>
	    
	    
		<a class="layui-btn"  onclick="getEntityList()"><spring:message code="op.query" /></a>
   		<a class="layui-btn"  onclick="addEntity()"><spring:message code="添加检查项" /></a>
   		<%-- <a class="layui-btn"  onclick="exportExcel()"><spring:message code="导出为xls" /></a> --%>
  	  </div>
  	  </form>
  </div>
    
  

<div class="layui-form">
  <table class="layui-table   table-resize">
    <thead>
      <tr>
        <th data-field="equipmentName"><spring:message code="设备名称" /></th>
        <th data-field="name"><spring:message code="检查项" /></th>
         <th data-field="content"><spring:message code="检查内容" /></th>
        <th data-field="frequency"><spring:message code="检查频率（天）" /></th>
       <%--   <th data-field="checkPoint"><spring:message code="上次检查要点" /></th> --%>
         <th data-field=checkSite><spring:message code="检查地点" /></th>
         <th data-field=iqpcName><spring:message code="巡检员" /></th>
        <th data-field="checkTime" formatter="registerTime"><spring:message code="最近检查时间" /></th>
        <th data-field="Id" formatter="optionFormatter" ><spring:message code="op" /></th>
      </tr> 
    </thead>
    <tbody>
    </tbody>
  </table>
   <div class="pager">
		<div id="pager" class="pager"></div>
	</div>
</div>

  </body>
  <script type="text/javascript">
  		$(function (){
    		getEntityList();
    		
    
   		});
  
    function registerTime(data,row,index){
		return dateToStr(data,"yyyy-MM-dd hh:mm:ss")
	}
	
	function optionFormatter(data,row,index){
		console.log(row);
		return "<a href='javascript:void()'  onclick='updEntity(\""+row.id+"\")' ><spring:message code='编辑' /></a>&nbsp;&nbsp; <a href='javascript:void()' onclick='delEntity(\""+row.id+"\")' ><spring:message code='op.delete' /></a>";
	}
	
    </script>
</html>
