<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%
	String path = request.getContextPath();
	String basePath = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort()
			+ path + "/";
%>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>

<title>添加巡检员</title>

<%@include file="../../../resource.jsp"%>

<link
	href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css"
	rel="stylesheet">
<link href="<%=basePath%>js/bootstrap-fileinput/css/fileinput.css"
	media="all" rel="stylesheet" type="text/css" />
<link
	href="<%=basePath%>/js/bootstrap-fileinput/themes/explorer/theme.css"
	media="all" rel="stylesheet" type="text/css" />
<script src="<%=basePath%>js/bootstrap-fileinput/js/fileinput.js"
	type="text/javascript"></script>
<script src="<%=basePath%>js/bootstrap-fileinput/js/locales/zh.js"
	type="text/javascript"></script>
<script
	src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"
	type="text/javascript"></script>
<style type="text/css">
.site-demo-upload img {
	width: 200px;
	height: 200px;
}

.layui-upload-button input {
	position: absolute;
	left: 0;
	top: 0;
	z-index: 10;
	font-size: 100px;
	width: 100%;
	height: 100%;
}

.Validform_checktip {
	display: inline-block;
	width: 90px;
	overflow: visible;
}

.layui-form-label {
	width: 150px;
}

body {
	padding: 20px;
}

.progress {
	display: none;
}

a:hover {
	color: #777;
}

a, a:hover, a:focus {
	color: #333333;
	text-decoration: none;
	outline: none;
	-webkit-transition: all 0.3s;
	transition: all 0.3s;
	-moz-transition: all 0.3s;
	-o-transition: all 0.3s;
}
</style>
</head>

<body>
	<div id="addOrUpdate">
		<fieldset class="layui-elem-field">
			<div class="layui-field-box">
				<form id="menuObj" class="layui-form" method="post"
					action="addEquipment.do">


					<div class="layui-form-item">
						<label class="layui-form-label"><spring:message
								code="设备名称" /></label><span class="required_field">*</span>
						<div class="layui-input-inline">
							<input type="text" id="name" name="name" datatype="*1-100"
								autocomplete="off"
								placeholder="请输入设备名称"
								class="layui-input">
						</div>
						<div class="Validform_checktip"></div>
					</div>
					
					
					<div class="layui-form-item">
						<label class="layui-form-label tb"><spring:message
								code="设备信息" /></label>
						<div class="layui-input-inline" style="width: 40%;">
							<textarea id="information" name="information"
								class="layui-textarea"></textarea>
						</div>
				</div>
				
				<div class="layui-form-item">
						<label class="layui-form-label"><spring:message
								code="设备位置" /></label>
						<div class="layui-input-inline">
							<input type="text" id="site" name="site" 
								autocomplete="off"
								placeholder="请输入设备位置"
								class="layui-input">
						</div>
						<div class="Validform_checktip"></div>
					</div>
					
					
					
					<%-- <div class="layui-form-item">
						<label class="layui-form-label"><spring:message
								code="巡检员" /></label>
						<div class="layui-input-inline">
							<select name="ipqcId" >
								<option value="-1"><spring:message code="== 请选择 ==" /></option>							
								<c:forEach var="cur" items="${ipqcList}" varStatus="vs">
									<option value="${cur.id }" >${cur.name}</option>
								</c:forEach>
							</select>
						</div>
					</div> --%>
					
					<div class="layui-form-item">
						<label class="layui-form-label"><spring:message
								code="设备图片1" /></label>
						<div class="layui-input-inline" style="width: 400px;">
							<input id="file-upload" name="file-upload" class="file"
								type="file" data-min-file-count="1"> <input
								type="hidden" id="picUrl1" name="picUrl1" value=""
								class="layui-input">
						</div>
					</div>
					
					<div class="layui-form-item">
						<label class="layui-form-label"><spring:message
								code="设备图片2" /></label>
						<div class="layui-input-inline" style="width: 400px;">
							<input id="file-upload2" name="file-upload2" class="file"
								type="file" data-min-file-count="1"> <input
								type="hidden" id="picUrl2" name="picUrl2" value=""
								class="layui-input">
						</div>
					</div>
					
					<div class="layui-form-item">
						<label class="layui-form-label"><spring:message
								code="设备图片3" /></label>
						<div class="layui-input-inline" style="width: 400px;">
							<input id="file-upload3" name="file-upload3" class="file"
								type="file" data-min-file-count="1"> <input
								type="hidden" id="picUrl3" name="picUrl3" value=""
								class="layui-input">
						</div>
					</div>
					
					
			 
					<div class="layui-form-item">
						<div class="layui-input-block">
							<button class="layui-btn"  type="submit" >
								<spring:message code="op.submit" />
							</button>
							<button type="reset" class="layui-btn layui-btn-primary"
								onclick="doClose();">
								<spring:message code="op.cancel" />
							</button>
						</div>
					</div>

				</form>
			</div>
		</fieldset>
	</div>
</body>
<script type="text/javascript">
    $(function (){
	
	 }); 
    
 	layui.use('form', function(){
	  	var form = layui.form();
	  	form.on('submit(formDemo)', function(data){
	    	//layer.msg(JSON.stringify(data.field));
	    	//doAddOrUpdate(data.field);
	    	return false;
	  	}); 
	}); 
  </script>
<script type="text/javascript">

	
  	window.onload = function (){
  		fileUploadAdd();
  	}
  
  	
	function fileUploadAdd(){
  		$("#file-upload0").fileinput({
	        language:'zh',//语言
	        dropZoneEnabled:false,
	        uploadUrl:'<%=basePath%>/upload/image.do',
	        maxFileCount: 1,
	        allowedFileExtensions: ['jpg', 'png', 'gif'],
	        overwriteInitial: false,
	        showRemove:false,
	        dropZoneEnabled: false,  
	        initialPreviewShowDelete:true,  
	        initialPreviewAsData: true,
	        showPreview:true,//是否预览图片
	        showUpload:false,//是否显示上传按钮
	        autoReplace:true//是否替换当前图片
	    }).on("filebatchselected", function(event, files) {
	    	// 如果要自动上传， 请使用以下注解
	    	//$(this).selection.clear();
	    	$(this).fileinput("upload");
	    	$(this).disable();
	    }).on("fileuploaded", function(event, data,pid,i) {
	    	console.log("fileuploaded");
	    	data = data.response;
	    	console.log(data);
	    	console.log(event);
	    	if(data.code == 200){
	    		//$(this).next().val(data.data);
	    		$("#newsLogo").val(data.data);
	    	}
	    }).on("filedeleted",function(vKey, jqXHR, extraData){
	    }).on("filecleared",function(event){
	    	$(".image-upload-input").remove();
	    }).on("filesuccessremove",function(event,id,obj){
	    	console.log("filesuccessremove 上传成功后移除");
	    	$("#"+id).remove();
	    }).on("filebatchuploadsuccess",function(event,data){
	    	console.log("filebatchuploadsuccess");
	    	console.log(data);
    	});	
  		
	 		$("#file-upload").fileinput({
		        language:'zh',//语言
		        dropZoneEnabled:false,
		        uploadUrl:'<%=basePath%>/upload/image.do',
		        maxFileCount: 1,
		        allowedFileExtensions: ['jpg', 'png', 'gif'],
		        overwriteInitial: false,
		        showRemove:false,
		        dropZoneEnabled: false,  
		        initialPreviewShowDelete:true,  
		        initialPreviewAsData: true,
		        showPreview:true,//是否预览图片
		        showUpload:false,//是否显示上传按钮
		        autoReplace:true//是否替换当前图片
		    }).on("filebatchselected", function(event, files) {
		    	// 如果要自动上传， 请使用以下注解
		    	//$(this).selection.clear();
		    	$(this).fileinput("upload");
		    	$(this).disable();
		    }).on("fileuploaded", function(event, data,pid,i) {
		    	console.log("fileuploaded");
		    	data = data.response;
		    	console.log(data);
		    	console.log(event);
		    	if(data.code == 200){
		    		//$(this).next().val(data.data);
		    		$("#picUrl1").val(data.data);
		    	}
		    }).on("filedeleted",function(vKey, jqXHR, extraData){
		    }).on("filecleared",function(event){
		    	$(".image-upload-input").remove();
		    }).on("filesuccessremove",function(event,id,obj){
		    	console.log("filesuccessremove 上传成功后移除");
		    	$("#"+id).remove();
		    }).on("filebatchuploadsuccess",function(event,data){
		    	console.log("filebatchuploadsuccess");
		    	console.log(data);
	    	});	
	    	
	    	
	    	$("#file-upload2").fileinput({
		        language:'zh',//语言
		        dropZoneEnabled:false,
		        uploadUrl:'<%=basePath%>/upload/image.do',
		        maxFileCount: 1,
		        allowedFileExtensions: ['jpg', 'png', 'gif'],
		        overwriteInitial: false,
		        showRemove:false,
		        dropZoneEnabled: false,  
		        initialPreviewShowDelete:true,  
		        initialPreviewAsData: true,
		        showPreview:true,//是否预览图片
		        
		        //showCaption:false,//是否显示jianjie
		        showUpload:false,//是否显示上传按钮
		        autoReplace:true//是否替换当前图片
		    }).on("filebatchselected", function(event, files) {
		    	// 如果要自动上传， 请使用以下注解
		    	//$(this).selection.clear();
		    	$(this).fileinput("upload");
		    	$(this).disable();
		    }).on("fileuploaded", function(event, data,pid,i) {
		    	console.log("fileuploaded");
		    	data = data.response;
		    	console.log(data);
		    	console.log(event);
		    	if(data.code == 200){
		    		//$(this).next().val(data.data);
		    		$("#picUrl2").val(data.data);
		    	}
		    }).on("filedeleted",function(vKey, jqXHR, extraData){
		    }).on("filecleared",function(event){
		    	$(".image-upload-input").remove();
		    }).on("filesuccessremove",function(event,id,obj){
		    	console.log("filesuccessremove 上传成功后移除");
		    	$("#"+id).remove();
		    }).on("filebatchuploadsuccess",function(event,data){
		    	console.log("filebatchuploadsuccess");
		    	console.log(data);
	    	});	
	    	
	    	$("#file-upload3").fileinput({
		        language:'zh',//语言
		        dropZoneEnabled:false,
		        uploadUrl:'<%=basePath%>/upload/image.do',
		        maxFileCount: 1,
		        allowedFileExtensions: ['jpg', 'png', 'gif'],
		        overwriteInitial: false,
		        showRemove:false,
		        dropZoneEnabled: false,  
		        initialPreviewShowDelete:true,  
		        initialPreviewAsData: true,
		        showPreview:true,//是否预览图片
		        showUpload:false,//是否显示上传按钮
		        autoReplace:true//是否替换当前图片
		    }).on("filebatchselected", function(event, files) {
		    	// 如果要自动上传， 请使用以下注解
		    	//$(this).selection.clear();
		    	$(this).fileinput("upload");
		    	$(this).disable();
		    }).on("fileuploaded", function(event, data,pid,i) {
		    	console.log("fileuploaded");
		    	data = data.response;
		    	console.log(data);
		    	console.log(event);
		    	if(data.code == 200){
		    		//$(this).next().val(data.data);
		    		$("#picUrl3").val(data.data);
		    	}
		    }).on("filedeleted",function(vKey, jqXHR, extraData){
		    }).on("filecleared",function(event){
		    	$(".image-upload-input").remove();
		    }).on("filesuccessremove",function(event,id,obj){
		    	console.log("filesuccessremove 上传成功后移除");
		    	$("#"+id).remove();
		    }).on("filebatchuploadsuccess",function(event,data){
		    	console.log("filebatchuploadsuccess");
		    	console.log(data);
	    	});	
	    	
	
	 	}
	 
  	

		
	 	
	 	$(document).ready(function(){
	 		console.log("eee");
			ajaxSubmitFormAndValid("menuObj",function(data){
				if(data.code == 200) {
					parent.getEntityList();
					parent.layer.msg("操作成功");
					var index = parent.layer.getFrameIndex(window.name);
					parent.layer.close(index);
				} else {
					layer.msg(data.msg);
				}
			},function(msg,o,cssctl){
				//msg：提示信息;
				//o:{obj:*,type:*,curform:*}, obj指向的是当前验证的表单元素（或表单对象），type指示提示的状态，值为1、2、3、4， 1：正在检测/提交数据，2：通过验证，3：验证失败，4：提示ignore状态, curform为当前form对象;
				//cssctl:内置的提示信息样式控制函数，该函数需传入两个参数：显示提示信息的对象 和 当前提示的状态（既形参o中的type）;
				if(!o.obj.is("form")){//验证表单元素时o.obj为该表单元素，全部验证通过提交表单时o.obj为该表单对象;
				console.log(o.obj);	
				var objtip=o.obj.parent(".layui-input-inline,.layui-input-block").nextAll(".Validform_checktip");
				if(objtip.length > 1) {
					objtip = objtip[0];
				}	
				cssctl(objtip,o.type);
					objtip.text(msg);
				}else{
					/* var objtip=o.obj.find("#msgdemo");
					cssctl(objtip,o.type);
					objtip.text(msg); */
				}
			});
		});
		
		layui.use('upload', function(){
		  layui.upload({
		    url: '' //上传接口
		    ,success: function(res){ //上传成功后的回调
		      console.log(res)
		    }
		  });
		  
		  layui.upload({
		    url: '/test/upload.json'
		    ,elem: '#test' //指定原始元素，默认直接查找class="layui-upload-file"
		    ,method: 'get' //上传接口的http类型
		    ,success: function(res){
		      LAY_demo_upload.src = res.url;
		    }
		  });
		});
		
	 	function doAddOrUpdate(data){
	 		console.log("XXX");
			$.ajax({
				url : 'doAddOrUpdate.do',
				type : 'POST',
				data : data,
				async : false,
				dataType : 'json',
				timeout : 10000,
				error : function() {layer.msg('<spring:message code="prompted.request" />');},
				success : function(result) {
					if (result.code == 200) {
						layer.msg("<spring:message code='prompted.success' />");
						var index = parent.layer.getFrameIndex(window.name); //先得到当前iframe层的索引
						parent.layer.close(index);
						//重新加载数据及树桩表格
						getEntityList();
			  			parent.nowEntity();
					} else {
						layer.msg(result.msg);
					}
				}
			});
		} 
		
		//新增数据，添加下级时使用
    	function treeChkDisabledThis(parentId,treeId){
			var zTree = $.fn.zTree.getZTreeObj(treeId);
			if(parentId == null || parentId == 0) {
				return;
			}
			var nodes = zTree.getNodeByParam("id", parentId);
			// 选中父节点
			zTree.checkNode(nodes, true, true,true);
			$("#"+treeId).trigger("tree.select.change");
    	}
    	
    	//修改数据时    设置选中   且自身及以下不可选
    	function treeChkDisabled(id,parentId,treeId){
			var zTree = $.fn.zTree.getZTreeObj(treeId);
			// 设置自身节点不能选
			zTree.setChkDisabled(zTree.getNodeByParam("id", id),true,false ,true );
			if(parentId == null || parentId == 0) {
				return;
			}
			var nodes = zTree.getNodeByParam("id", parentId);
			// 选中父节点
			zTree.checkNode(nodes, true, true,true);
			$("#"+treeId).trigger("tree.select.change");
    	}
    	
    	function doClose(){
    		var index = parent.layer.getFrameIndex(window.name); //先得到当前iframe层的索引
			parent.layer.close(index);
    	}
  </script>
</html>
